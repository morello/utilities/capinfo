// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Basic text formatting tools.
//!
//! This module allows common source text to be used for both CLI and GUI applications. Typically,
//! the CLI would strip or ignore markup, whilst the GUI would use it to format text.
//!
//! The syntax is based on a single-pass character-by-character parser, with the following rules:
//!
//! - `'\'` escapes the next character.
//! - `'$'` resets all formatting to the default (plain text).
//! - `'_'` starts subscript text.
//! - `'^'` starts superscript text.
//! - any whitespace ends the subscript/superscript (if active).
//! - additionally, `'\n'` and `'\t'` reset all formatting.
//! - ``'`'`` toggles monospace.
//! - `'*'` toggles bold text.

use itertools::Itertools;
use std::{borrow::Cow, fmt};

/// A simple formatting wrapper around a string-like value.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct RichText<S: AsRef<str>> {
    s: S,
}

impl<S: AsRef<str> + Default> RichText<S> {
    pub fn new() -> Self {
        Self::default()
    }
}

impl<S: AsRef<str>> RichText<S> {
    pub fn as_ref(&self) -> RichText<&S> {
        RichText::from(&self.s)
    }

    pub fn as_str(&self) -> RichText<&str> {
        RichText::from(self.as_raw_str())
    }

    /// Access the raw, unprocessed string.
    pub fn as_raw_str(&self) -> &str {
        self.s.as_ref()
    }

    /// Unwrap and return the raw, unprocessed string.
    pub fn raw(self) -> S {
        self.s
    }

    /// True if all parts are empty (ignoring formatting).
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::rich_text::RichText;
    /// assert!(!RichText::from("test").is_empty());
    /// assert!(!RichText::from("*test*").is_empty());
    /// assert!(RichText::from("**").is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.parts().all(|p| p.as_str().is_empty())
    }

    /// Iterate over segments separated by '\n'.
    pub fn lines(&self) -> impl Iterator<Item = RichText<&str>> + '_ {
        // We can simply split the raw string because '\n' resets all formatting.
        self.as_raw_str().lines().map(RichText::from)
    }

    /// Iterate over segments separated by '\t'.
    pub fn tabs(&self) -> impl Iterator<Item = RichText<&str>> + '_ {
        // We can simply split the raw string because '\t' resets all formatting.
        self.as_raw_str().split('\t').map(RichText::from)
    }

    /// Iterate over `Part`s of the string.
    #[allow(clippy::suspicious_operation_groupings)]
    pub fn parts(&self) -> impl Iterator<Item = Part<'_>> + '_ {
        let s = self.as_raw_str();
        self.char_indices()
            .map(|(i, c, fmt)| (i..(i + c.len_utf8()), fmt))
            .coalesce(|a, b| {
                let (a_fmt, b_fmt) = (a.1, b.1);
                // Coalesce contiguous parts with the same format.
                if (a_fmt == b_fmt) && (a.0.end == b.0.start) {
                    Ok((a.0.start..b.0.end, a_fmt))
                } else {
                    Err((a, b))
                }
            })
            .map(move |(range, fmt)| Part::new(&s[range], fmt))
    }

    fn char_indices(&self) -> IndexedChars {
        IndexedChars::new(self.as_raw_str())
    }
}

impl<S: AsRef<str> + Into<String>> RichText<S> {
    pub fn into_owned(self) -> RichText<String> {
        RichText::from(self.s.into())
    }
}

impl<'a, S: AsRef<str> + Into<Cow<'a, str>>> RichText<S> {
    pub fn into_cow(self) -> RichText<Cow<'a, str>> {
        RichText::from(self.s.into())
    }
}

impl RichText<String> {
    /// Create a new `RichText` from an iterator over `Parts`.
    ///
    /// This is the most efficient way to concatenate multiple `RichText`s, preserving formatting
    /// without generating redundant toggles, etc. This minimises empty or redundant <tspan>
    /// elements in the generated DOM.
    pub fn from_parts<'a>(parts: impl Iterator<Item = Part<'a>>) -> RichText<String> {
        let mut raw = String::new();
        let mut last_fmt = Format::new();
        for part in parts {
            let fmt = part.format();

            // Turn bold/mono off _before_ sub/super-script changes.
            if last_fmt.is_bold() && !fmt.is_bold() {
                last_fmt = last_fmt.toggled_bold();
                raw.push('*');
            }
            if last_fmt.is_mono() && !fmt.is_mono() {
                last_fmt = last_fmt.toggled_mono();
                raw.push('`');
            }

            if fmt.is_superscript() {
                if !last_fmt.is_superscript() {
                    last_fmt = last_fmt.made_superscript();
                    raw.push('^');
                }
            } else if fmt.is_subscript() {
                if !last_fmt.is_subscript() {
                    last_fmt = last_fmt.made_subscript();
                    raw.push('_');
                }
            } else if last_fmt.is_subscript() || last_fmt.is_superscript() {
                if part.as_str().starts_with(' ') {
                    last_fmt = last_fmt.made_normal_script();
                } else {
                    last_fmt = Format::new();
                    raw.push('$');
                }
            }

            // Turn bold/mono on _after_ sub/super-script changes.
            if !last_fmt.is_bold() && fmt.is_bold() {
                last_fmt = last_fmt.toggled_bold();
                raw.push('*');
            }
            if !last_fmt.is_mono() && fmt.is_mono() {
                last_fmt = last_fmt.toggled_mono();
                raw.push('`');
            }

            assert_eq!(fmt, last_fmt);
            raw.push_str(part.as_str());
        }
        RichText::from(raw)
    }
}

impl<S: AsRef<str>> From<S> for RichText<S> {
    fn from(s: S) -> Self {
        Self { s }
    }
}

impl<S: AsRef<str>> fmt::Display for RichText<S> {
    /// Plain formatting strips some markup, for clarity on plain-text terminals.
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        for part in self.parts() {
            if part.format().is_subscript() {
                write!(f, "_")?;
            }
            if part.format().is_superscript() {
                write!(f, "^")?;
            }
            write!(f, "{}", part.as_str())?;
        }
        Ok(())
    }
}

/// A contiguous section of text, with the same formatting throughout.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Part<'a> {
    s: &'a str,
    fmt: Format,
}

impl<'a> Part<'a> {
    /// Construct a new `Part`.
    ///
    /// This is private because the caller (`RichText`) must ensure that `s` contains no formatting
    /// markup.
    fn new(s: &'a str, fmt: Format) -> Self {
        Self { s, fmt }
    }

    pub fn as_str(&self) -> &'a str {
        self.s
    }

    pub fn format(&self) -> Format {
        self.fmt
    }
}

/// An iterator over each formatted `char` in a `RichText` string.
#[derive(Clone, Debug)]
struct IndexedChars<'a> {
    chars: std::str::CharIndices<'a>,
    fmt: Format,
    escape: bool,
}

impl<'a> IndexedChars<'a> {
    fn new(raw: &'a str) -> Self {
        Self {
            chars: raw.char_indices(),
            fmt: Format::new(),
            escape: false,
        }
    }
}

impl<'a> Iterator for IndexedChars<'a> {
    /// The index (into the raw source string), the `char` itself, and the format at this point.
    type Item = (usize, char, Format);

    fn next(&mut self) -> Option<Self::Item> {
        for (i, c) in &mut self.chars {
            let pc = if self.escape {
                // Escaped characters are interpreted verbatim.
                ParsedChar::Verbatim
            } else {
                ParsedChar::from(self.fmt, c)
            };
            self.escape = false;
            match pc {
                ParsedChar::Verbatim => {} // Nothing to do.
                ParsedChar::Markup {
                    new_fmt,
                    consume,
                    escape,
                } => {
                    self.fmt = new_fmt;
                    self.escape = escape;
                    if consume {
                        continue;
                    }
                }
            }
            return Some((i, c, self.fmt));
        }
        None
    }
}

enum ParsedChar {
    Verbatim,
    Markup {
        new_fmt: Format,
        consume: bool,
        escape: bool,
    },
}

impl ParsedChar {
    fn from(fmt: Format, c: char) -> Self {
        match c {
            '\\' => Self::Markup {
                new_fmt: fmt,
                consume: true,
                escape: true,
            },
            '$' => Self::new_markup(Format::new()),
            '\t' | '\n' => {
                if fmt.is_default() {
                    Self::Verbatim
                } else {
                    // Don't drop the tab/newline.
                    Self::Markup {
                        new_fmt: Format::new(),
                        consume: false,
                        escape: false,
                    }
                }
            }
            s if s.is_whitespace() => {
                if fmt.is_subscript() || fmt.is_superscript() {
                    // Don't drop the space.
                    Self::Markup {
                        new_fmt: fmt.made_normal_script(),
                        consume: false,
                        escape: false,
                    }
                } else {
                    Self::Verbatim
                }
            }
            '_' => Self::new_markup(fmt.made_subscript()),
            '^' => Self::new_markup(fmt.made_superscript()),
            '*' => Self::new_markup(fmt.toggled_bold()),
            '`' => Self::new_markup(fmt.toggled_mono()),
            _ => Self::Verbatim,
        }
    }

    fn new_markup(new_fmt: Format) -> Self {
        Self::Markup {
            new_fmt,
            consume: true,
            escape: false,
        }
    }
}

/// The format of a single `Part` of rich text.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Format {
    f: u8,
}

const BOLD: u8 = 0b00000001;
const MONO: u8 = 0b00000010;
const SUB: u8 = 0b00000100;
const SUP: u8 = 0b00001000;

impl Format {
    fn new() -> Self {
        Self { f: 0 }
    }

    fn toggled_bold(&self) -> Self {
        Self { f: self.f ^ BOLD }
    }

    fn toggled_mono(&self) -> Self {
        Self { f: self.f ^ MONO }
    }

    fn made_subscript(&self) -> Self {
        Self {
            f: (self.f & !SUP) | SUB,
        }
    }

    fn made_superscript(&self) -> Self {
        Self {
            f: (self.f & !SUB) | SUP,
        }
    }

    fn made_normal_script(&self) -> Self {
        Self {
            f: (self.f & !SUB & !SUP),
        }
    }

    pub fn is_default(&self) -> bool {
        self.f == 0
    }

    pub fn is_bold(&self) -> bool {
        (self.f & BOLD) != 0
    }

    pub fn is_mono(&self) -> bool {
        (self.f & MONO) != 0
    }

    pub fn is_subscript(&self) -> bool {
        (self.f & SUB) != 0
    }

    pub fn is_superscript(&self) -> bool {
        (self.f & SUP) != 0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::borrow::Cow;

    #[test]
    fn ctor() {
        assert_eq!("Test", RichText::from("Test").as_raw_str());
        assert_eq!("Test", RichText::from(String::from("Test")).as_raw_str());
        assert_eq!("Test", RichText::from(Cow::from("Test")).as_raw_str());
    }

    #[test]
    fn display() {
        let rt = RichText::from("Normal_sub^sup = *bold_sub* (`mono`)");
        assert_eq!("Normal_sub^sup = bold_sub (mono)", &format!("{}", rt));
    }

    macro_rules! check_next {
        ($parts:expr, $s:expr) => {
            check_next!($parts, $s, 0)
        };
        ($parts:expr, $s:expr, $f:expr) => {
            assert_eq!(Part::new($s, Format { f: $f }), $parts.next().unwrap())
        };
    }

    #[test]
    fn parts() {
        let rt = RichText::from("Normal_sub^sup = *bold_sub* (`mono`)");
        let mut parts = rt.parts();
        check_next!(parts, "Normal");
        check_next!(parts, "sub", SUB);
        check_next!(parts, "sup", SUP);
        check_next!(parts, " = ");
        check_next!(parts, "bold", BOLD);
        check_next!(parts, "sub", BOLD | SUB);
        check_next!(parts, " (");
        check_next!(parts, "mono", MONO);
        check_next!(parts, ")");
        assert_eq!(None, parts.next());
    }

    #[test]
    fn escape() {
        let rt = RichText::from(r"One$\Two\*Three*Four\*Five*Six\\");
        let mut parts = rt.parts();
        check_next!(parts, "One");
        check_next!(parts, "Two");
        check_next!(parts, "*Three");
        check_next!(parts, "Four", BOLD);
        check_next!(parts, "*Five", BOLD);
        check_next!(parts, "Six");
        check_next!(parts, r"\");
        assert_eq!(None, parts.next());
    }

    #[test]
    fn from_parts() {
        let a = RichText::from("aa``aa");
        let b = RichText::from("*bbbb*");
        let c = RichText::from("`c*cc*c"); // Mono left open.
        let d = RichText::from("**d_d d_d"); // Subscript left open.
        let e = RichText::from(" e_*e e_`e`"); // Bold, mono subscript left open.
        let f = RichText::from("^f"); // Start with a superscript.
        let g = RichText::from("` g"); // Start with a formatted space.

        assert_eq!("aaaa", RichText::from_parts(a.parts()).as_raw_str());
        assert_eq!("*bbbb", RichText::from_parts(b.parts()).as_raw_str());
        assert_eq!("`c*cc*c", RichText::from_parts(c.parts()).as_raw_str());
        assert_eq!("d_d d_d", RichText::from_parts(d.parts()).as_raw_str());
        assert_eq!(" e_*e e_`e", RichText::from_parts(e.parts()).as_raw_str());
        assert_eq!("^f", RichText::from_parts(f.parts()).as_raw_str());
        assert_eq!("` g", RichText::from_parts(g.parts()).as_raw_str());

        let ba = b.parts().chain(a.parts());
        assert_eq!("*bbbb*aaaa", RichText::from_parts(ba).as_raw_str());

        // Like formats are merged.
        let bb = b.parts().chain(b.parts());
        assert_eq!("*bbbbbbbb", RichText::from_parts(bb).as_raw_str());
        let cc = c.parts().chain(c.parts());
        assert_eq!("`c*cc*cc*cc*c", RichText::from_parts(cc).as_raw_str());
        let bff = b.parts().chain(f.parts()).chain(f.parts());
        assert_eq!("*bbbb*^ff", RichText::from_parts(bff).as_raw_str());

        // '$' is used to terminate subscripts without inserting spaces.
        let da = d.parts().chain(a.parts());
        assert_eq!("d_d d_d$aaaa", RichText::from_parts(da).as_raw_str());
        let ea = e.parts().chain(a.parts());
        assert_eq!(" e_*e e_`e*`$aaaa", RichText::from_parts(ea).as_raw_str());

        // ... but only if required.
        let de = d.parts().chain(e.parts());
        assert_eq!("d_d d_d e_*e e_`e", RichText::from_parts(de).as_raw_str());
        let dg = d.parts().chain(g.parts());
        assert_eq!("d_d d_d` g", RichText::from_parts(dg).as_raw_str());
        let ee = e.parts().chain(e.parts());
        assert_eq!(
            " e_*e e_`e*` e_*e e_`e",
            RichText::from_parts(ee).as_raw_str()
        );

        // '$' resets all formatting.
        let eb = e.parts().chain(b.parts());
        assert_eq!(" e_*e e_`e`$*bbbb", RichText::from_parts(eb).as_raw_str());
    }
}
