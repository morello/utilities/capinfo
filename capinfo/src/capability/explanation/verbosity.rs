// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

/// Control which explanations are returned from `Capability::explanations()` and related
/// functions.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
pub struct Verbosity {
    descriptions: bool,
    trivial: bool,
    fancy_bounds: bool,
    multi_value_maps: bool,
}

impl Verbosity {
    /// Show or hide trivial explanations.
    ///
    /// An explanation is considered trivial if it is a simple value that can be read directly from
    /// the encoded bits. For example, Morello's "Flags" field is trivial, since it is just an
    /// eight-bit field with no other purpose.
    pub fn set_trivial(&mut self, to: bool) {
        self.trivial = to;
    }

    /// Show or hide explanatory descriptions.
    ///
    /// If enabled, some explanations will include text to explain (to a human) what's being
    /// explained. This could be useful information for the unfamiliar, but visual clutter for
    /// those who just want the values.
    pub fn set_descriptions(&mut self, to: bool) {
        self.descriptions = to;
    }

    /// Enable or disable full bounds calculation.
    ///
    /// With fancy bounds, several explanations will be provided, covering every step of the bounds
    /// calculation. This is useful for debugging encoding details. Otherwise, the bounds and value
    /// will be shown in a single, concise explanation.
    pub fn set_fancy_bounds(&mut self, to: bool) {
        self.fancy_bounds = to;
    }

    /// Enable or disable field mapping diagrams.
    ///
    /// Most explanation elements map a value onto a single label. Sometimes, it is useful for
    /// the GUI to map values onto multiple labels in a single diagram. This tends to look
    /// cluttered on the CLI, so this is verbosity filter must be used to show such mappings.
    pub fn set_multi_value_maps(&mut self, to: bool) {
        self.multi_value_maps = to;
    }

    pub fn show_trivial(&self) -> bool {
        self.trivial
    }

    pub fn show_descriptions(&self) -> bool {
        self.descriptions
    }

    pub fn show_fancy_bounds(&self) -> bool {
        self.fancy_bounds
    }

    pub fn show_multi_value_maps(&self) -> bool {
        self.multi_value_maps
    }
}
