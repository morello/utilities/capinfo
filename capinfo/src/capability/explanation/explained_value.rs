// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Values described in arbitrary, human-readable intermediate stages.
//!
//! For example: `foo = bar + 1 = 43, because baz.`

use std::{borrow::Cow, fmt};

use crate::rich_text::RichText;

/// A value described by arbitrary, human-readable intermediate stages.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExplainedValue {
    s: RichText<String>,
}

/// Conveniently build `ExplainedValue`s programmatically.
#[derive(Debug, Clone)]
pub(crate) struct ExplainedValueBuilder<'a> {
    name: Option<RichText<Cow<'a, str>>>,
    value: RichText<Cow<'a, str>>,
    intermediates: Vec<RichText<Cow<'a, str>>>,
    reasoning: RichText<Cow<'a, str>>,
}

impl<'a> ExplainedValueBuilder<'a> {
    pub fn new<S>(value: RichText<S>) -> Self
    where
        S: Into<Cow<'a, str>> + AsRef<str>,
    {
        Self {
            name: None,
            value: value.into_cow(),
            intermediates: Vec::new(),
            reasoning: RichText::new(),
        }
    }

    pub fn with_name<S>(self, name: RichText<S>) -> Self
    where
        S: Into<Cow<'a, str>> + AsRef<str>,
    {
        Self {
            name: Some(name.into_cow()),
            ..self
        }
    }

    /// Name the `ExplainedValue`, with automatic conversion to `RichText`.
    pub fn with_rich_name<S>(self, name: S) -> Self
    where
        S: Into<Cow<'a, str>>,
    {
        self.with_name(RichText::from(name.into()))
    }

    pub fn with_reasoning<S>(self, reasoning: RichText<S>) -> Self
    where
        S: Into<Cow<'a, str>> + AsRef<str>,
    {
        Self {
            reasoning: reasoning.into_cow(),
            ..self
        }
    }

    pub fn with_intermediate<S>(mut self, intermediate: RichText<S>) -> Self
    where
        S: Into<Cow<'a, str>> + AsRef<str>,
    {
        self.intermediates.push(intermediate.into_cow());
        self
    }

    pub fn build(self) -> ExplainedValue {
        let equals = RichText::from("` = ");
        let value = RichText::from(format!("`{}", self.value));
        let parts = self
            .name
            .iter()
            .flat_map(|rt| rt.parts().chain(equals.parts()))
            .chain(
                self.intermediates
                    .iter()
                    .flat_map(|rt| rt.parts().chain(equals.parts())),
            )
            .chain(value.parts())
            .chain(self.reasoning.parts());
        ExplainedValue {
            s: RichText::from_parts(parts),
        }
    }
}

impl fmt::Display for ExplainedValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        self.s.fmt(f)
    }
}

impl ExplainedValue {
    pub fn into_rich_text(self) -> RichText<String> {
        self.s
    }
}
