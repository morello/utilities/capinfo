// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Map permissions names and values onto a field.

use itertools::Itertools;
use std::{fmt, iter};

use crate::{
    bitwise::Bitwise,
    field::{Field, FieldWithValue},
};

/// A contiguous list of `Permission` bits.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Permissions {
    fv: FieldWithValue<u64>,
    perms: Vec<Permission>,
}

impl Permissions {
    /// Construct a new `Permissions` list, mapping each name onto successive bits of `fv`
    /// (starting at `lsb`).
    ///
    /// The `names` iterator must return exactly `fv.size_in_bits()` names. This panics otherwise.
    pub(crate) fn from<S: Into<String>>(
        fv: FieldWithValue<u64>,
        names: impl Iterator<Item = S>,
    ) -> Self {
        let perms = (fv.lsb()..=fv.msb())
            .zip_eq(names)
            .map(|(abs_bit, name)| {
                let rel_bit = abs_bit - fv.lsb();
                // `FieldWithValue` checks on construction that all bits lsb..=msb are accessible, so
                // `unwrap()` is safe here.
                Permission::new(abs_bit, name.into(), fv.value().bit(rel_bit).unwrap())
            })
            .collect();
        Self { fv, perms }
    }

    /// Iterate over permissions in lsb->msb order.
    ///
    /// This implements `DoubleEndedIterator`, so `.rev()` can obtain msb->lsb order.
    pub fn iter(&self) -> impl Iterator<Item = &Permission> + DoubleEndedIterator {
        self.perms.iter()
    }

    pub fn field(&self) -> Field {
        self.fv.without_value()
    }
}

impl IntoIterator for Permissions {
    type Item = Permission;
    type IntoIter = <Vec<Permission> as IntoIterator>::IntoIter;
    /// Iterate over permissions in lsb->msb order.
    fn into_iter(self) -> Self::IntoIter {
        self.perms.into_iter()
    }
}

impl fmt::Display for Permissions {
    /// Draw the diagram with Unicode box-drawing characters.
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "0b{0:01$b} (0x{0:02$x})",
            self.fv.value(),
            self.fv.size_in_bits() as usize,
            self.fv.size_in_nibbles() as usize,
        )?;
        let mut line_chars: Vec<char> = (0..self.fv.size_in_bits())
            .rev() // Print little-endian.
            .map(|i| match self.fv.value().bit(i) {
                Some(true) => '|',
                _ => ' ',
            })
            .chain(iter::once(' ')) // Padding
            .collect();

        for p in self.perms.iter().filter(|p| p.value()) {
            writeln!(f)?;
            let mut c = (self.fv.msb() - p.bit()) as usize;
            line_chars[c] = '└';
            c += 1;
            while c < line_chars.len() {
                line_chars[c] = '─';
                c += 1;
            }
            write!(f, "  ")?; // Align after "0b".
            for ch in &line_chars {
                write!(f, "{}", ch)?;
            }
            write!(f, " {}", p.name())?;
        }
        Ok(())
    }
}

/// A permission, represented by a single bit in a capability.
#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq)]
pub struct Permission {
    bit: u32, // Highest priority for `Ord`.
    name: String,
    value: bool,
}

impl Permission {
    /// Construct a new `Permission`.
    ///
    /// # Example:
    ///
    /// ```
    /// # use capinfo::capability::explanation::permissions::Permission;
    /// let perm = Permission::new(42, "Load", true);
    /// assert_eq!("Load", perm.name());
    /// assert_eq!(42, perm.bit());
    /// assert!(perm.value());
    /// ```
    pub fn new<S>(bit: u32, name: S, value: bool) -> Permission
    where
        S: Into<String>,
    {
        Self {
            bit,
            name: name.into(),
            value,
        }
    }

    /// The index of the bit that controls this Permission.
    pub fn bit(&self) -> u32 {
        self.bit
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn value(&self) -> bool {
        self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn getters() {
        let p = Permission::new(42, "foo", false);
        assert_eq!(42, p.bit());
        assert_eq!("foo", p.name());
        assert_eq!(false, p.value());
    }

    #[test]
    fn ord() {
        // Sort by bit number first ...
        assert!(Permission::new(42, "a", true) > Permission::new(41, "b", false));
        // ... then by name ...
        assert!(Permission::new(42, "a", true) < Permission::new(42, "b", false));
        // ... and finally by value.
        assert!(Permission::new(42, "a", true) > Permission::new(42, "a", false));
    }

    #[test]
    fn ne() {
        assert!(Permission::new(42, "a", true) != Permission::new(41, "b", false));
        assert!(Permission::new(42, "a", true) != Permission::new(42, "b", false));
        assert!(Permission::new(42, "a", true) != Permission::new(42, "a", false));
    }

    #[test]
    fn eq() {
        assert!(Permission::new(42, "a", true) == Permission::new(42, "a", true));
    }
}
