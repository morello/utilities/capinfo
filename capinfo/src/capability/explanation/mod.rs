// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! UI-agnostic, human-readable explanations.

mod verbosity;
pub use verbosity::Verbosity;

pub mod composed_value;
pub mod explained_value;
pub mod permissions;

use std::{cmp, fmt, mem};

use crate::{capability::described_field::Category, field::FieldSet, rich_text::RichText};
use composed_value::ComposedValue;
use explained_value::ExplainedValue;
use permissions::Permissions;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Element {
    RichText(RichText<String>),
    ComposedValueU32(ComposedValue<u32>),
    ComposedValueU64(ComposedValue<u64>),
    ComposedValueU128(ComposedValue<u128>),
    ExplainedValue(ExplainedValue),
    Permissions(Permissions),
}

impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Self::RichText(rt) => rt.fmt(f),
            Self::ComposedValueU32(cv) => cv.fmt(f),
            Self::ComposedValueU64(cv) => cv.fmt(f),
            Self::ComposedValueU128(cv) => cv.fmt(f),
            Self::ExplainedValue(ev) => ev.fmt(f),
            Self::Permissions(d) => d.fmt(f),
        }
    }
}

impl<S: Into<String> + AsRef<str>> From<RichText<S>> for Element {
    fn from(rt: RichText<S>) -> Self {
        Self::RichText(rt.into_owned())
    }
}

impl From<ComposedValue<u32>> for Element {
    fn from(cv: ComposedValue<u32>) -> Self {
        Self::ComposedValueU32(cv)
    }
}

impl From<ComposedValue<u64>> for Element {
    fn from(cv: ComposedValue<u64>) -> Self {
        Self::ComposedValueU64(cv)
    }
}

impl From<ComposedValue<u128>> for Element {
    fn from(cv: ComposedValue<u128>) -> Self {
        Self::ComposedValueU128(cv)
    }
}

impl From<ExplainedValue> for Element {
    fn from(ev: ExplainedValue) -> Self {
        Self::ExplainedValue(ev)
    }
}

impl From<Permissions> for Element {
    fn from(d: Permissions) -> Self {
        Self::Permissions(d)
    }
}

/// A human-readable explanation of some aspect of a capability.
///
/// Each `Explanation` has:
/// - a human-readable title,
/// - one or more component [`Element`]s, which should usually be displayed line-by-line,
/// - a set of fields describing what is explained,
/// - a `generation` field, used to sort explanations and to enforce dependencies.
///
/// `Explanation`s sort by `generation()` first, but ordering within generations should be
/// considered unstable.
///
/// [`FieldSet`]: ../../struct.FieldSet.html
/// [`Element`]: ./enum.Element.html
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Explanation {
    // Sort by generation first.
    generation: u32,
    fields: FieldSet,
    title: RichText<String>,
    elements: Vec<Element>,
    category: Category,
}

impl PartialOrd for Explanation {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Explanation {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.generation
            .cmp(&other.generation)
            // Sort field sets left-to-right (msb -> lsb).
            .then(other.fields.cmp(&self.fields))
            .then(self.title.cmp(&other.title))
            .then(self.elements.cmp(&other.elements))
    }
}

impl Explanation {
    /// Construct a simple `Explanation`, with no contents.
    ///
    /// ```
    /// # use capinfo::{
    /// #     capability::{described_field::Category, explanation::Explanation},
    /// #     field::{Field, FieldSet},
    /// #     rich_text::RichText
    /// # };
    /// let set = FieldSet::new(Field::from_bits(94, 64)).with(Field::from_bits(55, 0));
    /// let expl = Explanation::new(RichText::from("Bounds"), set, Category::Bounds);
    /// assert_eq!("Bounds", expl.title().to_string().as_str());
    /// assert_eq!(Category::Bounds, expl.category());
    /// assert_eq!(Some(94), expl.fields().msb());
    /// assert_eq!(Some(0), expl.fields().lsb());
    /// assert_eq!(Some(Field::from_bits(94, 0)), expl.fields().enclosing_field());
    /// ```
    pub fn new<T, C>(title: RichText<T>, fields: C, category: Category) -> Self
    where
        T: Into<String> + AsRef<str>,
        C: Into<FieldSet>,
    {
        Self {
            generation: 0,
            fields: fields.into(),
            title: title.into_owned(),
            elements: vec![],
            category,
        }
    }

    /// Construct a [`RichText`] [`Element`] and add it to the end of this explanation.
    ///
    /// [`RichText`]: ../../rich_text/struct.RichText.html
    /// [`Element`]: enum.Element.html
    pub fn with_rich_text<S>(mut self, rt: S) -> Self
    where
        S: Into<String>,
    {
        self.add_rich_text(rt);
        self
    }

    /// Construct a [`RichText`] [`Element`] and add it to the end of this explanation.
    ///
    /// [`RichText`]: ../../rich_text/struct.RichText.html
    /// [`Element`]: enum.Element.html
    pub fn add_rich_text<S>(&mut self, rt: S)
    where
        S: Into<String>,
    {
        let rt = RichText::from(rt.into());
        self.add_element(Element::from(rt));
    }

    /// Add `element` to the end of this explanation.
    pub fn with_element<E>(mut self, element: E) -> Self
    where
        E: Into<Element>,
    {
        self.add_element(element);
        self
    }

    /// Add each `element` to the end of this explanation.
    pub fn with_elements<E>(mut self, elements: impl Iterator<Item = E>) -> Self
    where
        E: Into<Element>,
    {
        for element in elements {
            self.add_element(element);
        }
        self
    }

    /// Add `element` to the end of this explanation.
    pub fn add_element<E>(&mut self, element: E)
    where
        E: Into<Element>,
    {
        self.elements.push(element.into());
    }

    /// Set the generation of this `Explanation`.
    ///
    /// This determines the sort order, which permits dependencies between `Explanation`s to be
    /// handled efficiently.
    pub fn with_generation(self, generation: u32) -> Self {
        Self { generation, ..self }
    }

    /// The human-readable title for this `Explanation`.
    pub fn title(&self) -> RichText<&str> {
        self.title.as_str()
    }

    pub fn take_title(&mut self) -> RichText<String> {
        mem::replace(&mut self.title, RichText::new())
    }

    /// The set of all fields described by this `Explanation`.
    pub fn fields(&self) -> &FieldSet {
        &self.fields
    }

    pub fn take_fields(&mut self) -> FieldSet {
        mem::replace(&mut self.fields, FieldSet::empty())
    }

    /// A list of all `Element`s (set by `with_element()`) that provide the human-readable part of
    /// the explanation.
    pub fn elements(&self) -> impl Iterator<Item = &Element> {
        self.elements.iter()
    }

    pub fn take_elements(&mut self) -> Vec<Element> {
        mem::take(&mut self.elements)
    }

    /// The value set by `with_generation()`, or 0 if none has been set.
    pub fn generation(&self) -> u32 {
        self.generation
    }

    pub fn category(&self) -> Category {
        self.category
    }
}
