// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Values described by the concatenation of other values.
//!
//! For example: `foo = bar<5:3>:baz<7:0> = 0x123`

use crate::{
    bitwise::Bitwise,
    capability::described_field::{Category, DescribedField},
    field::{Field, FieldWithValue},
    rich_text::RichText,
};
use itertools::Itertools;
use std::{fmt, mem};

/// A value described by the concatenation of other values.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ComposedValue<V: Bitwise> {
    name: Option<RichText<String>>,
    fv: FieldWithValue<V>,
    context: Option<Field>,
    subfields: Vec<DescribedField>, // Non-overlapping, sorted lsb -> msb (once built).
    bin: bool,
    hex: bool,
    number_bits: bool,
    category: Category,
}

/// Conveniently build `ComposedValue`s programmatically.
#[derive(Debug, Clone)]
pub(crate) struct ComposedValueBuilder<V: Bitwise> {
    v: ComposedValue<V>,
}

impl<V: Bitwise> ComposedValueBuilder<V> {
    pub fn new(fv: FieldWithValue<V>) -> Self {
        Self {
            v: ComposedValue {
                name: None,
                fv,
                context: None,
                subfields: Vec::new(),
                bin: false,
                hex: false,
                number_bits: false,
                category: Category::Unknown,
            },
        }
    }

    pub fn with_name<N>(mut self, name: RichText<N>) -> Self
    where
        N: Into<String> + AsRef<str>,
    {
        self.v.name = Some(name.into_owned());
        self
    }

    /// Name the `ComposedValue`, with automatic conversion to `RichText`.
    pub fn with_rich_name<N>(self, name: N) -> Self
    where
        N: Into<String>,
    {
        self.with_name(RichText::from(name.into()))
    }

    /// Add a subfield.
    ///
    /// Empty `field`s are ignored.
    pub fn with_subfield<S>(mut self, name: RichText<S>, field: Field, category: Category) -> Self
    where
        S: Into<String> + AsRef<str>,
    {
        self.add_subfield(name, field, category);
        self
    }

    /// Add a subfield, treating `name` as RichText.
    ///
    /// Empty `field`s are ignored.
    pub fn with_rich_subfield<S>(self, name: S, field: Field, category: Category) -> Self
    where
        S: Into<String>,
    {
        self.with_subfield(RichText::from(name.into()), field, category)
    }

    /// Add a subfield.
    ///
    /// Empty `field`s are ignored.
    pub fn add_subfield<S>(&mut self, name: RichText<S>, field: Field, category: Category)
    where
        S: Into<String> + AsRef<str>,
    {
        if !field.is_empty() {
            self.v
                .subfields
                .push(DescribedField::new(field, name, category));
        }
    }

    /// Add a subfield, treating `name` as RichText.
    ///
    /// Empty `field`s are ignored.
    pub fn add_rich_subfield<S>(&mut self, name: S, field: Field, category: Category)
    where
        S: Into<String>,
    {
        self.add_subfield(RichText::from(name.into()), field, category);
    }

    /// Specify a binary format for the final value.
    ///
    /// If not specified, the render is free to choose a format.
    pub fn with_bin_value(mut self) -> Self {
        self.v.bin = true;
        self.v.hex = false;
        self
    }

    /// Specify a hex format for the final value.
    ///
    /// If not specified, the render is free to choose a format.
    pub fn with_hex_value(mut self) -> Self {
        self.v.bin = false;
        self.v.hex = true;
        self
    }

    /// Set the category for the whole value.
    ///
    /// This is used to render `ComposedValue`s that have no subfields.
    pub fn with_category(mut self, category: Category) -> Self {
        self.v.category = category;
        self
    }

    /// Describe the context from which the primary field was extracted.
    ///
    /// This is ignored by the provided text renderer.
    pub fn with_context(mut self, context: Field) -> Self {
        self.v.context = Some(context);
        self
    }

    /// Number bits on each subfield.
    ///
    /// This is ignored by the provided text renderer.
    pub fn with_subfield_bit_numbers(mut self) -> Self {
        self.v.number_bits = true;
        self
    }

    pub fn build(mut self) -> ComposedValue<V> {
        self.v.subfields.sort(); // lsb -> msb
        if self
            .v
            .subfields
            .iter()
            .tuple_windows()
            .any(|(a, b)| a.field().overlaps(b.field()))
        {
            panic!("Subfields must not overlap.");
        }
        self.v
    }
}

struct SubfieldFormatter<'a, V: Bitwise> {
    bits_remaining: u32,
    fv: &'a FieldWithValue<V>,
    subfields: &'a [DescribedField],
}

impl<'a, V: Bitwise> SubfieldFormatter<'a, V> {
    fn new(cv: &'a ComposedValue<V>) -> Self {
        // Find all subfields that are contained within `fv`.
        // Fields outside `fv` might be used by the GUI, but we'll ignore them for text output.
        let mut contained = cv
            .subfields
            .split(|sf| !cv.fv.contains(sf.field()))
            .filter(|slice| !slice.is_empty());

        // `fv` is contiguous, so sorted inputs will `split` into zero or one non-empty slices.
        let subfields = contained.next().unwrap_or(&[]);
        assert!(
            contained.next().is_none(),
            "Subfields should be sorted lsb -> msb"
        );

        let bits_remaining = if subfields.is_empty() {
            0
        } else {
            cv.fv.size_in_bits()
        };

        Self {
            bits_remaining,
            fv: &cv.fv,
            subfields,
        }
    }
}

impl<'a, V: Bitwise> Iterator for SubfieldFormatter<'a, V> {
    type Item = RichText<String>;

    fn next(&mut self) -> Option<RichText<String>> {
        if self.bits_remaining == 0 {
            return None;
        }
        let next_bit = self.fv.lsb() + self.bits_remaining - 1;

        // `self.subfields` is in lsb->msb order, so we `split_last()` to format them
        // left-to-right.
        let literal = if let Some((next_sf, remaining_sfs)) = self.subfields.split_last() {
            if next_sf.field().contains_bit(next_bit) {
                self.subfields = remaining_sfs;
                self.bits_remaining -= next_sf.field().size_in_bits();
                let name = next_sf.name();
                let contains_whitespace = name.as_raw_str().split_whitespace().nth(1).is_some();
                let display_name = if contains_whitespace {
                    // Wrap the name in brackets.
                    let open = RichText::from("`{");
                    let close = RichText::from("`}");
                    RichText::from_parts(open.parts().chain(name.parts()).chain(close.parts()))
                } else {
                    name.into_owned()
                };
                return Some(display_name);
            } else {
                Field::from_bits(next_bit, next_sf.field().msb() + 1)
            }
        } else {
            // There are no remaining fields, so fill the last part.
            Field::from_bits(next_bit, self.fv.lsb())
        };
        assert!(!literal.is_empty());
        self.bits_remaining -= literal.size_in_bits();
        // Print literal bits to fill the gap to the next subfield.
        let mut s = String::with_capacity(3 + literal.size_in_bits() as usize);
        s.push_str("`0b");
        let lsb = literal.lsb() - self.fv.lsb();
        let msb = literal.msb() - self.fv.lsb();
        for bit in (lsb..=msb).rev() {
            match self.fv.value_ref().bit(bit) {
                Some(false) => s.push('0'),
                Some(true) => s.push('1'),
                None => s.push('?'),
            }
        }
        Some(s.into())
    }
}

impl<V: Bitwise> ComposedValue<V> {
    pub fn has_subfields(&self) -> bool {
        !self.subfields.is_empty()
    }

    pub fn has_explicit_hex_value(&self) -> bool {
        self.hex
    }

    pub fn has_explicit_bin_value(&self) -> bool {
        self.bin
    }

    pub fn has_subfield_bit_numbers(&self) -> bool {
        self.number_bits
    }

    pub fn context(&self) -> Option<Field> {
        self.context
    }

    pub fn category(&self) -> Category {
        self.category
    }

    /// Take the name from this ComposedValue, leaving an empty value behind.
    pub fn take_name(&mut self) -> Option<RichText<String>> {
        self.name.take()
    }

    /// Take the subfields from this ComposedValue, leaving an empty list behind.
    ///
    /// Subjects are guaranteed to be non-overlapping, and in lsb->msb order.
    pub fn take_subfields(&mut self) -> Vec<DescribedField> {
        mem::take(&mut self.subfields)
    }
}

impl<V: Bitwise + Copy> ComposedValue<V> {
    pub fn field_with_value(&self) -> FieldWithValue<V> {
        self.fv
    }
}

impl<V: Bitwise> fmt::Display for ComposedValue<V>
where
    V: fmt::LowerHex,
    V: fmt::Binary,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        // `ComposedValue`s are best shown graphically, but we can approximate them.
        let equals = RichText::from("` = ");
        let colon = RichText::from("`:").into_owned();
        let subfields = SubfieldFormatter::new(self).collect_vec();
        // TODO: Print separators at field boundaries.
        let value = match (self.hex, self.bin) {
            // Binary is very verbose, so choose it only if unambiguously specified.
            (false, true) => RichText::from(format!(
                "`0b{:01$b}",
                self.fv.value_ref(),
                self.fv.size_in_bits() as usize
            )),
            _ => RichText::from(format!(
                "`0x{:01$x}",
                self.fv.value_ref(),
                self.fv.size_in_nibbles() as usize
            )),
        };

        let mut parts = Vec::new();
        if let Some(name) = &self.name {
            // <name> =
            parts.extend(name.parts());
            parts.extend(equals.parts());
        }
        if !subfields.is_empty() {
            // <subfield>:<subfield>:... =
            parts
                .extend(Itertools::intersperse(subfields.iter(), &colon).flat_map(|sf| sf.parts()));
            parts.extend(equals.parts());
        }
        // <value>
        parts.extend(value.parts());
        RichText::from_parts(parts.into_iter()).fmt(f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn composed_empty() {
        // Ensure that it's possible to build an empty `ComposedValue`.
        // This is useful for values with conditionally-included subfields.
        let fv = FieldWithValue::from_bits_with_value(109, 100, 42u16);
        let builder = ComposedValueBuilder::new(fv).with_name(RichText::from("Name"));
        // The composition itself is omitted with this usage.
        assert_eq!("Name = 0x02a", builder.build().to_string());
    }

    #[test]
    fn anon() {
        // Ensure that it's possible to build an anonymous `ComposedValue`.
        let fv = FieldWithValue::from_bits_with_value(109, 100, 42u16);
        let builder = ComposedValueBuilder::new(fv);
        // The composition itself is omitted with this usage.
        assert_eq!("0x02a", builder.build().to_string());
    }
}
