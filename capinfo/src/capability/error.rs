// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use bitvec::prelude::*;
use itertools::Itertools;
use std::{fmt, fmt::Write, ops::RangeInclusive};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ParseError {
    /// No input was provided (after ignored characters were stripped).
    Empty,

    /// One or more invalid characters were provided, indicated by `at`.
    InvalidCharacters { at: FlaggedChars },

    /// Too many digits were provided for a simple-format capability.
    ///
    /// `found` indicates the recognised digits (excluding readability separators), so
    /// `found.count()` gives the number of recognised digits.
    ///
    /// `max` indicates the maximum number of digits for a simple-format capability.
    TooManyDigits { found: FlaggedChars, max: usize },

    /// Too few digits were provided for a simple-format capability.
    ///
    /// `found` indicates the recognised digits (excluding readability separators), so
    /// `found.count()` gives the number of recognised digits.
    ///
    /// `min` indicates the minimum number of digits for a simple-format capability.
    ///
    /// If no digits were specified, `Empty` will be used instead.
    TooFewDigits { found: FlaggedChars, min: usize },

    /// At least one field separator was used, resulting in an invalid number of fields.
    ///
    /// `fields` marks each field, so `fields.len()` gives the number of recognised fields.
    /// `allowed` lists the permitted numbers of fields for this format.
    WrongNumberOfFields {
        fields: Vec<FlaggedChars>,
        allowed: Vec<u32>,
    },

    /// Each element describes the field, and the maxmimum number of digits for that field.
    ///
    /// Fields with a valid digit count are not included. Fields are reported in left-to-right
    /// (msb-to-lsb) order.
    ///
    /// Note that fields are allowed to have no digits, so there is no need for `TooFewDigitsInFields`.
    TooManyDigitsInFields(Vec<(FlaggedChars, usize)>),
}

impl ParseError {
    /// Return a string representation of the flagged characters, suitable for printing directly
    /// underneath the original input.
    pub fn flagged(&self) -> Option<String> {
        let groups = FlaggedChars::from_union(self.flagged_groups());
        if groups.none() {
            None
        } else {
            Some(groups.to_string()) // Combine all fields into one.
        }
    }

    /// An iterator over all `FlaggedChars` instances held by this `ParseError`.
    ///
    /// This avoids verbose matches in calling code.
    pub fn flagged_groups(&self) -> FlaggedGroups {
        FlaggedGroups::new(self)
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            ParseError::Empty => write!(f, "Input empty"),
            ParseError::InvalidCharacters { at } => match at.count() {
                0 => panic!(),
                1 => write!(f, "Invalid character found"),
                n => write!(f, "{} invalid characters found", n),
            },
            ParseError::TooManyDigits { found, max } => {
                write!(f, "Found {} digits, expected {}", found.count(), max)
            }
            ParseError::TooFewDigits { found, min } => {
                write!(f, "Found {} digits, expected {}", found.count(), min)
            }
            ParseError::WrongNumberOfFields { fields, allowed } => {
                assert!(!allowed.is_empty());
                let expected =
                    allowed
                        .iter()
                        .with_position()
                        .fold(String::new(), |mut acc, pos| {
                            match pos {
                                itertools::Position::Only(a) => write!(acc, "{}", a),
                                itertools::Position::First(a) => write!(acc, "{}", a),
                                itertools::Position::Middle(a) => write!(acc, ", {}", a),
                                itertools::Position::Last(a) => write!(acc, " or {}", a),
                            }
                            .unwrap(); // `write!` to a string can't fail.
                            acc
                        });
                write!(f, "Found {} fields, expected {}", fields.len(), expected)
            }
            ParseError::TooManyDigitsInFields(list) => match list.len() {
                0 => panic!(),
                1 => write!(
                    f,
                    "Too many digits in field (found {}, expected max {})",
                    list[0].0.count(),
                    list[0].1
                ),
                n => {
                    let found = list.iter().map(|n| n.0.count()).join(", ");
                    let expected = list.iter().map(|n| n.1).join(", ");
                    write!(
                        f,
                        "{} fields had too many digits \
                        (found [{}], expected max [{}], respectively)",
                        n, found, expected
                    )
                }
            },
        }
    }
}

/// An iterator over all `FlaggedChars` instances held by a `ParseError`.
pub struct FlaggedGroups<'a> {
    error: &'a ParseError,
    next: usize,
}

impl<'a> FlaggedGroups<'a> {
    fn new(error: &'a ParseError) -> Self {
        Self { error, next: 0 }
    }
}

impl<'a> Iterator for FlaggedGroups<'a> {
    type Item = &'a FlaggedChars;

    fn next(&mut self) -> Option<Self::Item> {
        if self.next >= self.len() {
            return None;
        }
        let result = Some(match self.error {
            ParseError::Empty => panic!(),
            ParseError::InvalidCharacters { at } => at,
            ParseError::TooManyDigits { found, .. } => found,
            ParseError::TooFewDigits { found, .. } => found,
            ParseError::WrongNumberOfFields { fields, .. } => &fields[self.next],
            ParseError::TooManyDigitsInFields(fields) => &fields[self.next].0,
        });
        self.next += 1;
        result
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = match self.error {
            ParseError::Empty => 0,
            ParseError::InvalidCharacters { .. }
            | ParseError::TooManyDigits { .. }
            | ParseError::TooFewDigits { .. } => 1,
            ParseError::WrongNumberOfFields { fields, .. } => fields.len(),
            ParseError::TooManyDigitsInFields(fields) => fields.len(),
        };
        (len, Some(len))
    }
}

impl<'a> ExactSizeIterator for FlaggedGroups<'a> {}

/// Indicate erroneous characters in the input.
///
/// This indexes grapheme clusters, not `char`s (which represent code points). This accurately
/// indexes actually-rendered characters in most cases. For example, "é" and "é" both appear the
/// same, but the former is a single (multi-byte) code point (0xe9) whilst the latter is formed
/// from two code-points: a latin "e" (0x65) followed by a diacritical acute (0x0301).
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FlaggedChars {
    flagged: BitVec,
}

impl FlaggedChars {
    pub(super) fn new() -> Self {
        Self {
            flagged: BitVec::new(),
        }
    }

    /// Merge `other` into `self`.
    fn or(&mut self, other: &Self) {
        let len = usize::max(self.flagged.len(), other.flagged.len());
        self.flagged.resize(len, false);
        self.flagged |= &other.flagged;
    }

    /// Combine a list of `FlaggedChars` objects into a single object.
    fn from_union<'a>(others: impl Iterator<Item = &'a Self>) -> Self {
        others.fold(Self::new(), |mut acc, other| {
            acc.or(other);
            acc
        })
    }

    /// True if no errors were flagged.
    pub fn none(&self) -> bool {
        !self.any()
    }

    /// True if at least one error was flagged.
    pub fn any(&self) -> bool {
        self.flagged.any()
    }

    /// The number of flagged errors.
    pub fn count(&self) -> usize {
        self.flagged.count_ones()
    }

    /// Fill in gaps to produce the smallest contiguous range that covers `self`.
    pub fn filled(&self) -> FlaggedChars {
        let first = match self.flagged.first_one() {
            None => return FlaggedChars::new(),
            Some(index) => index,
        };
        let last = self
            .flagged
            .last_one()
            .expect("first_one().is_some() should imply last_one().is_some()");
        Self::from_indices(first..=last)
    }

    /// Iterate over contiguous ranges of flagged characters.
    pub fn runs(&self) -> impl Iterator<Item = RangeInclusive<usize>> + '_ {
        self.flagged.iter_ones().map(|i| i..=i).coalesce(|a, b| {
            if (a.end() + 1) == *b.start() {
                Ok(*a.start()..=*b.end())
            } else {
                Err((a, b))
            }
        })
    }

    pub(super) fn from_indices<I: Iterator<Item = usize>>(indices: I) -> Self {
        let mut result = Self::new();
        for i in indices {
            result.flag(i);
        }
        result
    }

    pub(super) fn flag(&mut self, i: usize) {
        if i >= self.flagged.len() {
            self.flagged.resize(i + 1, false);
        }
        self.flagged.set(i, true);
    }
}

impl fmt::Display for FlaggedChars {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        for flag in self.flagged.iter().by_vals() {
            let c = if flag { '^' } else { ' ' };
            f.write_char(c)?;
        }
        Ok(())
    }
}
