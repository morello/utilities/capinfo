// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[cfg(not(test))]
compile_error!("Test modules should only build with tests enabled.");

use super::*;

#[test]
fn null_capabilities_value_length_0_0() {
    let cap = Morello::from_u128(true, 0x00000000_40000000_00000000_00000000);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(!cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0, cap.value().0);
    assert_eq!(0x0, cap.bounds_value().0);
    let base = 0x0;
    let limit = 0x0;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    assert_eq!(None, cap.effective_bounds());
}

#[test]
fn null_capabilities_value_length_0_1() {
    let cap = Morello::from_u128(false, 0x00000000_40000000_00000000_00000000);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(!cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0, cap.value().0);
    assert_eq!(0x0, cap.bounds_value().0);
    let base = 0x0;
    let limit = 0x0;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    assert_eq!(None, cap.effective_bounds());
}

#[test]
fn null_capabilities_0() {
    let cap = Morello::from_u128(true, 0x00000000_00000000_00000000_00000000);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0, cap.value().0);
    assert_eq!(0x0, cap.bounds_value().0);
    let base = 0x0_00000000_00000000;
    let limit = 0x1_00000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn null_capabilities_1() {
    let cap = Morello::from_u128(false, 0x00000000_00000000_00000000_00000000);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0, cap.value().0);
    assert_eq!(0x0, cap.bounds_value().0);
    let base = 0x0_00000000_00000000;
    let limit = 0x1_00000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_0() {
    let cap = Morello::from_u128(true, 0xd04f0000_11aa8787_718783d5_5899f364);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x71, cap.flags().0);
    assert_eq!(0x718783d5_5899f364, cap.value().0);
    assert_eq!(0xff8783d5_5899f364, cap.bounds_value().0);
    let base = 0xff878000_00000000;
    let limit = 0xffd1a800_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_1() {
    let cap = Morello::from_u128(true, 0xc63cc000_16baff80_11c2214e_3cbc19ac);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x11, cap.flags().0);
    assert_eq!(0x11c2214e_3cbc19ac, cap.value().0);
    assert_eq!(0xffc2214e_3cbc19ac, cap.bounds_value().0);
    let base = 0x0_ffc00000_00000000;
    let limit = 0x1_2b5c0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_2() {
    let cap = Morello::from_u128(true, 0x76148000_09a13fe6_7fc4b748_c73434d0);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7f, cap.flags().0);
    assert_eq!(0x7fc4b748_c73434d0, cap.value().0);
    assert_eq!(0xffc4b748_c73434d0, cap.bounds_value().0);
    let base = 0x0_7fc00000_00000000;
    let limit = 0x1_13400000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_3() {
    let cap = Morello::from_u128(true, 0xbb624000_1eeafff1_2dfd948d_b41c6ef4);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x2d, cap.flags().0);
    assert_eq!(0x2dfd948d_b41c6ef4, cap.value().0);
    assert_eq!(0xfffd948d_b41c6ef4, cap.bounds_value().0);
    let base = 0x0_fffc0000_00000000;
    let limit = 0x1_17ba0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_4() {
    let cap = Morello::from_u128(true, 0x5e790000_3832039b_ff39bb36_b7345fc6);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff39bb36_b7345fc6, cap.value().0);
    assert_eq!(0x39bb36_b7345fc6, cap.bounds_value().0);
    let base = 0x0398000_00000000;
    let limit = 0x7830000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_5() {
    let cap = Morello::from_u128(true, 0x02318000_1c093a76_fce95b1c_7b5c95cb);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xfc, cap.flags().0);
    assert_eq!(0xfce95b1c_7b5c95cb, cap.value().0);
    assert_eq!(0xffe95b1c_7b5c95cb, cap.bounds_value().0);
    let base = 0x0_74e00000_00000000;
    let limit = 0x1_38100000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_6() {
    let cap = Morello::from_u128(true, 0x2ca28000_25b937d7_ffd392eb_9972acd0);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xffd392eb_9972acd0, cap.value().0);
    assert_eq!(0xffd392eb_9972acd0, cap.bounds_value().0);
    let base = 0x37d00000_00000000;
    let limit = 0xa5b80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_7() {
    let cap = Morello::from_u128(true, 0x56930000_33d939fe_73f9318e_903f76f1);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x73, cap.flags().0);
    assert_eq!(0x73f9318e_903f76f1, cap.value().0);
    assert_eq!(0xfff9318e_903f76f1, cap.bounds_value().0);
    let base = 0x0_73f00000_00000000;
    let limit = 0x1_67b00000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_8() {
    let cap = Morello::from_u128(true, 0x7a5f8000_3d4109be_93788e90_338a8285);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x93, cap.flags().0);
    assert_eq!(0x93788e90_338a8285, cap.value().0);
    assert_eq!(0x788e90_338a8285, cap.bounds_value().0);
    let base = 0x13700000_00000000;
    let limit = 0xfa800000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_unsealed_capabilities_9() {
    let cap = Morello::from_u128(true, 0xbe3e8000_1399058e_0b174c00_67e39aeb);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xb, cap.flags().0);
    assert_eq!(0xb174c00_67e39aeb, cap.value().0);
    assert_eq!(0x174c00_67e39aeb, cap.bounds_value().0);
    let base = 0x0b100000_00000000;
    let limit = 0xa7300000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_0() {
    let cap = Morello::from_u128(false, 0x02b64000_0e3aff40_6fa37ce0_0bd35994);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x6f, cap.flags().0);
    assert_eq!(0x6fa37ce0_0bd35994, cap.value().0);
    assert_eq!(0xffa37ce0_0bd35994, cap.bounds_value().0);
    let base = 0x0_ffa00000_00000000;
    let limit = 0x1_271c0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_1() {
    let cap = Morello::from_u128(false, 0x7e560000_27092ece_dd925c8c_3553c617);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xdd, cap.flags().0);
    assert_eq!(0xdd925c8c_3553c617, cap.value().0);
    assert_eq!(0xff925c8c_3553c617, cap.bounds_value().0);
    let base = 0x0_5d900000_00000000;
    let limit = 0x1_4e100000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_2() {
    let cap = Morello::from_u128(false, 0x1fb38000_0e093a97_fa90e84f_312ed0fb);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xfa, cap.flags().0);
    assert_eq!(0xfa90e84f_312ed0fb, cap.value().0);
    assert_eq!(0xff90e84f_312ed0fb, cap.bounds_value().0);
    let base = 0x3a900000_00000000;
    let limit = 0x8e080000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_3() {
    let cap = Morello::from_u128(false, 0xb990c000_13292b37_6b37ed55_0260af0f);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x6b, cap.flags().0);
    assert_eq!(0x6b37ed55_0260af0f, cap.value().0);
    assert_eq!(0x37ed55_0260af0f, cap.bounds_value().0);
    let base = 0x2b300000_00000000;
    let limit = 0x93280000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_4() {
    let cap = Morello::from_u128(false, 0x2c308000_204153af_53a942a8_0c4b1032);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x53, cap.flags().0);
    assert_eq!(0x53a942a8_0c4b1032, cap.value().0);
    assert_eq!(0xffa942a8_0c4b1032, cap.bounds_value().0);
    let base = 0x53a80000_00000000;
    let limit = 0xa0400000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_5() {
    let cap = Morello::from_u128(false, 0xbad00000_3641224e_cc9f330b_7b2d1e20);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xcc, cap.flags().0);
    assert_eq!(0xcc9f330b_7b2d1e20, cap.value().0);
    assert_eq!(0xff9f330b_7b2d1e20, cap.bounds_value().0);
    let base = 0x44900000_00000000;
    let limit = 0xec800000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_6() {
    let cap = Morello::from_u128(false, 0x1f0bc000_0bb91ade_7fba054a_73b5e5f6);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7f, cap.flags().0);
    assert_eq!(0x7fba054a_73b5e5f6, cap.value().0);
    assert_eq!(0xffba054a_73b5e5f6, cap.bounds_value().0);
    let base = 0x0_35b00000_00000000;
    let limit = 0x1_17700000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_7() {
    let cap = Morello::from_u128(false, 0x84c18000_3f22fe33_6be378fb_84b3a1b9);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x6b, cap.flags().0);
    assert_eq!(0x6be378fb_84b3a1b9, cap.value().0);
    assert_eq!(0xffe378fb_84b3a1b9, cap.bounds_value().0);
    let base = 0x0_ffe30000_00000000;
    let limit = 0x1_03f20000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_8() {
    let cap = Morello::from_u128(false, 0x7cf88000_278aff01_57c180b0_a631e208);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x57, cap.flags().0);
    assert_eq!(0x57c180b0_a631e208, cap.value().0);
    assert_eq!(0xffc180b0_a631e208, cap.bounds_value().0);
    let base = 0x0_ffc00000_00000000;
    let limit = 0x1_19e20000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_unsealed_capabilities_9() {
    let cap = Morello::from_u128(false, 0xb70b0000_21d921ee_43d91532_30c7cc28);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x43, cap.flags().0);
    assert_eq!(0x43d91532_30c7cc28, cap.value().0);
    assert_eq!(0xffd91532_30c7cc28, cap.bounds_value().0);
    let base = 0x0_43d00000_00000000;
    let limit = 0x1_43b00000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_0() {
    let cap = Morello::from_u128(true, 0x2963bfff_a0ca00b0_07585108_8ff73f6e);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7, cap.flags().0);
    assert_eq!(0x7585108_8ff73f6e, cap.value().0);
    assert_eq!(0x585108_8ff73f6e, cap.bounds_value().0);
    let base = 0x00580000_00000000;
    let limit = 0x30640000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_1() {
    let cap = Morello::from_u128(true, 0xac5a8002_3c690377_ff73b57a_35f8a012);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff73b57a_35f8a012, cap.value().0);
    assert_eq!(0x73b57a_35f8a012, cap.bounds_value().0);
    let base = 0x03700000_00000000;
    let limit = 0x7c680000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_2() {
    let cap = Morello::from_u128(true, 0x4ed84001_89311b76_7ee1d1f8_545f5457);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7e, cap.flags().0);
    assert_eq!(0x7ee1d1f8_545f5457, cap.value().0);
    assert_eq!(0xffe1d1f8_545f5457, cap.bounds_value().0);
    let base = 0x0_36e00000_00000000;
    let limit = 0x1_12600000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_3() {
    let cap = Morello::from_u128(true, 0xcfd0bfff_82010a7e_dcf101f2_b6478e86);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xdc, cap.flags().0);
    assert_eq!(0xdcf101f2_b6478e86, cap.value().0);
    assert_eq!(0xfff101f2_b6478e86, cap.bounds_value().0);
    let base = 0x0_14f00000_00000000;
    let limit = 0x1_04000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_4() {
    let cap = Morello::from_u128(true, 0xc63ac002_0bf90e1e_dc38f8c9_6d07461c);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xdc, cap.flags().0);
    assert_eq!(0xdc38f8c9_6d07461c, cap.value().0);
    assert_eq!(0x38f8c9_6d07461c, cap.bounds_value().0);
    let base = 0x0_1c300000_00000000;
    let limit = 0x1_17f00000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_5() {
    let cap = Morello::from_u128(true, 0x9a398002_85010f0e_de179558_88f87e64);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xde, cap.flags().0);
    assert_eq!(0xde179558_88f87e64, cap.value().0);
    assert_eq!(0x179558_88f87e64, cap.bounds_value().0);
    let base = 0x0_1e100000_00000000;
    let limit = 0x1_0a000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_6() {
    let cap = Morello::from_u128(true, 0xd9158002_b36100af_01a9f9d6_caef9ec0);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x1, cap.flags().0);
    assert_eq!(0x1a9f9d6_caef9ec0, cap.value().0);
    assert_eq!(0xffa9f9d6_caef9ec0, cap.bounds_value().0);
    let base = 0x00a80000_00000000;
    let limit = 0x73600000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_7() {
    let cap = Morello::from_u128(true, 0xf860bfff_aaa932a7_33a53d84_5664e5e6);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x33, cap.flags().0);
    assert_eq!(0x33a53d84_5664e5e6, cap.value().0);
    assert_eq!(0xffa53d84_5664e5e6, cap.bounds_value().0);
    let base = 0x32a00000_00000000;
    let limit = 0xaaa80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_8() {
    let cap = Morello::from_u128(true, 0x3ea44005_3722ffa0_5ed2ad9d_6d748331);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5e, cap.flags().0);
    assert_eq!(0x5ed2ad9d_6d748331, cap.value().0);
    assert_eq!(0xffd2ad9d_6d748331, cap.bounds_value().0);
    let base = 0x0_ffd00000_00000000;
    let limit = 0x1_3b900000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn valid_sealed_capabilities_9() {
    let cap = Morello::from_u128(true, 0x996a0001_b19afe49_ff92b435_710c7f80);
    assert!(cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff92b435_710c7f80, cap.value().0);
    assert_eq!(0xff92b435_710c7f80, cap.bounds_value().0);
    let base = 0x0_ff920000_00000000;
    let limit = 0x1_1c660000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_0() {
    let cap = Morello::from_u128(false, 0xd7f44000_a0511f9f_5f9fbf15_16d2c27a);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5f, cap.flags().0);
    assert_eq!(0x5f9fbf15_16d2c27a, cap.value().0);
    assert_eq!(0xff9fbf15_16d2c27a, cap.bounds_value().0);
    let base = 0x1f980000_00000000;
    let limit = 0x60500000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_1() {
    let cap = Morello::from_u128(false, 0x6a228000_887a0098_5b4ecdb9_b71fb6a3);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5b, cap.flags().0);
    assert_eq!(0x5b4ecdb9_b71fb6a3, cap.value().0);
    assert_eq!(0x4ecdb9_b71fb6a3, cap.bounds_value().0);
    let base = 0x004c0000_00000000;
    let limit = 0x243c0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_2() {
    let cap = Morello::from_u128(false, 0xd1569388_0d992b9f_eb99ee62_af6cfc1c);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xeb, cap.flags().0);
    assert_eq!(0xeb99ee62_af6cfc1c, cap.value().0);
    assert_eq!(0xff99ee62_af6cfc1c, cap.bounds_value().0);
    let base = 0x2b980000_00000000;
    let limit = 0x8d980000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_3() {
    let cap = Morello::from_u128(false, 0x9c1e4001_0be9277e_4ff17610_382d13ba);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(2), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x4f, cap.flags().0);
    assert_eq!(0x4ff17610_382d13ba, cap.value().0);
    assert_eq!(0xfff17610_382d13ba, cap.bounds_value().0);
    let base = 0x0_4ef00000_00000000;
    let limit = 0x1_17d00000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_4() {
    let cap = Morello::from_u128(false, 0x1f80c005_0d290966_92c497d6_8c27c2b0);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x92, cap.flags().0);
    assert_eq!(0x92c497d6_8c27c2b0, cap.value().0);
    assert_eq!(0xffc497d6_8c27c2b0, cap.bounds_value().0);
    let base = 0x12c00000_00000000;
    let limit = 0x9a500000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_5() {
    let cap = Morello::from_u128(false, 0x82095388_193200c1_4b3096e5_a01a08c5);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x4b, cap.flags().0);
    assert_eq!(0x4b3096e5_a01a08c5, cap.value().0);
    assert_eq!(0x3096e5_a01a08c5, cap.bounds_value().0);
    let base = 0x00300000_00000000;
    let limit = 0x164c0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_6() {
    let cap = Morello::from_u128(false, 0xa1344001_8d42025a_df4bef01_b1a52701);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xdf, cap.flags().0);
    assert_eq!(0xdf4bef01_b1a52701, cap.value().0);
    assert_eq!(0x4bef01_b1a52701, cap.bounds_value().0);
    let base = 0x04b0000_00000000;
    let limit = 0x9a80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_7() {
    let cap = Morello::from_u128(false, 0x53ad0000_b8aaff79_7bde8f3a_b68c85e3);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7b, cap.flags().0);
    assert_eq!(0x7bde8f3a_b68c85e3, cap.value().0);
    assert_eq!(0xffde8f3a_b68c85e3, cap.bounds_value().0);
    let base = 0x0_ffde0000_00000000;
    let limit = 0x1_1e2a0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_8() {
    let cap = Morello::from_u128(false, 0x5a2b0001_b3e94f3f_4f3e40c9_2a364e3e);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x4f, cap.flags().0);
    assert_eq!(0x4f3e40c9_2a364e3e, cap.value().0);
    assert_eq!(0x3e40c9_2a364e3e, cap.bounds_value().0);
    let base = 0x4f380000_00000000;
    let limit = 0xb3e80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn invalid_sealed_capabilities_9() {
    let cap = Morello::from_u128(false, 0x86970002_09e177df_f7da36c0_9cb6fbb5);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xf7, cap.flags().0);
    assert_eq!(0xf7da36c0_9cb6fbb5, cap.value().0);
    assert_eq!(0xffda36c0_9cb6fbb5, cap.bounds_value().0);
    let base = 0x77d80000_00000000;
    let limit = 0xc9e00000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_0() {
    let cap = Morello::from_u128(false, 0xac7bbfff_b24fb246_ffef4d58_74c76484);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xffef4d58_74c76484, cap.value().0);
    assert_eq!(0xffef4d58_74c76484, cap.bounds_value().0);
    let base = 0xffef4d58_74c76480;
    let limit = 0xffef4d58_74c7e490;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_1() {
    let cap = Morello::from_u128(false, 0x002b4000_d5641565_884bc100_d6f31565);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(!cap.is_internal_exponent());
    assert_eq!(0x88, cap.flags().0);
    assert_eq!(0x884bc100_d6f31565, cap.value().0);
    assert_eq!(0x4bc100_d6f31565, cap.bounds_value().0);
    let base = 0x4bc100_d6f31565;
    let limit = 0x4bc100_d6f35564;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_2() {
    let cap = Morello::from_u128(false, 0x9ab3c000_db27db28_fffaa557_1e24db28);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(!cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xfffaa557_1e24db28, cap.value().0);
    assert_eq!(0xfffaa557_1e24db28, cap.bounds_value().0);
    let base = 0xfffaa557_1e24db28;
    let limit = 0xfffaa557_1e251b27;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_3() {
    let cap = Morello::from_u128(false, 0xd116bfff_aa77aa6f_714b7cd3_0043aa6e);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x71, cap.flags().0);
    assert_eq!(0x714b7cd3_0043aa6e, cap.value().0);
    assert_eq!(0x4b7cd3_0043aa6e, cap.bounds_value().0);
    let base = 0x4b7cd3_0043aa68;
    let limit = 0x4b7cd3_0043ea70;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_4() {
    let cap = Morello::from_u128(false, 0x3b288002_6429642a_dddf7920_71f5642a);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(!cap.is_internal_exponent());
    assert_eq!(0xdd, cap.flags().0);
    assert_eq!(0xdddf7920_71f5642a, cap.value().0);
    assert_eq!(0xffdf7920_71f5642a, cap.bounds_value().0);
    let base = 0xffdf7920_71f5642a;
    let limit = 0xffdf7920_71f5a429;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_5() {
    let cap = Morello::from_u128(false, 0xd62b0005_4b598b5a_41d396b8_b6a98b5a);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(!cap.is_internal_exponent());
    assert_eq!(0x41, cap.flags().0);
    assert_eq!(0x41d396b8_b6a98b5a, cap.value().0);
    assert_eq!(0xffd396b8_b6a98b5a, cap.bounds_value().0);
    let base = 0xffd396b8_b6a98b5a;
    let limit = 0xffd396b8_b6a9cb59;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_6() {
    let cap = Morello::from_u128(false, 0x29d34001_bd8f3d86_5e97864b_d3727b0c);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5e, cap.flags().0);
    assert_eq!(0x5e97864b_d3727b0c, cap.value().0);
    assert_eq!(0xff97864b_d3727b0c, cap.bounds_value().0);
    let base = 0xff97864b_d3727b00;
    let limit = 0xff97864b_d372fb10;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_7() {
    let cap = Morello::from_u128(false, 0x8156c005_0f4f4f46_3f41eba1_2a9e9e8b);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x3f, cap.flags().0);
    assert_eq!(0x3f41eba1_2a9e9e8b, cap.value().0);
    assert_eq!(0x41eba1_2a9e9e8b, cap.bounds_value().0);
    let base = 0x41eba1_2a9e9e80;
    let limit = 0x41eba1_2a9f1e90;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_8() {
    let cap = Morello::from_u128(false, 0x6d678001_983fd83f_49db9d4b_b9c4d838);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x49, cap.flags().0);
    assert_eq!(0x49db9d4b_b9c4d838, cap.value().0);
    assert_eq!(0xffdb9d4b_b9c4d838, cap.bounds_value().0);
    let base = 0xffdb9d4b_b9c4d838;
    let limit = 0xffdb9d4b_b9c51838;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_ie_lower_boundary_conditions_9() {
    let cap = Morello::from_u128(false, 0x38187fff_89870986_7ae17e15_59a4130b);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7a, cap.flags().0);
    assert_eq!(0x7ae17e15_59a4130b, cap.value().0);
    assert_eq!(0xffe17e15_59a4130b, cap.bounds_value().0);
    let base = 0xffe17e15_59a41300;
    let limit = 0xffe17e15_59a49300;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_0() {
    let cap = Morello::from_u128(false, 0x784b4005_2b990e85_fa0a469f_10011260);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xfa, cap.flags().0);
    assert_eq!(0xfa0a469f_10011260, cap.value().0);
    assert_eq!(0xa469f_10011260, cap.bounds_value().0);
    let base = 0x0_3a000000_00000000;
    let limit = 0x1_ae600000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_1() {
    let cap = Morello::from_u128(false, 0xe04a4001_a1a910d5_ff5366b3_399c779d);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff5366b3_399c779d, cap.value().0);
    assert_eq!(0x5366b3_399c779d, cap.bounds_value().0);
    let base = 0x0_43400000_00000000;
    let limit = 0x1_86a00000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_2() {
    let cap = Morello::from_u128(false, 0xdd17c002_0291180d_7a3ff893_fbef7b6f);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7a, cap.flags().0);
    assert_eq!(0x7a3ff893_fbef7b6f, cap.value().0);
    assert_eq!(0x3ff893_fbef7b6f, cap.bounds_value().0);
    let base = 0x60200000_00000000;
    let limit = 0x0a400000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    assert_eq!(None, cap.effective_bounds());
}

#[test]
fn high_exponents_3() {
    let cap = Morello::from_u128(false, 0xd0c5bfff_b2a10fc5_ff09df9e_d8fd37fb);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff09df9e_d8fd37fb, cap.value().0);
    assert_eq!(0x9df9e_d8fd37fb, cap.bounds_value().0);
    let base = 0x0_3f000000_00000000;
    let limit = 0x1_ca800000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_4() {
    let cap = Morello::from_u128(false, 0x83e50001_9f911a5d_7b6cf219_cdeecd18);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7b, cap.flags().0);
    assert_eq!(0x7b6cf219_cdeecd18, cap.value().0);
    assert_eq!(0x6cf219_cdeecd18, cap.bounds_value().0);
    let base = 0x0_69600000_00000000;
    let limit = 0x1_7e400000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_5() {
    let cap = Morello::from_u128(false, 0x9f648001_99c90976_5aeb4e47_0c57dcd5);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5a, cap.flags().0);
    assert_eq!(0x5aeb4e47_0c57dcd5, cap.value().0);
    assert_eq!(0xffeb4e47_0c57dcd5, cap.bounds_value().0);
    let base = 0x12e00000_00000000;
    let limit = 0xb3900000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_6() {
    let cap = Morello::from_u128(false, 0xca25c001_2231051e_ca3ddba6_d1ebd989);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(2), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xca, cap.flags().0);
    assert_eq!(0xca3ddba6_d1ebd989, cap.value().0);
    assert_eq!(0x3ddba6_d1ebd989, cap.bounds_value().0);
    let base = 0x0a300000_00000000;
    let limit = 0xc4600000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_7() {
    let cap = Morello::from_u128(false, 0x4ae08001_b4b12e6e_5fde418d_ef5c243f);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5f, cap.flags().0);
    assert_eq!(0x5fde418d_ef5c243f, cap.value().0);
    assert_eq!(0xffde418d_ef5c243f, cap.bounds_value().0);
    let base = 0x5cd00000_00000000;
    let limit = 0xe9600000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_8() {
    let cap = Morello::from_u128(false, 0xf33a8005_3ca91d9e_3b350838_261d2853);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x3b, cap.flags().0);
    assert_eq!(0x3b350838_261d2853, cap.value().0);
    assert_eq!(0x350838_261d2853, cap.bounds_value().0);
    let base = 0x3b300000_00000000;
    let limit = 0xf9500000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_9() {
    let cap = Morello::from_u128(false, 0xf1d35388_01211c4e_ff91ef72_f0d43392);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff91ef72_f0d43392, cap.value().0);
    assert_eq!(0xff91ef72_f0d43392, cap.bounds_value().0);
    let base = 0x0_38900000_00000000;
    let limit = 0x1_02400000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_10() {
    let cap = Morello::from_u128(false, 0x6a5b8005_25c91c07_1d04f80a_27eae53f);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x1d, cap.flags().0);
    assert_eq!(0x1d04f80a_27eae53f, cap.value().0);
    assert_eq!(0x4f80a_27eae53f, cap.bounds_value().0);
    let base = 0x1c000000_00000000;
    let limit = 0x65c80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_11() {
    let cap = Morello::from_u128(false, 0x98a38001_10491947_1946c91d_1cce3ba4);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(2), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x19, cap.flags().0);
    assert_eq!(0x1946c91d_1cce3ba4, cap.value().0);
    assert_eq!(0x46c91d_1cce3ba4, cap.bounds_value().0);
    let base = 0x19400000_00000000;
    let limit = 0x90480000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_12() {
    let cap = Morello::from_u128(false, 0x2fc58000_a33145f7_cdf61956_40c52a68);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xcd, cap.flags().0);
    assert_eq!(0xcdf61956_40c52a68, cap.value().0);
    assert_eq!(0xfff61956_40c52a68, cap.bounds_value().0);
    let base = 0x45f00000_00000000;
    let limit = 0xa3300000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_13() {
    let cap = Morello::from_u128(false, 0xdb2a4005_04c97457_7f50a90d_856e78cd);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7f, cap.flags().0);
    assert_eq!(0x7f50a90d_856e78cd, cap.value().0);
    assert_eq!(0x50a90d_856e78cd, cap.bounds_value().0);
    let base = 0x74500000_00000000;
    let limit = 0xc4c80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_14() {
    let cap = Morello::from_u128(false, 0x6d453fff_a14961df_ffdeabfa_baa431f1);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xffdeabfa_baa431f1, cap.value().0);
    assert_eq!(0xffdeabfa_baa431f1, cap.bounds_value().0);
    let base = 0x61d80000_00000000;
    let limit = 0xe1480000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_15() {
    let cap = Morello::from_u128(false, 0xec91d388_07d20048_7a2780e3_6e9d3a9c);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7a, cap.flags().0);
    assert_eq!(0x7a2780e3_6e9d3a9c, cap.value().0);
    assert_eq!(0x2780e3_6e9d3a9c, cap.bounds_value().0);
    let base = 0x00240000_00000000;
    let limit = 0x23e80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_16() {
    let cap = Morello::from_u128(false, 0x1983c000_87baff30_0b9a9c57_61da3b35);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xb, cap.flags().0);
    assert_eq!(0xb9a9c57_61da3b35, cap.value().0);
    assert_eq!(0xff9a9c57_61da3b35, cap.bounds_value().0);
    let base = 0x0_ff980000_00000000;
    let limit = 0x1_23dc0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_17() {
    let cap = Morello::from_u128(false, 0x4adbc005_085200d8_6c6f3f0c_6ff231c8);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x6c, cap.flags().0);
    assert_eq!(0x6c6f3f0c_6ff231c8, cap.value().0);
    assert_eq!(0x6f3f0c_6ff231c8, cap.bounds_value().0);
    let base = 0x006c0000_00000000;
    let limit = 0x24280000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_18() {
    let cap = Morello::from_u128(false, 0xb2218001_8ce20050_5b2b880f_88da127c);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5b, cap.flags().0);
    assert_eq!(0x5b2b880f_88da127c, cap.value().0);
    assert_eq!(0x2b880f_88da127c, cap.bounds_value().0);
    let base = 0x00280000_00000000;
    let limit = 0x26700000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_19() {
    let cap = Morello::from_u128(false, 0x2ffc0005_1eca00b0_ff595ccf_59fa545a);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff595ccf_59fa545a, cap.value().0);
    assert_eq!(0x595ccf_59fa545a, cap.bounds_value().0);
    let base = 0x00580000_00000000;
    let limit = 0x2f640000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_20() {
    let cap = Morello::from_u128(false, 0x14d65388_2762ff91_ffe53b75_196947df);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xffe53b75_196947df, cap.value().0);
    assert_eq!(0xffe53b75_196947df, cap.bounds_value().0);
    let base = 0x0_ffe40000_00000000;
    let limit = 0x1_19d80000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_21() {
    let cap = Morello::from_u128(false, 0x8de64005_3a120091_97257b12_efe4c168);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x97, cap.flags().0);
    assert_eq!(0x97257b12_efe4c168, cap.value().0);
    assert_eq!(0x257b12_efe4c168, cap.bounds_value().0);
    let base = 0x00240000_00000000;
    let limit = 0x1e840000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_22() {
    let cap = Morello::from_u128(false, 0x697fc005_11faffa1_5be90956_e95df2c2);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5b, cap.flags().0);
    assert_eq!(0x5be90956_e95df2c2, cap.value().0);
    assert_eq!(0xffe90956_e95df2c2, cap.bounds_value().0);
    let base = 0x0_ffe80000_00000000;
    let limit = 0x1_147e0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_23() {
    let cap = Morello::from_u128(false, 0x89641388_30b201c1_ff71744d_e2e3f588);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff71744d_e2e3f588, cap.value().0);
    assert_eq!(0x71744d_e2e3f588, cap.bounds_value().0);
    let base = 0x00700000_00000000;
    let limit = 0x1c2c0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_24() {
    let cap = Morello::from_u128(false, 0x4d518000_b9aaff51_5fd42877_577945e2);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5f, cap.flags().0);
    assert_eq!(0x5fd42877_577945e2, cap.value().0);
    assert_eq!(0xffd42877_577945e2, cap.bounds_value().0);
    let base = 0x0_ffd40000_00000000;
    let limit = 0x1_1e6a0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_25() {
    let cap = Morello::from_u128(false, 0x6d8dc002_9902017a_5a2f6425_81245ade);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5a, cap.flags().0);
    assert_eq!(0x5a2f6425_81245ade, cap.value().0);
    assert_eq!(0x2f6425_81245ade, cap.bounds_value().0);
    let base = 0x02f0000_00000000;
    let limit = 0xb200000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_26() {
    let cap = Morello::from_u128(false, 0xe7324005_1daa0282_5b503f33_ce39cd45);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5b, cap.flags().0);
    assert_eq!(0x5b503f33_ce39cd45, cap.value().0);
    assert_eq!(0x503f33_ce39cd45, cap.bounds_value().0);
    let base = 0x0500000_00000000;
    let limit = 0xbb50000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_27() {
    let cap = Morello::from_u128(false, 0x4f9d4002_376aff22_f6e47ff4_993c6ca9);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xf6, cap.flags().0);
    assert_eq!(0xf6e47ff4_993c6ca9, cap.value().0);
    assert_eq!(0xffe47ff4_993c6ca9, cap.bounds_value().0);
    let base = 0x0_ffe40000_00000000;
    let limit = 0x1_0eed0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_28() {
    let cap = Morello::from_u128(false, 0x102e8002_ac82ff5a_cceb38e4_b43caa47);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xcc, cap.flags().0);
    assert_eq!(0xcceb38e4_b43caa47, cap.value().0);
    assert_eq!(0xffeb38e4_b43caa47, cap.bounds_value().0);
    let base = 0x0_ffeb0000_00000000;
    let limit = 0x1_0d900000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_29() {
    let cap = Morello::from_u128(false, 0x994d8002_0ef20032_1d069ef2_091c817b);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x1d, cap.flags().0);
    assert_eq!(0x1d069ef2_091c817b, cap.value().0);
    assert_eq!(0x69ef2_091c817b, cap.bounds_value().0);
    let base = 0x0060000_00000000;
    let limit = 0x9de0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_30() {
    let cap = Morello::from_u128(false, 0xdeb0c000_b2d2ff33_45f37838_6102e00a);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(1), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x45, cap.flags().0);
    assert_eq!(0x45f37838_6102e00a, cap.value().0);
    assert_eq!(0xfff37838_6102e00a, cap.bounds_value().0);
    let base = 0x0_fff30000_00000000;
    let limit = 0x1_072d0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_31() {
    let cap = Morello::from_u128(false, 0xbd8e8002_2afa0483_cc483c7e_82463314);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xcc, cap.flags().0);
    assert_eq!(0xcc483c7e_82463314, cap.value().0);
    assert_eq!(0x483c7e_82463314, cap.bounds_value().0);
    let base = 0x0480000_00000000;
    let limit = 0x6af8000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_32() {
    let cap = Morello::from_u128(false, 0x2e384001_9cd2f863_01860707_c328f39d);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(3), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x1, cap.flags().0);
    assert_eq!(0x1860707_c328f39d, cap.value().0);
    assert_eq!(0xff860707_c328f39d, cap.bounds_value().0);
    let base = 0x0_ff860000_00000000;
    let limit = 0x1_05cd0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_33() {
    let cap = Morello::from_u128(false, 0x7b85c005_064af81b_5a81861f_546c5d2f);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5a, cap.flags().0);
    assert_eq!(0x5a81861f_546c5d2f, cap.value().0);
    assert_eq!(0xff81861f_546c5d2f, cap.bounds_value().0);
    let base = 0x0_ff818000_00000000;
    let limit = 0x1_04648000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_34() {
    let cap = Morello::from_u128(false, 0x0672c001_3ae207fb_477fae57_f90aede8);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(2), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x47, cap.flags().0);
    assert_eq!(0x477fae57_f90aede8, cap.value().0);
    assert_eq!(0x7fae57_f90aede8, cap.bounds_value().0);
    let base = 0x07f8000_00000000;
    let limit = 0x7ae0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_35() {
    let cap = Morello::from_u128(false, 0x65544002_b3faf734_7fb98aff_11d48b0d);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7f, cap.flags().0);
    assert_eq!(0x7fb98aff_11d48b0d, cap.value().0);
    assert_eq!(0xffb98aff_11d48b0d, cap.bounds_value().0);
    let base = 0x0_ffb98000_00000000;
    let limit = 0x1_039fc000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_36() {
    let cap = Morello::from_u128(false, 0x54e6c001_23420294_5a14af92_886ca2a3);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(2), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5a, cap.flags().0);
    assert_eq!(0x5a14af92_886ca2a3, cap.value().0);
    assert_eq!(0x14af92_886ca2a3, cap.bounds_value().0);
    let base = 0x0148000_00000000;
    let limit = 0x31a0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_37() {
    let cap = Morello::from_u128(false, 0x16450002_b45200c4_ec063616_206f58bc);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xec, cap.flags().0);
    assert_eq!(0xec063616_206f58bc, cap.value().0);
    assert_eq!(0x63616_206f58bc, cap.bounds_value().0);
    let base = 0x0060000_00000000;
    let limit = 0x3a28000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_38() {
    let cap = Morello::from_u128(false, 0xf3d7c005_3fbaf014_8a809189_1d090ed2);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x8a, cap.flags().0);
    assert_eq!(0x8a809189_1d090ed2, cap.value().0);
    assert_eq!(0xff809189_1d090ed2, cap.bounds_value().0);
    let base = 0x0_ff808000_00000000;
    let limit = 0x1_01fdc000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_39() {
    let cap = Morello::from_u128(false, 0xbb1a1388_348200f4_7b07b535_f0404a3f);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(10000), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7b, cap.flags().0);
    assert_eq!(0x7b07b535_f0404a3f, cap.value().0);
    assert_eq!(0x7b535_f0404a3f, cap.bounds_value().0);
    let base = 0x0078000_00000000;
    let limit = 0x3a40000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_40() {
    let cap = Morello::from_u128(false, 0x46de0001_24d2e615_459840f0_ad8967cf);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(2), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x45, cap.flags().0);
    assert_eq!(0x459840f0_ad8967cf, cap.value().0);
    assert_eq!(0xff9840f0_ad8967cf, cap.bounds_value().0);
    let base = 0x0_ff984000_00000000;
    let limit = 0x1_01934000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_41() {
    let cap = Morello::from_u128(false, 0x1a74c002_2842e7a5_239e89dd_ab587c8f);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(4), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x23, cap.flags().0);
    assert_eq!(0x239e89dd_ab587c8f, cap.value().0);
    assert_eq!(0xff9e89dd_ab587c8f, cap.bounds_value().0);
    let base = 0x0_ff9e8000_00000000;
    let limit = 0x1_00a10000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_42() {
    let cap = Morello::from_u128(false, 0x2e50c002_99820655_7b19430c_4cc73b90);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7b, cap.flags().0);
    assert_eq!(0x7b19430c_4cc73b90, cap.value().0);
    assert_eq!(0x19430c_4cc73b90, cap.bounds_value().0);
    let base = 0x0194000_00000000;
    let limit = 0x1660000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_43() {
    let cap = Morello::from_u128(false, 0x48febfff_b582f34d_ffcd3854_2ae608e3);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(32767), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xffcd3854_2ae608e3, cap.value().0);
    assert_eq!(0xffcd3854_2ae608e3, cap.bounds_value().0);
    let base = 0x0_ffcd2000_00000000;
    let limit = 0x1_00d60000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn high_exponents_44() {
    let cap = Morello::from_u128(false, 0x341bc002_8a7215c5_5b570813_20afe8a4);
    assert!(!cap.tag().0);
    assert!(cap.is_sealed());
    assert_eq!(Some(5), cap.object_type().0.map(|o| o.value()));
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5b, cap.flags().0);
    assert_eq!(0x5b570813_20afe8a4, cap.value().0);
    assert_eq!(0x570813_20afe8a4, cap.bounds_value().0);
    let base = 0x0570000_00000000;
    let limit = 0x229c000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_0() {
    let cap = Morello::from_u128(true, 0xda29c000_006a0295_330a47f5_97ab80ed);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x33, cap.flags().0);
    assert_eq!(0x330a47f5_97ab80ed, cap.value().0);
    assert_eq!(0xa47f5_97ab80ed, cap.bounds_value().0);
    let base = 0x00a4000_00000000;
    let limit = 0x201a000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_1() {
    let cap = Morello::from_u128(true, 0xd2a6c000_3532f266_e3e4c285_79cf7a02);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xe3, cap.flags().0);
    assert_eq!(0xe3e4c285_79cf7a02, cap.value().0);
    assert_eq!(0xffe4c285_79cf7a02, cap.bounds_value().0);
    let base = 0x0_ffe4c000_00000000;
    let limit = 0x1_006a6000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_2() {
    let cap = Morello::from_u128(true, 0x35268000_1953e6a8_7f735629_8ed00868);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7f, cap.flags().0);
    assert_eq!(0x7f735629_8ed00868, cap.value().0);
    assert_eq!(0x735629_8ed00868, cap.bounds_value().0);
    let base = 0x735400_00000000;
    let limit = 0xaca800_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_3() {
    let cap = Morello::from_u128(true, 0x3acc0000_270214f5_5b53d65f_45af1fc4);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5b, cap.flags().0);
    assert_eq!(0x5b53d65f_45af1fc4, cap.value().0);
    assert_eq!(0x53d65f_45af1fc4, cap.bounds_value().0);
    let base = 0x053c000_00000000;
    let limit = 0x19c0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_4() {
    let cap = Morello::from_u128(true, 0x3d434000_0b728507_05850496_ec26b32f);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5, cap.flags().0);
    assert_eq!(0x5850496_ec26b32f, cap.value().0);
    assert_eq!(0xff850496_ec26b32f, cap.bounds_value().0);
    let base = 0xff850000_00000000;
    let limit = 0xffcb7000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_5() {
    let cap = Morello::from_u128(true, 0x86b14000_0d92e6b5_df9ac05d_94657e48);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xdf, cap.flags().0);
    assert_eq!(0xdf9ac05d_94657e48, cap.value().0);
    assert_eq!(0xff9ac05d_94657e48, cap.bounds_value().0);
    let base = 0x0_ff9ac000_00000000;
    let limit = 0x1_01364000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_6() {
    let cap = Morello::from_u128(true, 0x27350000_03139251_5ae494c4_f59465de);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5a, cap.flags().0);
    assert_eq!(0x5ae494c4_f59465de, cap.value().0);
    assert_eq!(0xffe494c4_f59465de, cap.bounds_value().0);
    let base = 0x0_ffe49400_00000000;
    let limit = 0x1_0000c400_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_7() {
    let cap = Morello::from_u128(true, 0xc56a4000_2c523a5f_4b3a59d2_e0ab6ece);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x4b, cap.flags().0);
    assert_eq!(0x4b3a59d2_e0ab6ece, cap.value().0);
    assert_eq!(0x3a59d2_e0ab6ece, cap.bounds_value().0);
    let base = 0x3a5800_00000000;
    let limit = 0xac5000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_8() {
    let cap = Morello::from_u128(true, 0x10ca0000_0d12fc76_0cf8e1a1_4bd907ff);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xc, cap.flags().0);
    assert_eq!(0xcf8e1a1_4bd907ff, cap.value().0);
    assert_eq!(0xfff8e1a1_4bd907ff, cap.bounds_value().0);
    let base = 0x0_fff8e000_00000000;
    let limit = 0x1_009a2000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_above_limit_capabilities_9() {
    let cap = Morello::from_u128(true, 0x5fba4000_219a19cd_08672d83_39a28fdb);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x8, cap.flags().0);
    assert_eq!(0x8672d83_39a28fdb, cap.value().0);
    assert_eq!(0x672d83_39a28fdb, cap.bounds_value().0);
    let base = 0x0672000_00000000;
    let limit = 0x1866000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_0() {
    let cap = Morello::from_u128(true, 0xb998c000_1ad2f7ed_01dfada6_ed9a6e89);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x1, cap.flags().0);
    assert_eq!(0x1dfada6_ed9a6e89, cap.value().0);
    assert_eq!(0xffdfada6_ed9a6e89, cap.bounds_value().0);
    let base = 0x0_ffdfa000_00000000;
    let limit = 0x1_016b4000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_1() {
    let cap = Morello::from_u128(true, 0x26014000_0b3213bd_594ef764_5c18811b);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x59, cap.flags().0);
    assert_eq!(0x594ef764_5c18811b, cap.value().0);
    assert_eq!(0x4ef764_5c18811b, cap.bounds_value().0);
    let base = 0x04ee000_00000000;
    let limit = 0x22cc000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_2() {
    let cap = Morello::from_u128(true, 0x3df3c000_043bf150_ff78aac4_9795c795);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xff, cap.flags().0);
    assert_eq!(0xff78aac4_9795c795, cap.value().0);
    assert_eq!(0x78aac4_9795c795, cap.bounds_value().0);
    let base = 0x78a800_00000000;
    let limit = 0xa21c00_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_3() {
    let cap = Morello::from_u128(true, 0x9a708000_2fca10ae_6f215664_bb0aba7e);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x6f, cap.flags().0);
    assert_eq!(0x6f215664_bb0aba7e, cap.value().0);
    assert_eq!(0x215664_bb0aba7e, cap.bounds_value().0);
    let base = 0x215000_00000000;
    let limit = 0xdf9000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_4() {
    let cap = Morello::from_u128(true, 0x162bc000_16d2ffee_7fffd167_063f4937);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7f, cap.flags().0);
    assert_eq!(0x7fffd167_063f4937, cap.value().0);
    assert_eq!(0xffffd167_063f4937, cap.bounds_value().0);
    let base = 0x0_ffffd000_00000000;
    let limit = 0x1_00ada000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_5() {
    let cap = Morello::from_u128(true, 0xfd89c000_25fa0735_c41cc31f_89bc9c0e);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0xc4, cap.flags().0);
    assert_eq!(0xc41cc31f_89bc9c0e, cap.value().0);
    assert_eq!(0x1cc31f_89bc9c0e, cap.bounds_value().0);
    let base = 0x01cc000_00000000;
    let limit = 0x197e000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_6() {
    let cap = Morello::from_u128(true, 0x452d4000_3ec2e39d_7a8e7b00_261fda92);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x7a, cap.flags().0);
    assert_eq!(0x7a8e7b00_261fda92, cap.value().0);
    assert_eq!(0xff8e7b00_261fda92, cap.bounds_value().0);
    let base = 0x0_ff8e6000_00000000;
    let limit = 0x1_00fb0000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_7() {
    let cap = Morello::from_u128(true, 0x377b0000_337a1fd7_5f1fd4b5_09f3d37e);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x5f, cap.flags().0);
    assert_eq!(0x5f1fd4b5_09f3d37e, cap.value().0);
    assert_eq!(0x1fd4b5_09f3d37e, cap.bounds_value().0);
    let base = 0x1fd000_00000000;
    let limit = 0x737800_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_8() {
    let cap = Morello::from_u128(true, 0x4b740000_3a4a1435_6150cefb_7aae8a4a);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x61, cap.flags().0);
    assert_eq!(0x6150cefb_7aae8a4a, cap.value().0);
    assert_eq!(0x50cefb_7aae8a4a, cap.bounds_value().0);
    let base = 0x050c000_00000000;
    let limit = 0x1e92000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn values_below_base_capabilities_9() {
    let cap = Morello::from_u128(true, 0xe1a14000_1e7a3457_1b3453ca_59fd87c3);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x1b, cap.flags().0);
    assert_eq!(0x1b3453ca_59fd87c3, cap.value().0);
    assert_eq!(0x3453ca_59fd87c3, cap.bounds_value().0);
    let base = 0x345000_00000000;
    let limit = 0x9e7800_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}
