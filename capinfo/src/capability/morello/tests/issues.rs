// Copyright (c) 2022 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[cfg(not(test))]
compile_error!("Test modules should only build with tests enabled.");

use super::*;

#[test]
fn issue_2_cb_calc() {
    // https://git.morello-project.org/morello/utilities/capinfo/-/issues/2
    let mut cap = Morello::try_from("0x1:dc1040004001ffff:80030000").unwrap();
    cap.verbosity_mut().set_fancy_bounds(true);
    let bounds = cap.bounds();
    let cf_expl = bounds
        .1
        .iter()
        .find(|expl| expl.title().as_raw_str() == "Correction Factors")
        .expect("Explanation missing");
    let cb_desc = cf_expl
        .elements()
        .map(|elem| elem.to_string())
        .find(|s| s.starts_with("c_b = "))
        .expect("c_b Element missing");
    println!("{cb_desc}");
    assert!(cb_desc.ends_with(" = 0 - 1 = -1"));
}
