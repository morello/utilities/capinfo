// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[cfg(not(test))]
compile_error!("Test modules should only build with tests enabled.");

use super::*;

#[test]
fn nullptr() {
    let cap = Morello::nullptr();
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0, cap.value().0);
    assert_eq!(0x0, cap.bounds_value().0);
    assert_eq!(Exponent::Max, cap.exponent().0);
    assert_eq!(50, cap.exponent().0.get());
    let base = 0x0_00000000_00000000;
    let limit = 0x1_00000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn default_ddc() {
    let cap = Morello::from_u128(true, 0xffffc000_00010005_00000000_00000000);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0, cap.value().0);
    assert_eq!(0x0, cap.bounds_value().0);
    // This is the highest valid exponent (except for 63; see `nullptr()`).
    assert_eq!(Exponent::Normal(50), cap.exponent().0);
    let base = 0x0_00000000_00000000;
    let limit = 0x1_00000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn regress_t_mask() {
    // Set B<15:14> to make sure that they're properly propagated to T<15:14>.
    let cap = Morello::from_u128(true, 0xffffc000_60a05e80_0000007f_d5666100);
    assert!(cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(!cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0000007f_d5666100, cap.value().0);
    assert_eq!(0x0000007f_d5666100, cap.bounds_value().0);
    assert_eq!(Exponent::Normal(0), cap.exponent().0);
    let base = 0x0_0000007f_d5665e80;
    let limit = 0x0_0000007f_d56660a0;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = u64::try_from(limit - 1).unwrap();
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_51() {
    // The lowest invalid exponent.
    let cap = Morello::from_u128(false, 0xffffc000_34511234_0000007f_d5666100);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0000007f_d5666100, cap.value().0);
    assert_eq!(0x0000007f_d5666100, cap.bounds_value().0);
    assert_eq!(Exponent::Invalid(51), cap.exponent().0);
    let base = 0x0_00000000_00000000;
    let limit = 0x1_00000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}

#[test]
fn exp_62() {
    // The highest invalid exponent.
    let cap = Morello::from_u128(false, 0xffffc000_34501231_0000007f_d5666100);
    assert!(!cap.tag().0);
    assert!(!cap.is_sealed());
    assert_eq!(None, cap.object_type().0);
    assert!(cap.is_internal_exponent());
    assert_eq!(0x0, cap.flags().0);
    assert_eq!(0x0000007f_d5666100, cap.value().0);
    assert_eq!(0x0000007f_d5666100, cap.bounds_value().0);
    assert_eq!(Exponent::Invalid(62), cap.exponent().0);
    let base = 0x0_00000000_00000000;
    let limit = 0x1_00000000_00000000;
    assert_eq!(base, cap.base());
    assert_eq!(limit, cap.limit());
    assert_eq!(base..limit, cap.bounds().0);
    let min = u64::try_from(base).unwrap();
    let max = 0xffffffff_ffffffff;
    assert_eq!(Some(min..=max), cap.effective_bounds());
}
