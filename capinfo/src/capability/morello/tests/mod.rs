// Copyright (c) 2021-2022 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[cfg(not(test))]
compile_error!("Test modules should only build with tests enabled.");

mod atgve;
mod caps;
mod issues;
mod parse_errors;

use super::*;

#[test]
fn nullptr() {
    let cap = Morello::nullptr();
    assert_eq!((false, 0x00000000_00000000_00000000_00000000), cap.raw());
}

#[test]
fn from_u128() {
    let cap = Morello::from_u128(true, 0x12345678_aabbccdd_abcdef01_00112233);
    assert_eq!((true, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
}

#[test]
fn from_u64_parts() {
    let cap = Morello::from_u64_parts(true, 0x12345678_aabbccdd, 0xabcdef01_00112233);
    assert_eq!((true, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
}

#[test]
fn from_u32_parts() {
    let cap = Morello::from_u32_parts(true, 0x12345678, 0xaabbccdd, 0xabcdef01, 0x00112233);
    assert_eq!((true, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
}

#[test]
fn from_str_lifetime() {
    // The str is not required to live as long as the capability.
    let cap = {
        let s = String::from("0x112345678aabbccddabcdef0100112233");
        Morello::try_from(s.as_str()).unwrap()
    };
    assert_eq!((true, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
}

#[test]
fn from_str_simple() {
    let cap = Morello::try_from("0x012345678aabbccddabcdef0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Mixed capitalisation.
    let cap = Morello::try_from("0X012345678aabBCcddABcdeF0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // The leading "0x" is optional.
    let cap = Morello::try_from("012345678aabbccddabcdef0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Underscores, commas and apostrophes are ignored.
    let cap = Morello::try_from("0x__1_1234_5678aa,bbc,,cdda'bcd'ef01_0011223_3'").unwrap();
    assert_eq!((true, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
}

#[test]
fn from_str_three_fields() {
    // Bar ('|') separators.
    let cap = Morello::try_from("0x0|12345678aabbccdd|abcdef0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Colon (':') separators.
    let cap = Morello::try_from("0x0:12345678aabbccdd:abcdef0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Mixed capitalisation.
    let cap = Morello::try_from("0X0:12345678aabBCcdd:ABcdeF0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // The leading "0x" is optional.
    let cap = Morello::try_from("0:12345678aabbccdd:abcdef0100112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Short fields.
    let cap = Morello::try_from("0x1:1234:abcd").unwrap();
    assert_eq!((true, 0x00000000_00001234_00000000_0000abcd), cap.raw());
    let cap = Morello::try_from("0x1:123423453456:abcd").unwrap();
    assert_eq!((true, 0x00001234_23453456_00000000_0000abcd), cap.raw());
    let cap = Morello::try_from("0x1::abcd").unwrap();
    assert_eq!((true, 0x00000000_00000000_00000000_0000abcd), cap.raw());
    // Underscores, commas and apostrophes are ignored.
    let cap = Morello::try_from("0x1:123__42'3453456,:,,abcd'").unwrap();
    assert_eq!((true, 0x00001234_23453456_00000000_0000abcd), cap.raw());
}

#[test]
fn from_str_five_fields() {
    // Bar ('|') separators.
    let cap = Morello::try_from("0x0|12345678|aabbccdd|abcdef01|00112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Colon (':') separators.
    let cap = Morello::try_from("0x0:12345678:aabbccdd:abcdef01:00112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Mixed capitalisation.
    let cap = Morello::try_from("0X0:12345678:aabBCcdd:ABcdeF01:00112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // The leading "0x" is optional.
    let cap = Morello::try_from("0:12345678:aabbccdd:abcdef01:00112233").unwrap();
    assert_eq!((false, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    // Short fields.
    let cap = Morello::try_from("0x1:1234:2345:abcd:bcde").unwrap();
    assert_eq!((true, 0x00001234_00002345_0000abcd_0000bcde), cap.raw());
    let cap = Morello::try_from("0x1:1234::abcd:bcde").unwrap();
    assert_eq!((true, 0x00001234_00000000_0000abcd_0000bcde), cap.raw());
    // Underscores, commas and apostrophes are ignored.
    let cap = Morello::try_from("0x_1:1234_:23'45:,,:bcde").unwrap();
    assert_eq!((true, 0x00001234_00002345_00000000_0000bcde), cap.raw());
}

#[test]
fn from_str_empty_fields() {
    // Empty fields are treated as zero.
    let cap = Morello::try_from("0x1:1234:").unwrap();
    assert_eq!((true, 0x00000000_00001234_00000000_00000000), cap.raw());
    let cap = Morello::try_from("0x1::abcd").unwrap();
    assert_eq!((true, 0x00000000_00000000_00000000_0000abcd), cap.raw());
    let cap = Morello::try_from(":1234:abcd").unwrap();
    assert_eq!((false, 0x00000000_00001234_00000000_0000abcd), cap.raw());
    let cap = Morello::try_from("0x:1234:abcd").unwrap();
    assert_eq!((false, 0x00000000_00001234_00000000_0000abcd), cap.raw());
    let cap = Morello::try_from("::").unwrap();
    assert_eq!((false, 0), cap.raw());

    let cap = Morello::try_from("0x1:1234:2345:abcd:").unwrap();
    assert_eq!((true, 0x00001234_00002345_0000abcd_00000000), cap.raw());
    let cap = Morello::try_from("0x1:1234:::").unwrap();
    assert_eq!((true, 0x00001234_00000000_00000000_00000000), cap.raw());
    let cap = Morello::try_from(":1234:2345:abcd:bcde").unwrap();
    assert_eq!((false, 0x00001234_00002345_0000abcd_0000bcde), cap.raw());
    let cap = Morello::try_from("::::").unwrap();
    assert_eq!((false, 0), cap.raw());
}

#[test]
fn raw() {
    // We already rely on `raw()` to test the constructors, but check that the
    // `raw_*_parts()` methods agree.
    let cap = Morello::from_u128(true, 0x12345678_aabbccdd_abcdef01_00112233);
    assert_eq!((true, 0x12345678_aabbccdd_abcdef01_00112233), cap.raw());
    assert_eq!(
        (true, 0x12345678_aabbccdd, 0xabcdef01_00112233),
        cap.raw_u64_parts()
    );

    let cap = Morello::nullptr();
    assert_eq!((false, 0x00000000_00000000_00000000_00000000), cap.raw());
    assert_eq!(
        (false, 0x00000000_00000000, 0x00000000_00000000),
        cap.raw_u64_parts()
    );
}

#[test]
fn size() {
    assert_eq!(128, Morello::nullptr().msb());
    assert_eq!(129, Morello::nullptr().size_in_bits());
}

#[test]
fn bit() {
    let cap = Morello::from_u128(false, 0x12345678_aabbccdd_abcdef01_00112233);
    assert_eq!(Some(true), cap.bit(0));
    assert_eq!(Some(true), cap.bit(1));
    assert_eq!(Some(false), cap.bit(2));
    assert_eq!(Some(false), cap.bit(127));
    assert_eq!(Some(false), cap.bit(128));
    assert_eq!(None, cap.bit(129));
}

#[test]
fn nibble() {
    let cap = Morello::from_u128(false, 0x12345678_aabbccdd_abcdef01_00112233);
    assert_eq!(Some(0x3), cap.nibble(0));
    assert_eq!(Some(0x3), cap.nibble(1));
    assert_eq!(Some(0x2), cap.nibble(2));
    assert_eq!(Some(0x1), cap.nibble(31));
    assert_eq!(Some(0x0), cap.nibble(32));
    assert_eq!(None, cap.nibble(33));
}

#[test]
fn bits_u64() {
    // `bits_u64()` is the basis of many other helpers, so just test some corner-cases.
    let cap = Morello::from_u128(true, 0x12345678_aabbccdd_abcdef01_00112233);
    assert_eq!(0x1122, cap.bits_u64(Field::from_bits(23, 8)));
    assert_eq!(0xdda, cap.bits_u64(Field::from_bits(71, 60)));
    assert_eq!(0x11234, cap.bits_u64(Field::from_bits(128, 112)));
    assert_eq!(0x11234, cap.bits_u64(Field::from_bits(131, 112)));
    assert_eq!(0x11234, cap.bits_u64(Field::from_bits(175, 112)));
    assert_eq!(2, cap.bits_u64(Field::from_bits(128, 127)));
    assert_eq!(1, cap.bits_u64(Field::from_bits(128, 128)));
    assert_eq!(0, cap.bits_u64(Field::from_bits(127, 128)));
    assert_eq!(0, cap.bits_u64(Field::from_bits(129, 130)));
}

#[test]
fn fields() {
    // `fields()` should not overlap, and should lie within the capability.
    // Also, Morello capabilities have no unused bits, so every bit should be covered.
    let caps = [
        Morello::from_u128(true, 0x12345678_eabbccdd_abcdef01_00112233),
        Morello::from_u128(true, 0x12345678_aabbccdd_abcdef01_00112233),
    ];
    assert!(!caps[0].is_internal_exponent());
    assert!(caps[1].is_internal_exponent());

    for cap in &caps {
        let mut seen_tag = false;
        let mut seen = 0u128;
        for described_field in cap.fields() {
            let field = described_field.field();
            assert!(field.size_in_bits() > 0);
            assert!(field.msb() <= cap.msb());
            if field.lsb() < 128 {
                // No field spans both the tag and the 128-bit value.
                assert!(field.msb() < 128);
                let mask = (field.shifted_mask_u64() as u128) << field.lsb();
                assert!((mask & seen) == 0);
                seen |= mask;
            } else {
                assert!(field.bit() == 128);
                assert!(!seen_tag);
                seen_tag = true;
            }
        }
        assert!(seen_tag);
        assert!(seen == !0u128);
    }
}
