// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[cfg(not(test))]
compile_error!("Test modules should only build with tests enabled.");

use crate::capability::error::ParseError;

use super::*;

// Most error variants include one or more `FlaggedChars` instances, to highlight the errors. For
// readability, we check those by formatting them, and comparing the string.

fn too_few_digits(s: &str, flagged: &str) {
    match Morello::try_from(s).unwrap_err() {
        ParseError::TooFewDigits { found, min } => {
            assert_eq!(flagged, format!("{}", found));
            assert_eq!(33, min);
        }
        _ => panic!("Wrong error type."),
    }
}

fn too_many_digits(s: &str, flagged: &str) {
    match Morello::try_from(s).unwrap_err() {
        ParseError::TooManyDigits { found, max } => {
            assert_eq!(flagged, format!("{}", found));
            assert_eq!(33, max);
        }
        _ => panic!("Wrong error type."),
    }
}

fn invalid_characters(s: &str, flagged: &str) {
    match Morello::try_from(s).unwrap_err() {
        ParseError::InvalidCharacters { at } => {
            assert_eq!(flagged, format!("{}", at));
        }
        _ => panic!("Wrong error type."),
    }
}

fn wrong_number_of_fields(s: &str, flagged: Vec<&str>) {
    match Morello::try_from(s).unwrap_err() {
        ParseError::WrongNumberOfFields { fields, allowed } => {
            // TODO: Use `eq_by` once it's stable.
            assert_eq!(fields.len(), flagged.len());
            for (expected, found) in Iterator::zip(flagged.into_iter(), fields.into_iter()) {
                assert_eq!(expected, &format!("{}", found));
            }
            assert_eq!(vec![1, 3, 5], allowed);
        }
        _ => panic!("Wrong error type."),
    }
}

fn too_many_digits_in_fields(s: &str, flagged: Vec<(&str, usize)>) {
    match Morello::try_from(s).unwrap_err() {
        ParseError::TooManyDigitsInFields(fields) => {
            // TODO: Use `eq_by` once it's stable.
            assert_eq!(fields.len(), flagged.len());
            for (expected, found) in Iterator::zip(flagged.into_iter(), fields.into_iter()) {
                // Check the expected max digits.
                assert_eq!(expected.1, found.1);
                // Check the flags.
                assert_eq!(expected.0, &format!("{}", found.0));
            }
        }
        _ => panic!("Wrong error type."),
    }
}

#[test]
fn from_str_simple_fail() {
    too_few_digits(
        "0x12345678aabbccddabcdef0100112233",
        "  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
    );
    too_few_digits(
        "0x12345678aabbccdd_abcdef0100112233",
        "  ^^^^^^^^^^^^^^^^ ^^^^^^^^^^^^^^^^",
    );
    too_many_digits(
        "0x0012345678aabbccddabcdef0100112233",
        "  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
    );
    too_few_digits(
        "12345678aabbccddabcdef0100112233",
        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
    );
    too_few_digits(
        "12345678aabbccdd_abcdef0100112233",
        "^^^^^^^^^^^^^^^^ ^^^^^^^^^^^^^^^^",
    );
    too_many_digits(
        "0012345678aabbccddabcdef0100112233",
        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
    );

    // The tag digit must be 0 or 1.
    invalid_characters("f12345678aabbccddabcdef0100112233", "^");
    invalid_characters("212345678aabbccddabcdef0100112233", "^");
}

#[test]
fn from_str_fields_fail() {
    wrong_number_of_fields("0:0", vec!["^", "  ^"]);
    wrong_number_of_fields("0:0:0:0", vec!["^", "  ^", "    ^", "      ^"]);
    wrong_number_of_fields(
        "0:0:0:0:0:0",
        vec!["^", "  ^", "    ^", "      ^", "        ^", "          ^"],
    );
}

#[test]
fn from_str_three_fields_fail() {
    // TODO: Maybe use a macro, so we can format these better.
    too_many_digits_in_fields(
        "0x1|12345678aabbccdd|abcdef0100112233F",
        vec![("                     ^^^^^^^^^^^^^^^^^", 16)],
    );
    too_many_digits_in_fields(
        "0x1|12345678aabbccddF|abcdef0100112233",
        vec![("    ^^^^^^^^^^^^^^^^^", 16)],
    );
    too_many_digits_in_fields("0x00|12345678aabbccdd|abcdef0100112233", vec![("  ^^", 1)]);
    too_many_digits_in_fields(
        "0x01|012345678aabbccdd|0abcdef0100112233",
        vec![
            ("  ^^", 1),
            ("     ^^^^^^^^^^^^^^^^^", 16),
            ("                       ^^^^^^^^^^^^^^^^^", 16),
        ],
    );

    invalid_characters("0x2|12345678aabbccdd|abcdef0100112233", "  ^");
}

#[test]
fn from_str_five_fields_fail() {
    // TODO: Maybe use a macro, so we can format these better.
    too_many_digits_in_fields(
        "0x1|12345678|aabbccdd|abcdef01|00112233F",
        vec![("                               ^^^^^^^^^", 8)],
    );
    too_many_digits_in_fields(
        "0x1|12345678|aabbccdd|abcdef01F|00112233",
        vec![("                      ^^^^^^^^^", 8)],
    );
    too_many_digits_in_fields(
        "0x1|12345678|aabbccddF|abcdef01|00112233",
        vec![("             ^^^^^^^^^", 8)],
    );
    too_many_digits_in_fields(
        "0x1|12345678F|aabbccdd|abcdef01|00112233",
        vec![("    ^^^^^^^^^", 8)],
    );
    too_many_digits_in_fields(
        "0x00|12345678|aabbccdd|abcdef01|00112233",
        vec![("  ^^", 1)],
    );

    invalid_characters("0x2|12345678|aabbccdd|abcdef01|00112233", "  ^");
}

#[test]
fn from_str_empty_fail() {
    assert_eq!(Morello::try_from("").unwrap_err(), ParseError::Empty);
    assert_eq!(Morello::try_from("_,'").unwrap_err(), ParseError::Empty);
    assert_eq!(Morello::try_from("0x").unwrap_err(), ParseError::Empty);
    assert_eq!(Morello::try_from("0X").unwrap_err(), ParseError::Empty);
}

#[test]
fn from_str_unicode_fail() {
    invalid_characters(
        // Check that error markers align with UTF-8 multi-byte characters.
        //
        //            e5           e9               <- Code points
        //          c3 a5        c3 a9              <- UTF-8 bytes
        //            |            |
        "0x1|12345678|åabbccdd|abcdéf01|0011223x",
        "             ^            ^           ^",
    );

    invalid_characters(
        // Check that error markers align with multi-code-point characters.
        //
        // 0ba8 0bbf           6f 0308 0332         <- Code points
        // e0 ae a8 e0 ae bf   6f cc 88 cc b2       <- UTF-8 bytes
        // |          _________/
        // |         /
        // |        | 61 0310      65 0301          <- Code points
        // |        | 61 cc 90     65 cc 81         <- UTF-8 bytes
        // |        | |            |
        "0xநி|1234567ö̲|a̐abbccdd|abcdéf01|0011223x",
        "  ^        ^ ^            ^           ^",
    );
}
