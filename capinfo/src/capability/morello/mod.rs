// Copyright (c) 2021-2022 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[cfg(test)]
mod tests;

use static_assertions::const_assert;
use std::{convert::TryFrom, fmt, fmt::Display, iter, ops::Range};
use unicode_segmentation::UnicodeSegmentation;

use crate::{
    bitwise::Bitwise,
    capability::{
        described_field::{Category, DescribedField},
        error::{FlaggedChars, ParseError},
        explanation,
        explanation::{
            composed_value::ComposedValueBuilder, explained_value::ExplainedValueBuilder,
            permissions::Permissions, Explanation, Verbosity,
        },
        Capability,
    },
    field::{Field, FieldWithValue},
    field_set,
    rich_text::RichText,
};

// Layout constants.
// The names are loosely based on constants from the shared pseudocode.
const CAP_IE: Field = Field::from_bit(94);
const CAP_LIMIT: Field = Field::from_bits(93, 80);
const CAP_BASE: Field = Field::from_bits(79, 64);
const CAP_OTYPE: Field = Field::from_bits(109, 95);
const CAP_PERMS: Field = Field::from_bits(127, 110);
const CAP_TAG: Field = Field::from_bit(128);
const CAP_FLAGS: Field = Field::from_bits(63, 56);
const CAP_VALUE_FOR_BOUNDS: Field = Field::from_bits(55, 0);

// Breakdowns and compound fields.
const CAP_BOUNDS: Field = Field::from_bits(CAP_IE.msb(), CAP_BASE.lsb());
const CAP_BASE_EXP: Field = Field::from_bits(CAP_BASE.lsb() + 2, CAP_BASE.lsb());
const CAP_BASE_IE: Field = Field::from_bits(CAP_BASE.msb(), CAP_BASE_EXP.msb() + 1);
const CAP_LIMIT_EXP: Field = Field::from_bits(CAP_LIMIT.lsb() + 2, CAP_LIMIT.lsb());
const CAP_LIMIT_IE: Field = Field::from_bits(CAP_LIMIT.msb(), CAP_LIMIT_EXP.msb() + 1);
const CAP_VALUE: Field = Field::from_bits(CAP_FLAGS.msb(), CAP_VALUE_FOR_BOUNDS.lsb());

const CAP_MW: u32 = CAP_BASE.size_in_bits();
const CAP_MAX_EXPONENT: u32 = 50;
const CAP_BOUND_MIN: u64 = 0;
const CAP_BOUND_MAX: u128 = 1 << 64;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Morello {
    tag: bool,
    raw: u128,
    verbosity: Verbosity,
}

impl Morello {
    pub fn nullptr() -> Self {
        Self::from_u128(false, 0)
    }

    pub fn from_u128(tag: bool, raw: u128) -> Self {
        Self {
            tag,
            raw,
            verbosity: Verbosity::default(),
        }
    }

    pub fn from_u64_parts(tag: bool, high64: u64, low64: u64) -> Self {
        Self::from_u128(tag, (u128::from(high64) << 64) | (u128::from(low64)))
    }

    pub fn from_u32_parts(tag: bool, word3: u32, word2: u32, word1: u32, word0: u32) -> Self {
        Self::from_u128(
            tag,
            (u128::from(word3) << 96)
                | (u128::from(word2) << 64)
                | (u128::from(word1) << 32)
                | (u128::from(word0)),
        )
    }

    fn bits_u64(&self, field: Field) -> u64 {
        assert!(field.size_in_bits() <= 64);
        let tag_shift = CAP_TAG.lsb().wrapping_sub(field.lsb());
        let high = (self.tag as u64).checked_shl(tag_shift).unwrap_or(0);
        let low = self.raw.checked_shr(field.lsb()).unwrap_or(0) as u64;
        (high | low) & field.shifted_mask_u64()
    }

    fn bits_u32(&self, field: Field) -> u32 {
        assert!(field.size_in_bits() <= 32);
        let tag_shift = CAP_TAG.lsb().wrapping_sub(field.lsb());
        let high = (self.tag as u32).checked_shl(tag_shift).unwrap_or(0);
        let low = self.raw.checked_shr(field.lsb()).unwrap_or(0) as u32;
        (high | low) & field.shifted_mask_u32()
    }

    /// Extract the tag bit (as a bool) and the raw 128-bit capability
    /// representation as a tuple (tag, raw).
    pub fn raw(&self) -> (bool, u128) {
        (self.tag, self.raw)
    }

    /// Extract the tag bit (as a bool) and the raw 128-bit capability
    /// representation, split into high and low 64-bit parts, as a tuple (tag,
    /// high64, low64).
    pub fn raw_u64_parts(&self) -> (bool, u64, u64) {
        (self.tag, (self.raw >> 64) as u64, self.raw as u64)
    }

    /// Extract the tag bit (as a bool) and the raw 128-bit capability
    /// representation, split into 32-bit parts, as a tuple (tag, word3, word2, word1, word0).
    pub fn raw_u32_parts(&self) -> (bool, u32, u32, u32, u32) {
        (
            self.tag,
            (self.raw >> 96) as u32,
            (self.raw >> 64) as u32,
            (self.raw >> 32) as u32,
            self.raw as u32,
        )
    }

    pub fn mantissa_width(&self) -> u32 {
        CAP_MW
    }

    pub fn is_internal_exponent(&self) -> bool {
        !self.bit(CAP_IE.bit()).unwrap()
    }

    pub fn exponent(&self) -> (Exponent, impl Iterator<Item = explanation::Element>) {
        let element;
        let e;
        let name = RichText::from("`E");
        if self.is_internal_exponent() {
            let high = self.bits_u64(CAP_LIMIT_EXP) << CAP_BASE_EXP.size_in_bits();
            let low = self.bits_u64(CAP_BASE_EXP);
            let n_exp = u32::try_from(high | low).unwrap();
            let enc_exp = n_exp ^ 0b111111;
            match enc_exp {
                0b111111 => {
                    e = Exponent::Max;
                    let value = RichText::from(format!("`{}", CAP_MAX_EXPONENT));
                    element = ExplainedValueBuilder::new(value)
                        .with_name(name)
                        .with_reasoning(RichText::from(
                            ", as a special case, encoded as `0b000000`.",
                        ));
                }
                enc if enc > CAP_MAX_EXPONENT => {
                    e = Exponent::Invalid(enc);
                    let value = RichText::from(format!("`{}", CAP_MAX_EXPONENT));
                    element = ExplainedValueBuilder::new(value)
                        .with_name(name)
                        .with_reasoning(RichText::from(format!(
                        ", because the encoded value (`{}`) exceeds the maximum exponent (`{}`).",
                        enc, CAP_MAX_EXPONENT
                    )));
                }
                enc => {
                    e = Exponent::Normal(enc);
                    let value = RichText::from(format!("`{}", enc));
                    element = ExplainedValueBuilder::new(value)
                        .with_name(name)
                        .with_intermediate(RichText::from(format!("`~0b{:06b}", n_exp)));
                }
            }
        } else {
            e = Exponent::Normal(0);
            element = ExplainedValueBuilder::new(RichText::from("`0"))
                .with_name(name)
                .with_reasoning(self.ie_reasoning());
        };
        (e, iter::once(element.build().into()))
    }

    fn ie_reasoning(&self) -> RichText<&'static str> {
        if self.is_internal_exponent() {
            RichText::from(", because the capability has an internal exponent.")
        } else {
            RichText::from(", because the capability does not have an internal exponent.")
        }
    }

    /// The adjusted 'B' field.
    fn bottom(&self) -> (u32, impl Iterator<Item = explanation::Element>) {
        const WHOLE: Field = Field::from_bits(CAP_BASE.size_in_bits() - 1, 0);
        let lsb_enc = match self.is_internal_exponent() {
            false => 0,
            true => CAP_BASE_EXP.size_in_bits(),
        };
        let b = (self.bits_u32(CAP_BASE) >> lsb_enc) << lsb_enc;
        let encoded = Field::from_bits(WHOLE.msb(), lsb_enc);
        let mut cv = ComposedValueBuilder::new(WHOLE.with_value(b))
            .with_rich_name(format!("`B{}", WHOLE))
            .with_category(Category::BoundsBase);
        if WHOLE != encoded {
            // B<15:0> = B<15:3>:0b000 = ...
            cv.add_rich_subfield(format!("`B{}", encoded), encoded, Category::BoundsBase);
        }
        (b, iter::once(cv.build().into()))
    }

    /// The adjusted 'B' and 'T' fields.
    fn bottom_top(&self) -> (u32, u32, impl Iterator<Item = explanation::Element>) {
        const B_MSB: u32 = CAP_BASE.size_in_bits() - 1;
        const T_MSB: u32 = CAP_LIMIT.size_in_bits() - 1;

        const_assert!(T_MSB < B_MSB);

        const MASK: u32 = (1 << CAP_BASE.size_in_bits()) - 1; // <15:0>
        const LOW_MASK: u32 = (1 << CAP_LIMIT.size_in_bits()) - 1; // <13:0>
        const HIGH_MASK: u32 = MASK & !LOW_MASK; // <15:14>
        const LOW: Field = Field::from_bits(T_MSB, 0);
        const HIGH: Field = Field::from_bits(B_MSB, T_MSB + 1);
        const WHOLE: Field = Field::from_bits(B_MSB, 0);

        // We'll push the explanations roughly in order of use, since this will end up naturally
        // being in dependency order.
        let mut expls = Vec::new();

        let (l_msb, lsb_enc) = if self.is_internal_exponent() {
            (1, CAP_LIMIT_EXP.size_in_bits())
        } else {
            (0, 0)
        };

        expls.push(
            ExplainedValueBuilder::new(RichText::from(format!("`{}", l_msb)))
                .with_rich_name("`L`_msb")
                .with_reasoning(self.ie_reasoning())
                .build()
                .into(),
        );

        let (b, b_expl) = self.bottom();
        let b_high = b & HIGH_MASK; // B<15:14>
        let b_low = b & LOW_MASK; // B<13:0>
        let t_low = (self.bits_u32(CAP_LIMIT) >> lsb_enc) << lsb_enc; // T<13:0>
        assert!(t_low <= LOW_MASK);
        // T<15:14> must be calculated.

        let (l_carry, l_carry_reasoning) = if t_low < b_low {
            (1, format!(", because `T{0} < B{0}.", LOW).into())
        } else {
            (0, format!(", because `T{0} ≥ B{0}.", LOW).into())
        };

        expls.push(
            ExplainedValueBuilder::new(RichText::from(format!("`{}", l_carry)))
                .with_rich_name("`L`_carry")
                .with_reasoning(l_carry_reasoning)
                .build()
                .into(),
        );

        let t_high = b_high + ((l_msb + l_carry) << CAP_LIMIT.size_in_bits());
        let t = (t_high | t_low) & MASK; // Overflow from l_msb and l_carry wraps.

        let low_enc = Field::from_bits(LOW.msb(), lsb_enc);

        expls.push(
            ComposedValueBuilder::new(WHOLE.with_value(t))
                .with_rich_name(format!("`T{}", WHOLE))
                .with_rich_subfield(
                    format!("`B{} + L`_msb` + L`_carry", HIGH),
                    HIGH,
                    Category::BoundsBase,
                )
                .with_rich_subfield(format!("`T{}", low_enc), low_enc, Category::BoundsLimit)
                .build()
                .into(),
        );
        expls.extend(b_expl);

        (b, t, expls.into_iter())
    }

    pub fn permissions_field(&self) -> FieldWithValue<u64> {
        self.field_with_value_u64(CAP_PERMS)
    }

    pub fn flags_field(&self) -> FieldWithValue<u8> {
        let fv = self.field_with_value_u64(CAP_FLAGS);
        fv.with_value(u8::try_from(fv.value()).expect("CAP_FLAGS should have 8 bits."))
    }

    pub fn flags(&self) -> (u8, Option<Explanation>) {
        let flags = self.flags_field();
        if self.verbosity.show_trivial() {
            let expl = Explanation::new(RichText::from("Flags"), &flags, Category::ValueMeta)
                .with_rich_text(format!("0x{:02x} (0b{0:08b})", flags.value()));
            (flags.value(), Some(expl))
        } else {
            (flags.value(), None)
        }
    }
}

impl Bitwise for Morello {
    fn bit(&self, idx: u32) -> Option<bool> {
        match idx {
            0..=127 => Some(((self.raw >> idx) & 0b1) != 0),
            128 => Some(self.tag),
            _ => None,
        }
    }

    fn nibble(&self, idx: u32) -> Option<u8> {
        match idx {
            0..=31 => Some(u8::try_from((self.raw >> (idx * 4)) & 0b1111).unwrap()),
            32 => Some(self.tag.into()),
            _ => None,
        }
    }

    fn byte(&self, idx: u32) -> Option<u8> {
        match idx {
            0..=15 => Some(u8::try_from((self.raw >> (idx * 8)) & 0b11111111).unwrap()),
            16 => Some(self.tag.into()),
            _ => None,
        }
    }
}

impl Capability<'_> for Morello {
    // Generic layout and bit accessors.

    fn size_in_bits(&self) -> u32 {
        129
    }

    fn field_with_value_u64(&self, f: Field) -> FieldWithValue<u64> {
        f.with_value(self.bits_u64(f))
    }

    fn verbosity_mut(&mut self) -> &mut Verbosity {
        &mut self.verbosity
    }

    // TODO: Consider pre-computing all the properties up front, so that accessors become trivial.

    fn permissions(&self) -> Permissions {
        Permissions::from(
            self.field_with_value_u64(CAP_PERMS),
            [
                "Global",
                "Executive",
                "User<0>",
                "User<1>",
                "User<2>",
                "User<3>",
                "MutableLoad",
                "CompartmentId",
                "BranchSealedPair",
                "System",
                "Unseal",
                "Seal",
                "StoreLocal",
                "StoreCap",
                "LoadCap",
                "Execute",
                "Store",
                "Load",
            ]
            .iter()
            .copied(),
        )
    }

    fn bounds(&self) -> (Range<u128>, Vec<Explanation>) {
        const LIMIT_FIELD: Field = Field::from_bits(64, 0);
        const BASE_FIELD: Field = Field::from_bits(63, 0);

        let simple = |base, addr: u64, limit| {
            Explanation::new(
                RichText::from("Bounds"),
                field_set![CAP_BOUNDS, CAP_VALUE_FOR_BOUNDS],
                Category::Bounds,
            )
            .with_rich_text(format!(
                "`0x{:016x} {} 0x{:016x} {} 0x{:016x}",
                base,
                fmt_le(base, addr.into()),
                addr,
                fmt_lt(addr.into(), limit),
                limit
            ))
        };

        // Helper for special cases that return the maximum bounds.
        let max_bounds = |intro_elems, a: u64, mut expls: Vec<Explanation>| {
            if self.verbosity.show_fancy_bounds() {
                // Forward the `bounds_value()` explanation (if we have one) and add another, with
                // the provided intro.
                let expl_bounds =
                    Explanation::new(RichText::from("Bounds"), CAP_BOUNDS, Category::Bounds)
                        .with_elements(intro_elems)
                        .with_element(
                            ComposedValueBuilder::new(LIMIT_FIELD.with_value(CAP_BOUND_MAX))
                                .with_rich_name(format!("`Limit{}", LIMIT_FIELD))
                                .with_hex_value()
                                .build(),
                        )
                        .with_element(
                            ComposedValueBuilder::new(BASE_FIELD.with_value(CAP_BOUND_MIN))
                                .with_rich_name(format!("`Base{}", BASE_FIELD))
                                .with_hex_value()
                                .build(),
                        );
                expls.push(expl_bounds);
            } else {
                expls.push(simple(CAP_BOUND_MIN.into(), a, CAP_BOUND_MAX));
            }
            (CAP_BOUND_MIN.into()..CAP_BOUND_MAX, expls)
        };

        let (a, maybe_a_expl) = self.bounds_value();
        let mut expls = maybe_a_expl.into_iter().collect();

        let (exp, exp_elems) = self.exponent();
        let exp = match exp {
            Exponent::Normal(e) => e,
            Exponent::Invalid(..) | Exponent::Max => return max_bounds(exp_elems, a, expls),
        };

        let (b, t, bt_elems) = self.bottom_top();
        let base = u128::from(b) << exp;
        let limit = u128::from(t) << exp;

        // Correction calculations.
        let f3 = Field::from_bits(2, 0);
        let a_mid = Field::from_bits(exp + CAP_MW - 1, exp);
        let a_mid3 = Field::from_bits(a_mid.msb(), a_mid.msb() - 2);
        let mant_mid3 = Field::from_bits(CAP_MW - 1, CAP_MW - 3);
        let a3 = ((a >> a_mid3.lsb()) & 0b111) as u32;
        let b3 = ((b >> mant_mid3.lsb()) & 0b111) as u32;
        let t3 = ((t >> mant_mid3.lsb()) & 0b111) as u32;
        let r3 = b3.wrapping_sub(1) & 0b111;

        let a_hi = (a3 < r3) as i8;
        let b_hi = (b3 < r3) as i8;
        let t_hi = (t3 < r3) as i8;

        let cb = b_hi - a_hi;
        let ct = t_hi - a_hi;

        let a_top = u128::from(a) >> (exp + CAP_MW);
        let base = base + (a_top.wrapping_add(cb as u128) << (exp + CAP_MW));
        let limit = limit + (a_top.wrapping_add(ct as u128) << (exp + CAP_MW));

        // Truncate to 64 bits for the base, and 65 bits for the limit.
        let base = base & ((1u128 << BASE_FIELD.size_in_bits()) - 1);
        let limit = limit & ((1u128 << LIMIT_FIELD.size_in_bits()) - 1);

        // Limit correction for capabilities that wrap the address space.
        let l2 = (limit >> 63) & 0b11;
        let b2 = (base >> 63) & 0b01;
        let limit = limit
            ^ if (exp < (CAP_MAX_EXPONENT - 1)) && (l2.wrapping_sub(b2) > 0b01) {
                1 << 64
            } else {
                0
            };

        // The remainder of the function is concerned with building human-readable explanations.

        if !self.verbosity.show_fancy_bounds() {
            expls.push(simple(base, a, limit));
            return (base..limit, expls);
        }

        // A summary diagram naming all bounds subfields.
        let summary_elem = if self.verbosity.show_multi_value_maps() {
            let whole_cap = Field::from_bits(self.msb(), 0);
            Some(
                self.fields()
                    .iter()
                    .fold(
                        ComposedValueBuilder::new(self.field_with_value_u64(CAP_BOUNDS)),
                        |acc, sf| acc.with_subfield(sf.name(), sf.field(), sf.category()),
                    )
                    .with_bin_value()
                    .with_context(whole_cap)
                    .build(),
            )
        } else {
            None
        };

        let bt_expl = Explanation::new(
            RichText::from("Bounds Fields"),
            field_set![CAP_BOUNDS, CAP_VALUE_FOR_BOUNDS],
            Category::Bounds,
        )
        .with_elements(summary_elem.into_iter())
        .with_elements(exp_elems)
        .with_elements(bt_elems);

        let cf_expl = Explanation::new(
            "Correction Factors".into(),
            field_set![CAP_BOUNDS, CAP_VALUE_FOR_BOUNDS],
            Category::Bounds,
        )
        .with_generation(bt_expl.generation() + 1)
        // TODO: Make the subfields more accurate. For example, T3 is formed from the high bits of
        // T<15:0>, but the top two bits are coloured as BoundsBase in the T<15:0> diagram.
        .with_element(
            ComposedValueBuilder::new(f3.with_value(a3))
                .with_rich_name("`A3")
                .with_rich_subfield(format!("`A{}", a_mid3), f3, Category::Value)
                .with_bin_value()
                .build(),
        )
        .with_element(
            ComposedValueBuilder::new(f3.with_value(t3))
                .with_rich_name("`T3")
                .with_rich_subfield(format!("`T{}", mant_mid3), f3, Category::BoundsLimit)
                .with_bin_value()
                .build(),
        )
        .with_element(
            ComposedValueBuilder::new(f3.with_value(b3))
                .with_rich_name("`B3")
                .with_rich_subfield(format!("`B{}", mant_mid3), f3, Category::BoundsBase)
                .with_bin_value()
                .build(),
        )
        .with_element(
            ExplainedValueBuilder::new(RichText::from(format!("`0b{:03b}", r3)))
                .with_rich_name("`R3")
                .with_intermediate("`(B3 - 1) & 0b111".into())
                .build(),
        )
        .with_element(
            ExplainedValueBuilder::new(RichText::from(format!("`{}", ct)))
                .with_rich_name("`c`_t")
                .with_intermediate(RichText::from("`(T3 < R3) - (A3 < R3)"))
                .with_intermediate(RichText::from(format!("`{} - {}", t_hi, a_hi)))
                .build(),
        )
        .with_element(
            ExplainedValueBuilder::new(RichText::from(format!("`{}", cb)))
                .with_rich_name("`c`_b")
                .with_intermediate(RichText::from("`(B3 < R3) - (A3 < R3)"))
                .with_intermediate(RichText::from(format!("`{} - {}", b_hi, a_hi)))
                .build(),
        );

        // The "mid" subfield differs for high exponents, since the bounds have different lengths.
        let a_mid_limit = a_mid.intersection(&LIMIT_FIELD);
        let a_mid_base = a_mid.intersection(&BASE_FIELD);
        // Note that limit_high and/or base_high may be empty.
        let limit_high = Field::from_bits(LIMIT_FIELD.msb(), a_mid_limit.msb() + 1);
        let base_high = Field::from_bits(BASE_FIELD.msb(), a_mid_base.msb() + 1);
        let limit_mant = Field::from_bits(a_mid_limit.msb() - exp, 0);
        let base_mant = Field::from_bits(a_mid_base.msb() - exp, 0);
        let a_low = if a_mid.lsb() == 0 {
            Field::empty()
        } else {
            Field::from_bits(a_mid.lsb() - 1, 0)
        };
        let bounds_expl = Explanation::new(
            RichText::from("Bounds"),
            field_set![CAP_BOUNDS, CAP_VALUE_FOR_BOUNDS],
            Category::Bounds,
        )
        .with_generation(2)
        .with_element(
            ComposedValueBuilder::new(LIMIT_FIELD.with_value(limit))
                .with_rich_name(format!("`Limit{}", LIMIT_FIELD))
                .with_hex_value()
                .with_subfield_bit_numbers()
                .with_rich_subfield(
                    format!("`A{} + c_t", limit_high),
                    limit_high,
                    Category::Value,
                )
                .with_rich_subfield(
                    format!("`T{}", limit_mant),
                    a_mid_limit,
                    Category::BoundsLimit,
                )
                .with_rich_subfield(format!("zeroes{}", a_low), a_low, Category::Unknown)
                .build(),
        )
        .with_element(
            ComposedValueBuilder::new(BASE_FIELD.with_value(base))
                .with_rich_name(format!("`Base{}", BASE_FIELD))
                .with_hex_value()
                .with_subfield_bit_numbers()
                .with_rich_subfield(format!("`A{} + c_b", base_high), base_high, Category::Value)
                .with_rich_subfield(format!("`B{}", base_mant), a_mid_base, Category::BoundsBase)
                .with_rich_subfield(format!("zeroes{}", a_low), a_low, Category::Unknown)
                .build(),
        );

        expls.push(bt_expl);
        expls.push(cf_expl);
        expls.push(bounds_expl);

        (base..limit, expls)
    }

    fn fields(&self) -> Vec<DescribedField> {
        let mut fields = Vec::new();
        macro_rules! push {
            ($field:expr, $name:expr, $category:expr) => {
                fields.push(DescribedField::new_with_rich_name($field, $name, $category))
            };
        }

        // Fields common to all Morello capabilities.
        push!(CAP_TAG, "Tag", Category::Tag);
        push!(CAP_PERMS, "Permissions", Category::Permissions);
        push!(CAP_OTYPE, "ObjectType", Category::ObjectType);
        push!(CAP_FLAGS, "Flags", Category::ValueMeta);
        push!(
            CAP_VALUE_FOR_BOUNDS,
            format!("Value{}", CAP_VALUE_FOR_BOUNDS),
            Category::BoundsValue
        );

        // Bounds field expansion.
        push!(CAP_IE, "!IE", Category::BoundsFormat);
        let b_msb = CAP_BASE.size_in_bits() - 1;
        let t_msb = CAP_LIMIT.size_in_bits() - 1;
        if self.is_internal_exponent() {
            let e_size_b = CAP_BASE_EXP.size_in_bits();
            let e_size_t = CAP_LIMIT_EXP.size_in_bits();
            let b = format!("B{}", Field::from_bits(b_msb, e_size_b));
            let t = format!("T{}", Field::from_bits(t_msb, e_size_t));
            push!(CAP_BASE_IE, b, Category::BoundsBase);
            push!(CAP_LIMIT_IE, t, Category::BoundsLimit);
            let e_b = format!("!E{}", Field::from_bits(e_size_b - 1, 0));
            let e_t = format!("!E{}", Field::from_bits(e_size_b + e_size_t - 1, e_size_b));
            push!(CAP_BASE_EXP, e_b, Category::BoundsExponent);
            push!(CAP_LIMIT_EXP, e_t, Category::BoundsExponent);
        } else {
            let b = format!("B{}", Field::from_bits(b_msb, 0));
            let t = format!("T{}", Field::from_bits(t_msb, 0));
            push!(CAP_BASE, b, Category::BoundsBase);
            push!(CAP_LIMIT, t, Category::BoundsLimit);
        }
        fields
    }

    fn explanations(&self) -> Vec<Explanation> {
        let mut expls = Vec::new();

        let perms = Explanation::new(
            RichText::from("Permissions"),
            CAP_PERMS,
            Category::Permissions,
        )
        .with_element(self.permissions());

        expls.extend(self.tag().1);
        expls.extend(self.object_type().1);
        expls.push(perms);
        expls.extend(self.value().1);
        expls.extend(self.flags().1);
        expls.extend(self.bounds().1);

        // Sort by generation only, so we can somewhat control the default display order whilst
        // still observing dependencies between explanations.
        expls.sort_by_key(|expl| expl.generation());

        expls
    }

    fn value(&self) -> (u64, Option<Explanation>) {
        let fv = self.field_with_value_u64(CAP_VALUE);
        if self.verbosity.show_trivial() && self.verbosity.show_fancy_bounds() {
            let expl = Explanation::new(RichText::from("Value"), &fv, Category::Value)
                .with_element(
                    ExplainedValueBuilder::new(RichText::from(format!("`0x{:016x}", fv.value())))
                        .with_reasoning(", as returned by `GCVALUE`.".into())
                        .build(),
                );
            (fv.value(), Some(expl))
        } else {
            (fv.value(), None)
        }
    }

    fn bounds_value(&self) -> (u64, Option<Explanation>) {
        // Sign-extend value<55:0> to 64 bits.
        const V: Field = CAP_VALUE_FOR_BOUNDS;
        let value = self.bits_u64(V);
        let sign_ext = (u64::MAX * (value >> V.msb())) & !V.shifted_mask_u64();
        let bounds_value = sign_ext | value;

        if self.verbosity.show_fancy_bounds() {
            const A: Field = Field::from_bits(65, 0);
            let mut expl = Explanation::new(
                RichText::from("Bounds Value"),
                &CAP_VALUE_FOR_BOUNDS,
                Category::BoundsValue,
            )
            .with_element(
                ComposedValueBuilder::new(A.with_value(u128::from(bounds_value)))
                    .with_rich_name(format!("`A{}", A))
                    .with_hex_value()
                    .with_subfield_bit_numbers()
                    .with_rich_subfield("sign extension", CAP_FLAGS, Category::ValueMeta)
                    .with_rich_subfield(format!("`Value{}", V), V, Category::BoundsValue)
                    .build(),
            );

            if self.verbosity.show_descriptions() {
                // TODO: Wrap text automatically. Fonts vary quite considerably.
                expl.add_rich_text(
                    "The \"bounds value\", or effective address, is formed by sign-\n\
                     extending `Value<55:0>` to 64 bits, then zero-extending to 66 bits.",
                );
            }
            (bounds_value, Some(expl))
        } else {
            (bounds_value, None)
        }
    }

    fn tag(&self) -> (bool, Option<Explanation>) {
        if self.verbosity.show_trivial() {
            let mut expl = Explanation::new(RichText::from("Tag"), &CAP_TAG, Category::Tag);
            if self.tag {
                expl.add_rich_text("1");
            } else {
                expl.add_rich_text("0 (capability is invalid)");
            }
            (self.tag, Some(expl))
        } else {
            (self.tag, None)
        }
    }

    fn object_type(&self) -> (Option<FieldWithValue<u64>>, Option<Explanation>) {
        let fv = self.field_with_value_u64(CAP_OTYPE);
        let expl = Explanation::new(
            RichText::from("ObjectType"),
            CAP_OTYPE,
            Category::ObjectType,
        );
        if fv.value() == 0 {
            if self.verbosity.show_descriptions() && self.verbosity.show_trivial() {
                (None, Some(expl.with_rich_text("Not sealed.")))
            } else {
                (None, None)
            }
        } else {
            (
                Some(fv),
                Some(expl.with_rich_text(format!("Sealed with type 0x{:x}.", fv.value()))),
            )
        }
    }
}

impl Morello {
    pub fn legal_formats() -> RichText<&'static str> {
        // Note that that tabs are useful for variable-width fonts (e.g. in the GUI).
        RichText::from(
            "There are three supported variants:

1.\tA simple 33-digit, 129-bit hexadecimal value.
2.\tThree fields separated by colons or bars, like `{tag}|{high64}|{low64}`. In
  \tthis case, each field is zero-extended.
3.\tFive fields separated by colons or bars, as above but with 32-bit chunks,
  \tlike `{tag}|{word3}|{word2}|{word1}|{word0}`.

In all cases:

-\tA single leading \"`0x`\" or \"`0X`\" is accepted, but not required.
-\tUnderscore (`\\_`), comma (`,`) and apostrophe (`'`) characters in the input string
 \tare ignored, and may be used to make values more readable.",
        )
    }
}

impl TryFrom<&str> for Morello {
    type Error = ParseError;

    /// Try to parse a capability from a string representation.
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let is_readability_sep = |c| c == b'_' || c == b',' || c == b'\'';
        let is_part_sep = |c| c == b':' || c == b'|';

        let prefix = ["0x", "0X"];
        let prefix_len = prefix.iter().fold(0, |acc, p| {
            // Check that we can treat the result as a grapheme cluster count.
            assert!(p.is_ascii());
            acc.max((s.starts_with(p) as usize) * p.len())
        });

        let mut illegal = FlaggedChars::new();
        let chars: Vec<(usize, u8)> = s
            .graphemes(true)
            // Enumerate graphemes first. We do this, even though all valid inputs are simple ASCII
            // characters, so that error messages point to the correct parts of the input string.
            .enumerate()
            // Drop the prefix (if present).
            .skip(prefix_len)
            // Flag anything that isn't plain ASCII.
            .map(|(i, g)| {
                if g.is_ascii() {
                    // ASCII bytes are grapheme clusters by themselves.
                    assert!(g.len() == 1);
                }
                (i, g.bytes().next().unwrap(), !g.is_ascii())
            })
            // Drop readability separators.
            .filter(|&(_, c, _)| !is_readability_sep(c))
            // All remaining characters should be part separators or digits.
            .map(|(i, c, f)| {
                // Record flagged characters.
                if f || !(is_part_sep(c) || c.is_ascii_hexdigit()) {
                    illegal.flag(i)
                }
                (i, c)
            })
            .collect();
        if illegal.any() {
            return Err(ParseError::InvalidCharacters { at: illegal });
        }
        if chars.is_empty() {
            return Err(ParseError::Empty);
        }

        // A helper for checking that each field had the proper number of digits, and if necessary,
        // building a diagnostic error.
        let check_field_digits = |fields: &[(usize, &[(usize, u8)])]| -> Result<(), Self::Error> {
            let mut flagged = Vec::new();
            for &(max, chars) in fields {
                if chars.len() > max {
                    let found = chars.iter().map(|&(i, _)| i);
                    flagged.push((FlaggedChars::from_indices(found), max));
                }
            }
            if flagged.is_empty() {
                Ok(())
            } else {
                Err(ParseError::TooManyDigitsInFields(flagged))
            }
        };

        // Split into the tag and 32-bit slices.
        // TODO: `split_inclusive`, once stable, would make it possible to identify the positions
        // of empty parts, so we can highlight empty fields intuitively.
        let parts: Vec<&[(usize, u8)]> = chars.split(|&(_, s)| is_part_sep(s)).collect();
        let (tag, word3, word2, word1, word0);
        match parts.as_slice() {
            [simple] => {
                let indices = simple.iter().map(|&(i, _)| i);
                match simple.len() {
                    0..=32 => {
                        let found = FlaggedChars::from_indices(indices);
                        return Err(ParseError::TooFewDigits { found, min: 33 });
                    }
                    33 => {
                        tag = &simple[0..1];
                        word3 = &simple[1..9];
                        word2 = &simple[9..17];
                        word1 = &simple[17..25];
                        word0 = &simple[25..33];
                    }
                    _ => {
                        let found = FlaggedChars::from_indices(indices);
                        return Err(ParseError::TooManyDigits { found, max: 33 });
                    }
                }
            }
            [t, high64, low64] => {
                check_field_digits(&[(1, t), (16, high64), (16, low64)])?;
                tag = t;
                let mut l_chunks = low64.rchunks(8);
                let mut h_chunks = high64.rchunks(8);
                word0 = l_chunks.next().unwrap_or(&[]);
                word1 = l_chunks.next().unwrap_or(&[]);
                word2 = h_chunks.next().unwrap_or(&[]);
                word3 = h_chunks.next().unwrap_or(&[]);
            }
            [t, w3, w2, w1, w0] => {
                check_field_digits(&[(1, t), (8, w3), (8, w2), (8, w1), (8, w0)])?;
                tag = t;
                word3 = w3;
                word2 = w2;
                word1 = w1;
                word0 = w0;
            }
            fields => {
                // Convert each field into flags.
                let fields: Vec<_> = fields
                    .iter()
                    .map(|part| FlaggedChars::from_indices(part.iter().map(|(i, _)| *i)))
                    .collect();
                return Err(ParseError::WrongNumberOfFields {
                    fields,
                    allowed: vec![1, 3, 5],
                });
            }
        };

        // At this point, the tag and each word are known to have a valid number of digits, and are
        // known to be composed of hex characters.

        let tag = match *tag {
            [] | [(_, b'0')] => false,
            [(_, b'1')] => true,
            _ => {
                // We already checked the field length ...
                assert!(tag.len() == 1);
                // ... so the only possible error is that the digit is >= 2.
                let indices = tag.iter().map(|&(i, _)| i);
                let at = FlaggedChars::from_indices(indices);
                return Err(ParseError::InvalidCharacters { at });
            }
        };

        let parse_32 = |field: &[(usize, u8)]| {
            assert!(field.len() <= 8);
            field
                .iter()
                // Parse hex, digit by digit.
                // We already checked that the digits are valid hex.
                .map(|&(_, c)| char::from(c).to_digit(16).unwrap())
                .fold(0, |acc, d| (acc << 4) | d)
        };

        let word3 = parse_32(word3);
        let word2 = parse_32(word2);
        let word1 = parse_32(word1);
        let word0 = parse_32(word0);
        Ok(Self::from_u32_parts(tag, word3, word2, word1, word0))
    }
}

impl Display for Morello {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if f.alternate() {
            let (tag, high64, low64) = self.raw_u64_parts();
            write!(f, "0x{}|{:x}|{:x}", tag as u32, high64, low64)
        } else {
            let (tag, word3, word2, word1, word0) = self.raw_u32_parts();
            write!(
                f,
                "0x{}'{:08x}'{:08x}'{:08x}'{:08x}",
                tag as u32, word3, word2, word1, word0
            )
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Exponent {
    Normal(u32),
    Invalid(u32),
    Max,
}

impl Exponent {
    pub fn get(&self) -> u32 {
        match self {
            Self::Normal(e) | Self::Invalid(e) => *e,
            Self::Max => CAP_MAX_EXPONENT,
        }
    }
}

fn fmt_le<T: PartialOrd>(left: T, right: T) -> char {
    if left <= right {
        '≤'
    } else {
        '≰'
    }
}

fn fmt_lt<T: PartialOrd>(left: T, right: T) -> char {
    if left < right {
        '<'
    } else {
        '≮'
    }
}
