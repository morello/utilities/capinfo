// Copyright (c) 2021-2022 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

pub mod described_field;
pub mod error;
pub mod explanation;
pub mod morello;

use std::{
    convert::TryFrom,
    fmt,
    ops::{Range, RangeInclusive},
};

use crate::{
    bitwise::Bitwise,
    field::{Field, FieldWithValue},
    rich_text::RichText,
};
use described_field::DescribedField;
pub use explanation::Explanation;
use explanation::{permissions::Permissions, Verbosity};
use morello::Morello;

/// A [CHERI]-style capability, with 64-bit addressing.
///
/// - Capabilities are represented as a contiguous set of bits (e.g. 129 bits for [Morello]), where
///   bit 0 is the least significant. Implementers of `Capability` must also implement [`Bitwise`],
///   and `self.`[`bit(x)`] must be `Some(...)` for all `0 <= x < self.`[`size_in_bits()`].
/// - All capabilities have:
///     - ... a 64-bit [`value()`], which is typically an address but may include additional fields
///       (such as Morello's flags).
///     - ... a 64-bit [`bounds_value()`], which is the value actually used for bounds checks.
///     - ... upper and lower [`bounds()`], describing a contiguous range of bounds values that can
///       be accessed using this capability.
///     - ... a [`tag()`] bit representing provenance validity.
///     - ... a list of [`permissions()`] describing what operations this capability permits.
///     - ... an optional [`object_type()`] field, with implementation-defined semantics. The
///       object type is typically present only for sealed capabilities, though this trait does not
///       require that.
/// - Implementations of `Capability` should also provide detailed descriptions of how these values
///   are derived.
///     - Several accessors return one or more [`Explanation`]s, explaining how their results were
///       calculated. Such explanations may duplicate those returned by other accessors (e.g. if
///       decoding one field requires that another is decoded first), and implementations may
///       provide arbitrary additional [`Explanation`]s (e.g. for accessors not required by this
///       trait).
///     - High-level [`explanations()`] and [`fields()`] methods permit easy handling in generic UI
///       code.
///
/// Refer to the [CHERI] and [Morello] documentation for further details.
///
/// [CHERI]: https://www.cl.cam.ac.uk/research/security/ctsrd/cheri/
/// [Morello]: https://www.morello-project.org/
/// [`Bitwise`]: ../bitwise/trait.Bitwise.html
/// [`Explanation`]: ./explanation/struct.Explanation.html
/// [`fields()`]: #tymethod.fields
/// [`explanations()`]: #tymethod.explanations
/// [`permissions()`]: #tymethod.permissions
/// [`size_in_bits()`]: #tymethod.size_in_bits
/// [`value()`]: #tymethod.value
/// [`bounds_value()`]: #tymethod.bounds_value
/// [`bounds()`]: #tymethod.bounds
/// [`tag()`]: #tymethod.tag
/// [`object_type()`]: #tymethod.object_type
/// [`bit(x)`]: ../bitwise/trait.Bitwise.html#tymethod.bit
pub trait Capability<'a>: fmt::Display + Bitwise {
    /// The index of the most-significant bit of the capability.
    ///
    /// The index of the _least_-significant bit should be taken as zero.
    fn msb(&self) -> u32 {
        self.size_in_bits() - 1
    }

    fn size_in_bits(&self) -> u32;

    /// Extract the bits specified by the field.
    ///
    /// Panics if `f.size_in_bits() > 64`.
    fn field_with_value_u64(&self, f: Field) -> FieldWithValue<u64>;

    /// Override the verbosity level for explanations.
    fn set_verbosity(&mut self, verbosity: Verbosity) {
        *self.verbosity_mut() = verbosity
    }

    /// Modify the verbosity level for explanations.
    ///
    /// The verbosity affects both the existence and the contents of explanations returned by all
    /// accessors (including the primary `explanations()` interface).
    fn verbosity_mut(&mut self) -> &mut Verbosity;

    fn permissions(&self) -> Permissions;
    fn bounds(&self) -> (Range<u128>, Vec<Explanation>);
    fn value(&self) -> (u64, Option<Explanation>);
    fn bounds_value(&self) -> (u64, Option<Explanation>);
    fn tag(&self) -> (bool, Option<Explanation>);
    fn object_type(&self) -> (Option<FieldWithValue<u64>>, Option<Explanation>);

    /// A list of top-level [`Field`]s (with human-readable names).
    ///
    /// Fields are guaranteed not to overlap, but are not returned in any particular order, and
    /// might not cover all bits in the capability (e.g. if there are unused bits).
    ///
    /// [`Field`]: ../field/type.Field.html
    fn fields(&self) -> Vec<DescribedField>;

    /// A list of explanations describing fields and properties of the capability.
    ///
    /// Explanations are returned sorted in expected (linear) display order, not necessarily by the
    /// natural ordering of `Explanation`.
    fn explanations(&self) -> Vec<Explanation>;

    fn is_sealed(&self) -> bool {
        self.object_type().0.is_some()
    }
    fn base(&self) -> u128 {
        self.bounds().0.start
    }
    fn limit(&self) -> u128 {
        self.bounds().0.end
    }

    /// A range of addresses that can actually be accessed (with 64-bit addressing). This is like
    /// [`bounds()`], but it truncates the range to the 64-bit address space, and returns an
    /// inclusive range so that full-address-space capabilities can be represented with simple
    /// 64-bit values.
    ///
    /// If the bounds are empty, this returns `None`.
    ///
    /// [`bounds()`]: #method.bounds
    fn effective_bounds(&self) -> Option<RangeInclusive<u64>> {
        let bounds = self.bounds();
        let min = u64::try_from(bounds.0.start).unwrap_or(u64::MAX);
        let max = u64::try_from(bounds.0.end).map_or(u64::MAX, |e| e.saturating_sub(1));
        if min < max {
            Some(min..=max)
        } else {
            None
        }
    }
}

/// An enumeration of available implementations of `Capability`, to emulate down-casts into a known
/// format.
#[derive(Debug, Copy, Clone)]
pub enum CapabilityWithFormat {
    Morello(Morello),
}

impl CapabilityWithFormat {
    /// Extract the contained capability as a trait object.
    pub fn cap(&self) -> &dyn Capability {
        match self {
            Self::Morello(cap) => cap,
        }
    }

    /// Extract the contained capability as a trait object.
    pub fn cap_mut(&mut self) -> &mut dyn Capability {
        match self {
            Self::Morello(cap) => cap,
        }
    }

    pub fn as_field_with_value(self) -> FieldWithValue<Self> {
        FieldWithValue::from_bits_with_value(self.msb(), 0, self)
    }

    pub fn as_field(&self) -> Field {
        Field::from_bits(self.msb(), 0)
    }

    pub fn legal_formats() -> RichText<&'static str> {
        Morello::legal_formats()
    }
}

impl TryFrom<&str> for CapabilityWithFormat {
    type Error = error::ParseError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Ok(Self::Morello(Morello::try_from(s)?))
    }
}

impl Capability<'_> for CapabilityWithFormat {
    fn size_in_bits(&self) -> u32 {
        self.cap().size_in_bits()
    }

    fn field_with_value_u64(&self, f: Field) -> FieldWithValue<u64> {
        self.cap().field_with_value_u64(f)
    }

    fn verbosity_mut(&mut self) -> &mut Verbosity {
        self.cap_mut().verbosity_mut()
    }

    fn permissions(&self) -> Permissions {
        self.cap().permissions()
    }

    fn bounds(&self) -> (Range<u128>, Vec<Explanation>) {
        self.cap().bounds()
    }

    fn value(&self) -> (u64, Option<Explanation>) {
        self.cap().value()
    }

    fn bounds_value(&self) -> (u64, Option<Explanation>) {
        self.cap().bounds_value()
    }

    fn tag(&self) -> (bool, Option<Explanation>) {
        self.cap().tag()
    }

    fn object_type(&self) -> (Option<FieldWithValue<u64>>, Option<Explanation>) {
        self.cap().object_type()
    }

    fn fields(&self) -> Vec<DescribedField> {
        self.cap().fields()
    }

    fn explanations(&self) -> Vec<Explanation> {
        self.cap().explanations()
    }
}

impl fmt::Display for CapabilityWithFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        self.cap().fmt(f)
    }
}

impl Bitwise for CapabilityWithFormat {
    fn bit(&self, idx: u32) -> Option<bool> {
        self.cap().bit(idx)
    }
    fn nibble(&self, idx: u32) -> Option<u8> {
        self.cap().nibble(idx)
    }
    fn byte(&self, idx: u32) -> Option<u8> {
        self.cap().byte(idx)
    }
}

/// `Eq` means bit-equality here.
impl Eq for CapabilityWithFormat {}

impl PartialEq for CapabilityWithFormat {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Morello(lhs), Self::Morello(rhs)) => lhs == rhs,
        }
    }
}
