// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{field::Field, rich_text::RichText};

/// Categorise fields to aid displays (e.g. colour-coding).
///
/// Categories are independent of field names so that UI code can remain robust against changes to
/// field names.
#[non_exhaustive]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Category {
    Unknown,
    Value,
    BoundsValue,
    ValueMeta,
    Bounds,
    BoundsBase,
    BoundsLimit,
    BoundsExponent,
    BoundsFormat,
    ObjectType,
    Permissions,
    Tag,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct DescribedField {
    // Sort (`PartialOrd`) by `field` first.
    field: Field,
    name: RichText<String>,
    category: Category,
}

impl From<Field> for DescribedField {
    /// Create a new `DescribedField` with no description.
    fn from(field: Field) -> Self {
        Self {
            field,
            category: Category::Unknown,
            name: RichText::new(),
        }
    }
}

impl DescribedField {
    pub fn new<S: Into<String> + AsRef<str>>(
        field: Field,
        name: RichText<S>,
        category: Category,
    ) -> Self {
        Self::from(field).with_name(name).with_category(category)
    }

    /// Create a `DescribedField`, interpreting `name` using `RichText` syntax.
    pub fn new_with_rich_name<S: Into<String>>(field: Field, name: S, category: Category) -> Self {
        Self::new(field, RichText::from(name.into()), category)
    }

    /// Name the `DescribedField`.
    pub fn with_name<S: Into<String> + AsRef<str>>(self, name: RichText<S>) -> Self {
        let name = name.into_owned();
        Self { name, ..self }
    }

    /// Name the `DescribedField`, interpreting `name` using `RichText` syntax.
    pub fn with_rich_name<S: Into<String>>(self, name: S) -> Self {
        self.with_name(RichText::from(name.into()))
    }

    pub fn without_name(self) -> Self {
        self.with_rich_name("")
    }

    pub fn with_category(self, category: Category) -> Self {
        Self { category, ..self }
    }

    pub fn field(&self) -> Field {
        self.field
    }

    pub fn has_name(&self) -> bool {
        !self.name.is_empty()
    }

    pub fn name(&self) -> RichText<&str> {
        self.name.as_str()
    }

    pub fn category(&self) -> Category {
        self.category
    }
}
