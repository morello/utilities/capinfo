// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Tools for treating values as sets of bits.

use std::convert::TryFrom;

/// Generic bit-level accessors.
pub trait Bitwise {
    /// Extract the specified bit, or `None` if it lies outside the value.
    fn bit(&self, idx: u32) -> Option<bool>;

    /// Extract the specified nibble, or `None` if it lies wholly outside the value. Where
    /// individual bits lie outside the value, they are set to zero.
    fn nibble(&self, idx: u32) -> Option<u8>;

    /// Extract the specified byte, or `None` if it lies wholly outside the value. Where individual
    /// bits lie outside the value, they are set to zero.
    fn byte(&self, idx: u32) -> Option<u8>;
}

impl Bitwise for bool {
    fn bit(&self, idx: u32) -> Option<bool> {
        match idx {
            0 => Some(*self),
            _ => None,
        }
    }

    fn nibble(&self, idx: u32) -> Option<u8> {
        match idx {
            0 => Some(u8::try_from(*self).unwrap()),
            _ => None,
        }
    }

    fn byte(&self, idx: u32) -> Option<u8> {
        match idx {
            0 => Some(u8::try_from(*self).unwrap()),
            _ => None,
        }
    }
}

macro_rules! impl_bitwise {
    ($t:ty, $bits:expr) => {
        impl Bitwise for $t {
            fn bit(&self, idx: u32) -> Option<bool> {
                const MSB: u32 = $bits - 1;
                match idx {
                    0..=MSB => Some(((self >> idx) & 0b1) != 0),
                    _ => None,
                }
            }

            fn nibble(&self, idx: u32) -> Option<u8> {
                const MSNIBBLE: u32 = (($bits + 3) / 4) - 1;
                match idx {
                    0..=MSNIBBLE => Some(((self >> (idx * 4)) & 0b1111) as u8),
                    _ => None,
                }
            }

            fn byte(&self, idx: u32) -> Option<u8> {
                const MSBYTE: u32 = (($bits + 7) / 8) - 1;
                match idx {
                    0..=MSBYTE => Some(((self >> (idx * 8)) & 0b11111111) as u8),
                    _ => None,
                }
            }
        }
    };
}
impl_bitwise!(u8, 8);
impl_bitwise!(u16, 16);
impl_bitwise!(u32, 32);
impl_bitwise!(u64, 64);
impl_bitwise!(u128, 128);
