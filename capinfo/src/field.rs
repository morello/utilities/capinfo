// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Types representing and handling bit fields.

use itertools::Itertools;
use std::fmt;

use crate::bitwise::Bitwise;

#[macro_export]
/// Conveniently build a `FieldSet`, with `vec!`-like syntax.
macro_rules! field_set {
    ($($e:expr), *) => {
        {
            let mut set = $crate::field::FieldSet::empty();
            $(set.include($e);)*
            set
        }
    };
}

/// A contiguous range of bits, with an associated value.
///
/// If no value is available, it may be `()`. See [`Field`].
///
/// [`Field`]: ./type.Field.html
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FieldWithValue<T> {
    msb: u32,
    lsb: u32,
    value: T,
}

/// A contiguous range of bits.
///
/// Like [`FieldWithValue`], but with no associated value.
///
/// `Field` is analagous to `std::ops::RangeInclusive`, but offers more specialised functions.
///
/// [`FieldWithValue`]: ./struct.FieldWithValue.html
pub type Field = FieldWithValue<()>;

impl<T> FieldWithValue<T> {
    /// Drop the value, returning a plain `Field`.
    pub const fn without_value(&self) -> Field {
        Field::from_bits(self.msb, self.lsb)
    }

    /// Replace the value, returning a new `FieldWithValue`.
    ///
    /// `value.bit(x)` must be `Some(...)` for all `0 <= x < self.size_in_bits()`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::{Field, FieldWithValue};
    /// let field = Field::from_bits(5, 2);
    /// let fv = field.with_value(0b1111);
    /// assert_eq!(fv, FieldWithValue::<u64>::from_bits_with_value(5, 2, 0b1111));
    /// ```
    pub fn with_value<V: Bitwise + Copy>(&self, value: V) -> FieldWithValue<V> {
        FieldWithValue::from_bits_with_value(self.msb, self.lsb, value)
    }

    /// Return a reference to the contained value.
    ///
    /// If `T` implements `Copy`, [`value()`] may be more convenient.
    ///
    /// [`value()`]: #method.value
    pub const fn value_ref(&self) -> &T {
        &self.value
    }

    pub const fn is_empty(&self) -> bool {
        self.msb < self.lsb
    }

    /// The length (in bits) of the field.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bits(5, 0).size_in_bits(), 6);
    /// assert_eq!(Field::from_bits(5, 2).size_in_bits(), 4);
    /// assert_eq!(Field::from_bit(5).size_in_bits(), 1);
    /// ```
    pub const fn size_in_bits(&self) -> u32 {
        if self.is_empty() {
            0
        } else {
            self.msb - self.lsb + 1
        }
    }

    /// The length (in nibbles) of the field, rounding up to the next whole nibble.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bits(5, 0).size_in_nibbles(), 2);
    /// assert_eq!(Field::from_bits(5, 2).size_in_nibbles(), 1);
    /// assert_eq!(Field::from_bit(5).size_in_nibbles(), 1);
    /// ```
    pub const fn size_in_nibbles(&self) -> u32 {
        (self.size_in_bits() + 3) / 4
    }

    /// True if the field is a single bit.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bit(42).is_bit(), true);
    /// assert_eq!(Field::from_bits(42, 42).is_bit(), true);
    /// assert_eq!(Field::from_bits(44, 40).is_bit(), false);
    /// ```
    pub const fn is_bit(&self) -> bool {
        self.msb == self.lsb
    }

    /// A bit mask with the `len()` least-significant bits set, and all others clear.
    ///
    /// Panics if the field has more than 64 bits.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bits(5, 0).shifted_mask_u64(), 0b00111111);
    /// assert_eq!(Field::from_bits(95, 92).shifted_mask_u64(), 0b00001111);
    /// assert_eq!(Field::from_bit(95).shifted_mask_u64(), 0b00000001);
    ///
    /// // Field extraction example.
    /// let field = Field::from_bits(55, 48);
    /// let data = 0x123456789abcdef0;
    /// let extracted = (data >> field.lsb()) & field.shifted_mask_u64();
    /// assert_eq!(extracted, 0x34);
    /// ```
    pub fn shifted_mask_u64(&self) -> u64 {
        let bits = self.size_in_bits();
        assert!(bits <= 64); // We expect to return the result in a u64.
        if bits == 64 {
            !0
        } else {
            (1u64 << bits) - 1
        }
    }

    /// A bit mask with the `len()` least-significant bits set, and all others clear.
    ///
    /// Panics if the field has more than 32 bits.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bits(5, 0).shifted_mask_u32(), 0b00111111);
    /// assert_eq!(Field::from_bits(95, 92).shifted_mask_u32(), 0b00001111);
    /// assert_eq!(Field::from_bit(95).shifted_mask_u32(), 0b00000001);
    ///
    /// // Field extraction example.
    /// let field = Field::from_bits(55, 48);
    /// let data = 0x123456789abcdef0;
    /// let extracted = (data >> field.lsb()) & u64::from(field.shifted_mask_u32());
    /// assert_eq!(extracted, 0x34);
    /// ```
    pub fn shifted_mask_u32(&self) -> u32 {
        let bits = self.size_in_bits();
        assert!(bits <= 32); // We expect to return the result in a u32.
        if bits == 32 {
            !0
        } else {
            (1u32 << bits) - 1
        }
    }

    /// For single-bit `Field`s, return the index of the bit.
    ///
    /// This panics if the `Field` is not a single bit.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bit(5).bit(), 5);
    /// assert_eq!(Field::from_bit(42).bit(), 42);
    /// ```
    pub fn bit(&self) -> u32 {
        assert!(self.is_bit());
        self.lsb()
    }

    /// The index of the most-significant bit of the `Field`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bits(44, 40).msb(), 44);
    /// assert_eq!(Field::from_bit(42).msb(), 42);
    /// ```
    pub const fn msb(&self) -> u32 {
        self.msb
    }

    /// The index of the least-significant bit of the `Field`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// assert_eq!(Field::from_bits(44, 40).lsb(), 40);
    /// assert_eq!(Field::from_bit(42).lsb(), 42);
    /// ```
    pub const fn lsb(&self) -> u32 {
        self.lsb
    }

    /// Return `true` if `other` is a subset of this `Field`, and `false` otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// let a = Field::from_bits(44, 40);
    /// let b = Field::from_bit(42);
    /// assert!(a.contains(b));
    /// assert!(!b.contains(a));
    ///
    /// // Fields contain themselves.
    /// assert!(a.contains(a));
    /// assert!(b.contains(b));
    ///
    /// // Empty fields contain nothing, and nothing contains empty fields.
    /// let e = Field::from_bits(40, 44);
    /// assert!(!Field::empty().contains(a));
    /// assert!(!e.contains(a));
    /// assert!(!a.contains(Field::empty()));
    /// assert!(!a.contains(e));
    /// ```
    pub fn contains(&self, other: Field) -> bool {
        !other.is_empty() && (self.msb() >= other.msb()) && (self.lsb() <= other.lsb())
    }

    /// Return `true` if `bit` is part of this `Field`, and `false` otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// let f = Field::from_bits(44, 40);
    /// assert!(!f.contains_bit(45));
    /// assert!(f.contains_bit(44));
    /// assert!(f.contains_bit(43));
    /// assert!(f.contains_bit(42));
    /// assert!(f.contains_bit(41));
    /// assert!(f.contains_bit(40));
    /// assert!(!f.contains_bit(39));
    ///
    /// // Empty fields contain nothing.
    /// let e = Field::from_bits(40, 44);
    /// assert!(!e.contains_bit(39));
    /// assert!(!e.contains_bit(42));
    /// assert!(!e.contains_bit(45));
    /// ```
    pub fn contains_bit(&self, bit: u32) -> bool {
        (self.msb() >= bit) && (self.lsb() <= bit)
    }

    /// Return `true` if this `Field` overlaps `other` (wholly or partially), and `false`
    /// otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// let a = Field::from_bits(44, 40);
    /// let b = Field::from_bits(42, 38);
    /// let c = Field::from_bit(39);
    /// assert!(a.overlaps(a));
    /// assert!(a.overlaps(b));
    /// assert!(!a.overlaps(c));
    /// assert!(b.overlaps(a));
    /// assert!(b.overlaps(c));
    /// assert!(b.overlaps(c));
    /// assert!(!c.overlaps(a));
    /// assert!(c.overlaps(b));
    /// assert!(c.overlaps(c));
    ///
    /// // Empty fields overlap with nothing.
    /// let e = Field::from_bits(40, 44);
    /// assert!(!a.overlaps(e));
    /// assert!(!b.overlaps(e));
    /// assert!(!c.overlaps(e));
    /// assert!(!e.overlaps(a));
    /// assert!(!e.overlaps(b));
    /// assert!(!e.overlaps(c));
    /// assert!(!e.overlaps(e));
    /// ```
    pub fn overlaps(&self, other: Field) -> bool {
        self.msb().min(other.msb()) >= self.lsb().max(other.lsb())
    }
}

impl Field {
    /// Construct an empty `Field`.
    pub const fn empty() -> Self {
        Self {
            msb: 0,
            lsb: u32::MAX,
            value: (),
        }
    }

    /// Construct a `Field` spanning the range from `msb` to `lsb` (inclusive).
    ///
    /// This is a `const fn` so that it can be used to initialise `const` values. Stable Rust
    /// cannot include assertions in a `const fn`, so `msb < lsb` is accepted, and results in a
    /// `Field` with zero length.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// let field = Field::from_bits(5, 2);
    /// ```
    pub const fn from_bits(msb: u32, lsb: u32) -> Self {
        Self {
            msb,
            lsb,
            value: (),
        }
    }

    /// Construct a `Field` spanning a single bit.
    ///
    /// This is a `const fn` so that it can be used to initialise `const` values.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::Field;
    /// let field = Field::from_bit(5);
    /// ```
    pub const fn from_bit(bit: u32) -> Self {
        Self::from_bits(bit, bit)
    }

    /// The intersection of `self` and `other`.
    ///
    /// This may be empty.
    pub fn intersection<T>(mut self, other: &FieldWithValue<T>) -> Self {
        self.intersect(other);
        self
    }

    /// Reduce `self` to the intersection of `self` and `other`.
    ///
    /// This may leave `self` empty.
    pub fn intersect<T>(&mut self, other: &FieldWithValue<T>) {
        self.lsb = u32::max(self.lsb, other.lsb);
        self.msb = u32::min(self.msb, other.msb);
    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if self.is_empty() {
            if self == &Self::empty() {
                write!(f, "<empty>")
            } else {
                write!(f, "<empty({}:{})>", self.msb(), self.lsb())
            }
        } else if self.is_bit() {
            write!(f, "<{}>", self.bit())
        } else {
            write!(f, "<{}:{}>", self.msb(), self.lsb())
        }
    }
}

impl FieldWithValue<bool> {
    pub const fn from_bit_with_value(bit: u32, value: bool) -> Self {
        Self {
            msb: bit,
            lsb: bit,
            value,
        }
    }
}

impl<T: Bitwise> FieldWithValue<T> {
    /// Create a new `FieldWithValue` from `msb` to `lsb` (inclusive) with the provided `value`.
    ///
    /// `value.bit(x)` must be `Some(...)` for all `0 <= x < Field::new(msb, lsb).size_in_bits()`.
    pub fn from_bits_with_value(msb: u32, lsb: u32, value: T) -> Self {
        for bit in 0..=(msb - lsb) {
            assert!(value.bit(bit).is_some());
        }
        Self { msb, lsb, value }
    }
}

impl<T: Copy> FieldWithValue<T> {
    /// Return a copy of the contained value.
    pub fn value(&self) -> T {
        self.value
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
/// Zero or more non-overlapping `Field`s.
pub struct FieldSet {
    fields: Vec<Field>,
}

impl FieldSet {
    /// Create a new empty `FieldSet`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::FieldSet;
    /// let set = FieldSet::empty();
    /// assert_eq!("{empty}", format!("{}", set));
    /// ```
    pub fn empty() -> Self {
        Self { fields: vec![] }
    }

    /// Create a new `FieldSet` with a single field.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::{Field, FieldSet};
    /// let set = FieldSet::new(Field::from_bits(63, 32));
    /// assert_eq!(format!("{}", set), "{<63:32>}");
    /// ```
    pub fn new(field: Field) -> Self {
        if field.is_empty() {
            // Only store non-empty fields.
            Self::empty()
        } else {
            Self {
                fields: vec![field],
            }
        }
    }

    /// Add `other` to this `FieldSet`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::{Field, FieldSet};
    /// let set = FieldSet::empty().with(Field::from_bits(44, 40));
    /// assert_eq!("{<44:40>}", format!("{}", set));
    /// ```
    pub fn with(mut self, other: Field) -> Self {
        self.include(other);
        self
    }

    /// Combine `other` into this `FieldSet`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::{Field, FieldSet};
    /// let set1 = FieldSet::new(Field::from_bits(63, 49));
    /// let set2 = set1.with_set(&FieldSet::new(Field::from_bits(44, 40)));
    /// assert_eq!("{<63:49>,<44:40>}", format!("{}", set2));
    /// ```
    pub fn with_set(mut self, other: &FieldSet) -> Self {
        self.include_set(other);
        self
    }

    /// Add `other` to this `FieldSet`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::{Field, FieldSet};
    /// let mut set = FieldSet::empty();
    /// set.include(Field::from_bits(44, 40));
    /// assert_eq!("{<44:40>}", format!("{}", set));
    /// set.include(Field::from_bits(50, 47));
    /// assert_eq!("{<50:47>,<44:40>}", format!("{}", set));
    /// set.include(Field::from_bits(46, 44));
    /// assert_eq!("{<50:40>}", format!("{}", set));
    /// ```
    pub fn include(&mut self, other: Field) {
        self.fields.push(other);
        self.canonicalise();
    }

    /// Combine `other` into this `FieldSet`.
    ///
    /// # Examples
    ///
    /// ```
    /// use capinfo::field::{Field, FieldSet};
    /// let mut set = FieldSet::new(Field::from_bits(63, 49));
    /// set.include_set(&FieldSet::new(Field::from_bits(44, 40)));
    /// assert_eq!("{<63:49>,<44:40>}", format!("{}", set));
    /// ```
    pub fn include_set(&mut self, other: &FieldSet) {
        self.fields.extend(other.fields.iter().copied());
        self.canonicalise();
    }

    /// Ensure that:
    ///   - Fields are in msb->lsb order.
    ///   - Adjacent and overlapping fields are merged.
    ///   - Empty fields are removed.
    fn canonicalise(&mut self) {
        self.fields.sort_by_key(|&f| std::cmp::Reverse(f));
        let mut fields = self.fields.iter_mut();
        if let Some(mut a) = fields.next() {
            for b in fields {
                if a.lsb() <= (b.msb() + 1) {
                    *a = Field::from_bits(a.msb(), b.lsb().min(a.lsb()));
                    *b = Field::empty();
                } else {
                    a = b;
                }
            }
        }
        self.fields.retain(|f| !f.is_empty());
    }

    /// Does this `FieldSet` completely cover the specified `Field`?
    pub fn contains(&self, other: Field) -> bool {
        self.fields.iter().any(|f| f.contains(other))
    }

    /// The most significant bit of any contained `Field`.
    pub fn msb(&self) -> Option<u32> {
        // `self.fields` is sorted msb->lsb, and contains no overlapping or empty fields.
        self.fields.first().map(|f| f.msb())
    }

    /// The least significant bit of any contained `Field`.
    pub fn lsb(&self) -> Option<u32> {
        // `self.fields` is sorted msb->lsb, and contains no overlapping or empty fields.
        self.fields.last().map(|f| f.lsb())
    }

    /// The smallest `Field` that encloses the whole `FieldSet`.
    ///
    /// Returns `None` if the `self` contains no fields.
    pub fn enclosing_field(&self) -> Option<Field> {
        Some(Field::from_bits(self.msb()?, self.lsb()?))
    }

    pub fn is_empty(&self) -> bool {
        self.fields.is_empty()
    }
}

impl fmt::Display for FieldSet {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if self.is_empty() {
            write!(f, "{{empty}}")
        } else {
            write!(
                f,
                "{{{}}}",
                self.fields.iter().map(|f| format!("{}", f)).format(",")
            )
        }
    }
}

impl fmt::Debug for FieldSet {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        <Self as fmt::Display>::fmt(self, f)
    }
}

impl<T> From<FieldWithValue<T>> for FieldSet {
    fn from(fv: FieldWithValue<T>) -> Self {
        Self::from(&fv)
    }
}

impl<T> From<&FieldWithValue<T>> for FieldSet {
    fn from(fv: &FieldWithValue<T>) -> Self {
        Self::new(fv.without_value())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic() {
        let field = Field::from_bits(24, 7);
        assert_eq!("<24:7>", format!("{}", field));
        assert_eq!(18, field.size_in_bits());
        assert_eq!(false, field.is_bit());
        assert_eq!(0x3ffff, field.shifted_mask_u64());
        assert_eq!(24, field.msb());
        assert_eq!(7, field.lsb());
        assert_eq!(true, field.contains(field));
        assert_eq!(true, field.contains(Field::from_bits(24, 8)));
        assert_eq!(true, field.contains(Field::from_bits(23, 7)));
        assert_eq!(true, field.contains(Field::from_bit(24)));
        assert_eq!(true, field.contains(Field::from_bit(7)));
    }

    #[test]
    fn fmt() {
        assert_eq!("<42>", Field::from_bit(42).to_string());
        assert_eq!("<59:21>", Field::from_bits(59, 21).to_string());
        assert_eq!("<empty>", Field::empty().to_string());
        assert_eq!("<empty(21:59)>", Field::from_bits(21, 59).to_string());
    }

    #[test]
    fn msb_lt_lsb() {
        assert_eq!(0, Field::from_bits(0, 1).size_in_bits());
        assert_eq!(0, Field::from_bits(0, 42).size_in_bits());
    }

    #[test]
    #[should_panic]
    fn bit_panic_0() {
        Field::from_bits(1, 0).bit();
    }

    #[test]
    #[should_panic]
    fn bit_panic_24() {
        Field::from_bits(25, 24).bit();
    }

    #[test]
    // Make sure that we can get u64 masks even if the source data is bigger than a u64.
    fn shifted_mask_u64() {
        assert_eq!(0xff, Field::from_bits(7, 0).shifted_mask_u64());
        assert_eq!(0xff, Field::from_bits(32, 25).shifted_mask_u64());
        assert_eq!(0xff, Field::from_bits(63, 56).shifted_mask_u64());
        assert_eq!(0xff, Field::from_bits(64, 57).shifted_mask_u64());
        assert_eq!(0xff, Field::from_bits(70, 63).shifted_mask_u64());
        assert_eq!(0xff, Field::from_bits(128, 121).shifted_mask_u64());
    }

    #[test]
    #[should_panic]
    fn shifted_mask_u64_panic_0() {
        Field::from_bits(64, 0).shifted_mask_u64();
    }

    #[test]
    #[should_panic]
    fn shifted_mask_u64_panic_42() {
        Field::from_bits(106, 42).shifted_mask_u64();
    }

    #[test]
    fn set_include() {
        let mut set = FieldSet::empty();
        set.include(Field::from_bits(20, 10));
        assert_eq!("{<20:10>}", format!("{}", set));
        set.include(Field::from_bits(5, 0));
        assert_eq!("{<20:10>,<5:0>}", format!("{}", set));
        set.include(Field::from_bits(40, 30));
        assert_eq!("{<40:30>,<20:10>,<5:0>}", format!("{}", set));
        set.include(Field::from_bits(29, 20));
        assert_eq!("{<40:10>,<5:0>}", format!("{}", set));
        set.include(Field::from_bits(8, 7));
        assert_eq!("{<40:10>,<8:7>,<5:0>}", format!("{}", set));
        set.include(Field::from_bits(64, 0));
        assert_eq!("{<64:0>}", format!("{}", set));
    }
}
