// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

pub mod bitwise;
pub mod capability;
pub mod field;
pub mod rich_text;

pub use capability::{morello::Morello, Capability, CapabilityWithFormat};

pub fn version() -> String {
    String::from(env!("CARGO_PKG_VERSION"))
}
