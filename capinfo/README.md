# Capinfo (core library)

Morello capability inspection library.

This implements the capability-handling logic common to the front-end UIs
included in this project.

Since this is potentially reusable by other tools, API documentation is
included, and can be generated using `cargo doc` (either locally or from the
workspace root).

Refer to the [project-level documentation][top] for further information.

[top]: ../README.md
