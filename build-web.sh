#!/usr/bin/env sh

# Copyright (c) 2021-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

set -eu

# `wasm-pack` doesn't fully understand workspaces, so build the `web` component
# by itself.
cd web

ROOT=root/capinfo
wasm-pack build --out-dir="$ROOT" --target=web -- "$@"

cat > "$ROOT/LICENSE-deps.md" <<LICENSES
Capinfo incorporates (directly or indirectly) several dependencies.

# By dependency (as published, from 'cargo license'):

$(cargo license --avoid-build-deps --avoid-dev-deps -d -a | sed -e 's/^/  - /')

# By license (as used, from 'cargo about'):

$(cargo about generate md.hbs)
LICENSES
