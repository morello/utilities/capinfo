#!/bin/bash

# Copyright (c) 2021 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

set -eu

cargo fmt -- --check
cargo clippy
cargo doc
cargo test

./build-web.sh

# Check that debug_layout builds but don't specify `--out-dir` (so it won't
# overwrite whatever `build-web.sh` built last time).
pushd web   # `wasm-pack` doesn't fully understand workspaces.
wasm-pack build --target=web -- --features=debug_layout
popd

echo -e "\033[1;33mNote: The web application has been built but requires manual testing.\033[m"
