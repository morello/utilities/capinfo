# Capinfo

Morello capability inspection tools.

Capinfo has three main components:

- A [core library][capinfo], implementing all the capability-handling logic.
- A [command-line interface][cli] to that library.
- A [graphical web interface][gui] to the same library (using [WebAssembly]).

Capinfo has been tested with [Rust] 1.60.0 and [wasm-pack] 0.10.3.

# Building and Running

The workspace is configured so that the standard `cargo` commands do the obvious
thing. `cargo run` runs the command-line interface, so Capinfo can be tested
locally:

```
cargo run -- 0x1ffffc000_4000_0001_007fffffffffffff
```

To install the `capinfo` binary on your `PATH`:

```
# From the project root:
cargo install --path cli
```

Refer to the [cargo-install] documentation for details.

## Building the web GUI

The web GUI is based on WebAssembly, and is built using [wasm-pack]:

```
./build-web.sh
```

This requires both [cargo-license] and [cargo-about].

## Deployment to morello-project.org/capinfo

The Linaro website has its own infrastructure, but needs the following files
from Capinfo:

  - `LICENSE`
  - `LICENSE-deps.md`
  - `web/root/capinfo/capinfo_web.js`
  - `web/root/capinfo/capinfo_web_bg.wasm`

These files must be copied to [`assets/capinfo/`][web-assets-capinfo], tested, and then
submitted as a pull request on [the website repository][web].

# Tests

The included `test.sh` runs several standard checks, and is used as a presubmit
script.

# Licence

Capinfo and its components are licensed under BSD-3-Clause, as described in [LICENSE].

[Rust]: https://www.rust-lang.org/tools/install
[cargo-install]: https://doc.rust-lang.org/cargo/commands/cargo-install.html
[cargo-about]: https://crates.io/crates/cargo-about
[cargo-license]: https://crates.io/crates/cargo-license
[capinfo]: capinfo/README.md
[cli]: cli/README.md
[gui]: web/README.md
[wasm-pack]: https://rustwasm.github.io/wasm-pack/installer/
[web]: https://github.com/MorelloProject/website
[web-assets-capinfo]: https://github.com/MorelloProject/website/tree/master/assets/capinfo
[WebAssembly]: https://webassembly.org/
[LICENSE]: LICENSE
