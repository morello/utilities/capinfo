// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::dom::SvgDomBuilder;
use std::fmt;
use wasm_bindgen::{prelude::*, JsCast};

// TODO: With debug_layout, automatically draw a bounding rectangle.
pub type RenderResult<'a, T> = Result<SvgDomBuilder<'a, T>, JsValue>;

/// A consistent API for UI components.
///
/// We don't, strictly, rely on this trait, but it ensures that all the components have a
/// consistent API, and should ease porting to a DOM framework later.
///
/// We don't use an existing framework now, because (at the time of writing) none of those
/// available appear to offer the features and stability we want for SVG output.
pub trait Component {
    type Outer: JsCast + fmt::Debug;
    type NewArgs;

    /// Initialise a component, but don't build the DOM.
    ///
    /// This must be infallible, and so is not expected to have DOM access.
    fn new(args: Self::NewArgs) -> Self;

    /// Update `outer` to represent this component.
    ///
    /// No assumptions are made about the initial DOM structure of `outer` (including its
    /// children); this will completely rewrite it.
    //
    // TODO: Borrow `outer` (mutably) so this can add zero or more elements of its choosing. It
    // would be cleaner in usage, but might complicate the top level Capinfo.
    fn render<'a>(&self, outer: SvgDomBuilder<'a, Self::Outer>) -> RenderResult<'a, Self::Outer>;
}
