// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    colour,
    colour::Colour,
    config::*,
    dom::SvgDomBuilder,
    layout::{Alignable, ElementGroup, HorizontalAligner, TryIntoAlignable},
    ui,
    ui::{
        component::{Component, RenderResult},
        element::{UiElement, UiElementLayout},
    },
    util::*,
};
use capinfo::{capability::explanation::Explanation, rich_text::RichText};
use itertools::Itertools;
use wasm_bindgen::prelude::*;
use web_sys::{SvgRectElement, SvgTextElement, SvggElement};

/// A visualisation of a `capinfo::capability::Explanation`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UiExplanation {
    title: RichText<String>,
    colour: Colour,
    elements: Vec<UiElement>,
}

impl Component for UiExplanation {
    type Outer = SvggElement;
    type NewArgs = Explanation;

    fn new(mut subject: Explanation) -> Self {
        let colour = Self::colour_for(&subject);
        Self {
            colour,
            title: subject.take_title(),
            elements: subject
                .take_elements()
                .into_iter()
                .map(|e| UiElement::new((colour, e)))
                .collect(),
        }
    }

    /// Render the `UiExplanation`.
    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, SvggElement>,
    ) -> RenderResult<'a, SvggElement> {
        outer.add_class("explanation");

        // Generate backgrounds first. We'll update the dimensions once the content is rendered.
        let mut bg_title = outer.make_child::<SvgRectElement>("rect")?;
        let mut bg_body = outer.make_child::<SvgRectElement>("rect")?;

        let mut aligner = HorizontalAligner::new();
        self.render_title(&mut aligner, &mut outer)?;
        aligner.add_margin_break(0.0);
        let title_height = aligner.min_height();
        self.render_body(&mut aligner, &mut outer)?;

        let vb = aligner.resolve()?.viewbox();
        outer.override_viewbox(vb);

        let x_str = vb.min_x().to_string();
        let width_str = vb.width().to_string();
        let stroke = self.colour.darken(0.15).html();

        bg_title.add_class("bg_title");
        bg_title.set_attribute("x", &x_str)?;
        bg_title.set_attribute("y", vb.min_y().to_string())?;
        bg_title.set_attribute("width", &width_str)?;
        bg_title.set_attribute("height", title_height.to_string())?;
        bg_title.set_attribute("fill", self.colour.html())?;
        bg_title.set_attribute("stroke", &stroke)?;
        bg_body.add_class("bg_body");
        bg_body.set_attribute("x", &x_str)?;
        bg_body.set_attribute("y", (vb.min_y() + title_height).to_string())?;
        bg_body.set_attribute("width", &width_str)?;
        bg_body.set_attribute("height", (vb.height() - title_height).to_string())?;
        bg_body.set_attribute(
            "fill",
            self.colour
                .blended_with(&colour::arm::LIGHT_GREY, 0.1)
                .html(),
        )?;
        bg_body.set_attribute("stroke", &stroke)?;

        #[cfg(feature = "debug_layout")]
        outer.make_debug_rect(vb, "#993333")?;

        Ok(outer)
    }
}

impl UiExplanation {
    pub fn title(&self) -> RichText<&str> {
        self.title.as_str()
    }

    fn colour_for(subject: &Explanation) -> Colour {
        colour_for_explanation(subject).unwrap_or(colour::arm::GREY)
    }

    fn render_title<'a>(
        &self,
        ha: &mut HorizontalAligner<'a>,
        outer: &mut SvgDomBuilder<'a, SvggElement>,
    ) -> Result<(), JsValue> {
        let mut text = outer.make_child::<SvgTextElement>("text")?;
        text.make_rich_text(self.title())?;
        text.set_attribute("font-weight", "bold")?;
        text.set_attribute("font-size", format!("{}", EXPLANATION_TITLE_FONT_SIZE))?;
        text.set_attribute("text-anchor", "middle")?;
        text.force_line_height(EXPLANATION_TITLE_LINE_HEIGHT - 2.0 * TEXT_MARGIN)?;
        ha.try_add_centre(text, TEXT_MARGIN).or_else(|_| {
            error(format!("Could not align title for '{}'.", self.title()));
            Ok(()) // Continue anyway.
        })
    }

    fn render_body<'a>(
        &self,
        ha: &mut HorizontalAligner<'a>,
        outer: &mut SvgDomBuilder<'a, SvggElement>,
    ) -> Result<(), JsValue> {
        // Render all elements first, so we can measure them.
        let elements: Vec<(&UiElement, SvgDomBuilder<SvggElement>)> = self
            .elements
            .iter()
            .map(|e| -> Result<_, JsValue> {
                Ok((e, e.render(outer.make_child::<SvggElement>("g")?)?))
            })
            .try_collect()?;
        // The smallest possible body width is the width of the largest element.
        let min_width =
            elements
                .iter()
                .fold(0.0, |max, (element, builder)| match builder.viewbox() {
                    None => max,
                    Some(vb) => (vb.width() + 2.0 * element.margin()).max(max),
                });

        for (layout, group) in &elements.into_iter().group_by(|(e, _builder)| e.layout()) {
            match layout {
                UiElementLayout::Text => self.place_text_elements(ha, group)?,
                UiElementLayout::FieldDiagram => self.place_field_diagrams(ha, group, min_width)?,
                UiElementLayout::OtherDiagram => {
                    // Centre other diagrams individually.
                    for (element, builder) in group {
                        if ha.try_add_centre(builder, element.margin()).is_err() {
                            error(format!("Could not align diagram for '{}'.", self.title()));
                            // Continue anyway.
                        }
                    }
                }
            }
        }
        Ok(())
    }

    /// Render text elements in blocks.
    fn place_text_elements<'a, 'this_fn>(
        &self,
        ha: &mut HorizontalAligner<'a>,
        group: impl Iterator<Item = (&'this_fn UiElement, SvgDomBuilder<'a, SvggElement>)>,
    ) -> Result<(), JsValue> {
        let mut inner_ha = HorizontalAligner::new();
        for (_, builder) in group {
            // Place lines together (respecting LINE_HEIGHT).
            if inner_ha.try_add_left(builder, 0.0).is_err() {
                error(format!(
                    "Could not align text element for '{}'.",
                    self.title()
                ));
                // Continue anyway.
            }
        }
        if inner_ha.element_count() == 1 {
            // Centre standalone text.
            ha.add_centre(inner_ha.resolve()?, TEXT_MARGIN);
        } else {
            // Left-align multiline text.
            ha.add_left(inner_ha.resolve()?, TEXT_MARGIN);
        }
        Ok(())
    }

    /// Render field diagrams, placing them side-by-side on a single line if they'll fit nicely,
    /// otherwise on seperate lines, aligned by their least-significant bit.
    fn place_field_diagrams<'a, 'this_fn>(
        &self,
        ha: &mut HorizontalAligner<'a>,
        group: impl Iterator<Item = (&'this_fn UiElement, SvgDomBuilder<'a, SvggElement>)>,
        min_width: f32,
    ) -> Result<(), JsValue> {
        // Place everything on separate lines first, but measure each diagram as we go.
        let mut inner_ha = HorizontalAligner::new();
        // Account for DIAG_MARGIN on either side, and negate first DIAG_HORIZONTAL_MARGIN to
        // simplify accumulation.
        let mut inline_width = 2.0 * DIAG_MARGIN - DIAG_HORIZONTAL_MARGIN;
        let mut inline_min_y = None;
        let mut inline_max_y = None;
        for (element, builder) in group {
            if let Ok(alignable) = builder.try_into_alignable() {
                let vb = alignable.viewbox().extended_y_by(DIAG_MARGIN);
                let lsb_x = element
                    .field_lsb_x()
                    .expect("Expected a `Field` with an `lsb_x()`.");
                inner_ha.add_offset(alignable, -lsb_x, DIAG_MARGIN);
                // Measure the size required for each element to be on the same line.
                inline_width += vb.width() + DIAG_HORIZONTAL_MARGIN;
                let min_y = vb.min_y();
                let max_y = vb.max_y();
                inline_min_y = Some(inline_min_y.unwrap_or(min_y).min(min_y));
                inline_max_y = Some(inline_max_y.unwrap_or(max_y).max(max_y));
            } else {
                error(format!(
                    "Could not align field diagram for '{}'.",
                    self.title()
                ));
                // Continue anyway.
            }
        }
        let inline_height = match (inline_max_y, inline_min_y) {
            (Some(max), Some(min)) => max - min,
            _ => 0.0,
        };

        // If it fits better, place small fields side-by-side on a single line.
        let inline_area = inline_width * inline_height;
        if inline_area <= (min_width * inner_ha.min_height() * INLINE_DIAGRAM_PREFERENCE) {
            let mut x = 0.0;
            let placed = inner_ha.take_raw_elements().map(|mut alignable| {
                // We don't strictly need to translate by -MAIN_MIN_Y yet, but we need it to exist
                alignable.translate(x, -ui::field::MAIN_MIN_Y)?;
                x += alignable.viewbox().width() + DIAG_HORIZONTAL_MARGIN;
                Ok(alignable)
            });
            inner_ha.add_centre(ElementGroup::try_with_elements(placed)?, DIAG_MARGIN)
        }

        // Whatever we did, center the inner aligner in the provided one.
        ha.add_centre(inner_ha.resolve()?, TEXT_MARGIN);
        Ok(())
    }
}
