// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    colour,
    config::*,
    dom::SvgDomBuilder,
    layout::{Alignable, ElementGroup, HorizontalAligner, TryIntoAlignable},
    ui::component::{Component, RenderResult},
    util::*,
    viewbox::Viewbox,
};
use capinfo::{capability, capability::CapabilityWithFormat};
use itertools::Itertools;
use unicode_segmentation::UnicodeSegmentation;
use wasm_bindgen::prelude::*;
use web_sys::{SvgRectElement, SvgTextElement, SvggElement};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UiError {
    input: String,
    parse_error: capability::error::ParseError,
}

impl Component for UiError {
    type Outer = SvggElement;
    type NewArgs = (String, capability::error::ParseError);

    fn new(args: (String, capability::error::ParseError)) -> Self {
        let (input, parse_error) = args;
        log(format!("UiError::new('{}', {})", &input, &parse_error));
        Self { input, parse_error }
    }

    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, SvggElement>,
    ) -> RenderResult<'a, SvggElement> {
        log(format!(
            "UiError::render('{}', {})",
            self.input, self.parse_error
        ));
        outer.add_class("cap_error");
        outer.set_attribute("font-size", FONT_SIZE.to_string())?;
        outer.set_attribute("font-family", FONT_FAMILY)?;

        let mut aligner = HorizontalAligner::new();

        // Display the basic description.
        let mut desc = outer.make_child::<SvgTextElement>("text")?;
        desc.set_attribute("y", "0.0")?;
        desc.set_attribute("text-anchor", "middle")?;
        desc.make_rich_text(self.parse_error.to_string())?;
        if aligner.try_add_centre(desc, TEXT_MARGIN).is_err() {
            error("Could not align error description.");
            // Continue anyway.
        }

        aligner.add_centre(self.render_diagram(&mut outer)?, DIAG_MARGIN);
        aligner.add_margin_break(DIAG_MARGIN * 2.0);

        // Display usage information.
        for line in CapabilityWithFormat::legal_formats().lines() {
            let mut text = outer.make_child::<SvgTextElement>("text")?;
            text.make_rich_text(line)?;
            text.force_line_height(LINE_HEIGHT)?;
            if let Some(vb) = text.viewbox() {
                text.override_viewbox(vb.extended_x(0.0));
            }
            if aligner.try_add_left(text, 0.0).is_err() {
                error("Could not align usage text.");
                // Don't bother trying other lines.
                break;
            }
        }

        outer.override_viewbox(aligner.resolve()?.viewbox());
        Ok(outer)
    }
}

impl UiError {
    fn render_diagram<'a>(
        &self,
        outer: &mut SvgDomBuilder<'a, SvggElement>,
    ) -> Result<ElementGroup<'a>, JsValue> {
        // We're going to render each input character, one by one, with highlights behind the bad
        // ones.
        let mut highlights = outer.make_child::<SvggElement>("g")?;
        let mut digits = outer.make_child::<SvggElement>("g")?;
        digits.set_attribute("font-family", MONO_FONT_FAMILY)?;
        digits.set_attribute("text-anchor", "middle")?;

        // Render digits, keeping track of grapheme boundaries. This should be a fixed-width font,
        // but we need to measure it anyway, and the fixed-width property often doesn't apply to
        // all graphemes, so we just measure every digit.
        let mut digits_vb = Viewbox::empty(0.0, 0.0);
        let digit_viewboxes: Vec<Viewbox> = self
            .input
            .graphemes(true) // is_extended = true, recommended "for general processing".
            .map::<Result<_, JsValue>, _>(|g| {
                let x = digits_vb.width();
                let mut text = digits.make_child::<SvgTextElement>("text")?;
                text.set_attribute("x", x.to_string())?;
                text.set_text_content(g);

                let vb = text.viewbox().unwrap_or_else(|| Viewbox::empty(x, 0.0));
                digits_vb.extend(&vb);
                digits_vb.extend_x(digits_vb.max_x() + ERROR_CHAR_MARGIN);
                #[cfg(feature = "debug_layout")]
                digits.make_debug_rect(vb, "#339933")?;

                Ok(vb.extended_by(ERROR_CHAR_MARGIN / 2.0))
            })
            .try_collect()?;
        digits_vb.extend_y_by(ERROR_CHAR_MARGIN);

        // Flags are provided grouped, where each group roughly represents one
        // error. However, note that the flags in each group may be sparse (e.g.
        // ignoring separator characters), or absent entirely (e.g. for missing
        // fields).
        let y_str = digits_vb.min_y().to_string();
        let height_str = digits_vb.height().to_string();
        let groups = self.parse_error.flagged_groups();
        let r_str = ERROR_CHAR_MARGIN.to_string();
        for flags in groups.filter(|flags| flags.any()) {
            // Highlight the whole group.
            let group = flags
                .filled()
                .runs()
                .next()
                .expect("Empty flag groups were filtered out");
            let min_x = digit_viewboxes[*group.start()].min_x();
            let max_x = digit_viewboxes[*group.end()].max_x();
            let mut hl = highlights.make_child::<SvgRectElement>("rect")?;
            hl.set_attribute("x", min_x.to_string())?;
            hl.set_attribute("width", (max_x - min_x).to_string())?;
            hl.set_attribute("y", &y_str)?;
            hl.set_attribute("height", &height_str)?;
            hl.set_attribute("fill", colour::arm::ORANGE.lighten(0.85).html())?;
            hl.set_attribute("stroke", "none")?;
            hl.set_attribute("rx", &r_str)?;
            hl.set_attribute("ry", &r_str)?;

            // Outline adjacent flagged characters.
            for run in flags.runs() {
                let min_x = digit_viewboxes[*run.start()].min_x();
                let max_x = digit_viewboxes[*run.end()].max_x();
                let mut ol = highlights.make_child::<SvgRectElement>("rect")?;
                ol.set_attribute("x", min_x.to_string())?;
                ol.set_attribute("width", (max_x - min_x).to_string())?;
                ol.set_attribute("y", &y_str)?;
                ol.set_attribute("height", &height_str)?;
                ol.set_attribute("fill", "none")?;
                ol.set_attribute("stroke", colour::arm::ORANGE.html())?;
                ol.set_attribute("rx", &r_str)?;
                ol.set_attribute("ry", &r_str)?;
            }
        }

        if let (Ok(highlights), Ok(digits)) =
            (highlights.try_into_alignable(), digits.try_into_alignable())
        {
            let elements = std::iter::once(highlights).chain(std::iter::once(digits));
            Ok(ElementGroup::with_elements(elements))
        } else {
            error("Could not measure error diagram for alignment.");
            Ok(ElementGroup::empty())
        }
    }
}
