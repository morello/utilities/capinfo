// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    config::*,
    dom::SvgDomBuilder,
    ui::{
        component::{Component, RenderResult},
        explanation::UiExplanation,
    },
    util::*,
    viewbox::Viewbox,
};
use capinfo::{
    capability,
    field::{Field, FieldSet},
    rich_text::RichText,
};
use itertools::Itertools;
use std::fmt;
use wasm_bindgen::prelude::*;
use web_sys::SvggElement;

#[derive(Clone, Debug)]
pub struct UiExplanationView {
    // Stored with the generation and `fields()`, sorted for display.
    explanations: Vec<ExplanationWithMeta>,
    cap_field: Field,
    msx: f32,
    lsx: f32,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Properties {
    explanations: Vec<capability::Explanation>,
    cap_field: Field,
    msx: f32,
    lsx: f32,
}

#[derive(Clone, Debug)]
struct ExplanationWithMeta {
    generation: u32,
    fields: FieldSet,
    ui: UiExplanation,
}

impl ExplanationWithMeta {
    fn top_to_bottom_key(&self) -> impl Ord {
        self.generation
    }

    fn left_to_right_key(&self) -> impl Ord {
        // The index of the centre bit, doubled to avoid rounding.
        match self.fields.enclosing_field() {
            None => {
                warn(format!(
                    "No `enclosing_field()` for explanation: {}",
                    self.ui.title()
                ));
                0
            }
            Some(field) => !(field.msb() + field.lsb()),
        }
    }
}

impl Properties {
    /// Properties for `UiExplanationView`.
    ///
    /// `explanations`: The explanations to display.
    /// `cap_field`: A `Field` covering the capability being explained.
    /// `bit_x`: A function mapping bit indices onto X co-ordinates. This must return a finite
    /// result for every bit index covered by `cap_field`.
    pub fn new(
        explanations: Vec<capability::Explanation>,
        cap_field: Field,
        bit_x: impl Fn(u32) -> f32,
    ) -> Self {
        // We sort explanations during `render()`, with knowledge of their dimensions, so don't
        // bother here. If the input order is unstable, we may end up re-rendering unnecessarily,
        // but this is unlikely (and probably benign anyway).
        Self {
            explanations,
            msx: bit_x(cap_field.msb()),
            lsx: bit_x(cap_field.lsb()),
            cap_field,
        }
    }
}

impl Component for UiExplanationView {
    type Outer = SvggElement;
    type NewArgs = Properties;

    fn new(args: Properties) -> Self {
        let Properties {
            explanations,
            cap_field,
            msx,
            lsx,
        } = args;
        log(format!(
            "UiExplanationView::new({{'{}'}})",
            explanations.iter().map(|e| e.title()).format("', '")
        ));
        Self {
            explanations: explanations
                .into_iter()
                .map(|mut e| ExplanationWithMeta {
                    generation: e.generation(),
                    fields: e.take_fields(),
                    ui: UiExplanation::new(e),
                })
                .collect(),
            cap_field,
            msx,
            lsx,
        }
    }

    /// Render the `UiExplanationView` to align with the capability layout described by the last
    /// `new()` or `update()`.
    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, SvggElement>,
    ) -> RenderResult<'a, SvggElement> {
        let _cg = ConsoleGroup::new_collapsed("UiExplanationView::render()");
        outer.add_class("explanation_view");

        // Group explanations into rows.
        #[derive(Debug)]
        struct Row<'a> {
            min_width: f32,
            expls: Vec<RenderedExplanation<'a>>,
        }

        // Render all the explanations first (so we can measure them).
        let rendered: Vec<_> = self
            .explanations
            .iter()
            .sorted_by_key(|e| (e.top_to_bottom_key(), e.left_to_right_key()))
            // Start with each explanation on its own row.
            .map::<Result<_, JsValue>, _>(|expl_with_meta| {
                match RenderedExplanation::new(
                    expl_with_meta,
                    expl_with_meta.ui.render(outer.make_child("g")?)?,
                ) {
                    // We get `None` if the `viewbox()` couldn't be measured. Ignore such
                    // explanations, but carry on.
                    None => Ok(Row {
                        min_width: 0.0,
                        expls: Vec::new(),
                    }),
                    Some(re) => Ok(Row {
                        min_width: re.viewbox().width() + EXPLANATION_MARGIN,
                        expls: vec![re],
                    }),
                }
            })
            .try_collect()?;

        // Heuristic: allow explanations to spill a little over the edges of the field.
        let max_width = (self.lsx - self.msx) * 1.0625;

        // Place rendered explanations.
        let mut y = EXPLANATION_MARGIN;
        for row in rendered
            .into_iter()
            // Merge into rows as long as they fit.
            .coalesce(|mut a, mut b| {
                let combined = a.min_width + b.min_width;
                if combined <= max_width {
                    a.expls.append(&mut b.expls);
                    Ok(Row {
                        min_width: combined,
                        expls: a.expls,
                    })
                } else {
                    Err((a, b))
                }
            })
        {
            #[cfg(feature = "debug_layout")]
            {
                let line_y = y - EXPLANATION_MARGIN / 2.0;
                outer.make_debug_line((self.msx, line_y), (self.lsx, line_y), "#993333")?;
            }

            y += self.place_row(row.expls, y).height() + EXPLANATION_MARGIN;
        }

        if let Some(vb) = outer.viewbox() {
            outer.override_viewbox(vb.extended_y(0.0).extended_x_by(EXPLANATION_MARGIN));
        }
        Ok(outer)
    }
}

impl UiExplanationView {
    /// The X co-ordinate of the centre of the specified bit.
    ///
    /// The actual bits are rendered by `UiCapOverview`, but explanations are placed (horizontally)
    /// to align with relevant fields.
    pub fn bit_x(&self, idx: u32) -> f32 {
        if self.cap_field.size_in_bits() < 2 {
            error("Cannot extrapolate bit_x() for capability with fewer than 2 bits.");
            return (self.msx + self.lsx) / 2.0;
        }
        let scale_den = (self.cap_field.size_in_bits() - 1) as f32;
        let scale_num = self.msx - self.lsx;
        self.lsx + ((idx as f32) - (self.cap_field.lsb() as f32)) * scale_num / scale_den
    }

    fn place_row(&self, mut rendered: Vec<RenderedExplanation>, y: f32) -> Viewbox {
        let _cg = ConsoleGroup::new_collapsed("UiExplanationView::place_generation()");
        // The horizontal placement algorithm starts with each explanation centered over the
        // fields it describes, then shuffles each explanation left or right until they no longer
        // overlap.
        rendered.sort_by_key(|r| r.subject().left_to_right_key());
        let mut placements = rendered
            .into_iter()
            .map(|r| self.place_provisionally(r, y))
            .collect_vec();

        // Find placement pairs that are too close to one another (or overlapping). Move them apart
        // and combine them into a single placement. Repeat until everything is suitably spaced.
        let mut combine = || -> bool {
            let mut combined = false;
            let mut non_empty = placements.iter_mut().filter(|p| !p.is_empty());
            if let Some(mut a) = non_empty.next() {
                for b in non_empty {
                    if a.is_too_close_to(b) {
                        combined = true;
                        a.combine(b);
                    } else {
                        a = b;
                    }
                }
            }
            combined
        };
        while combine() {}
        placements.retain(|p| !p.is_empty());

        placements
            .iter()
            .map(|p| p.viewbox)
            .reduce(|acc, vb| acc.extended(&vb))
            .expect("Attempt to place an empty explanation row.")
    }

    fn place_provisionally<'a>(&self, r: RenderedExplanation<'a>, y: f32) -> Placement<'a> {
        let title = r.title();
        let fields = r.fields();
        let viewbox = r.viewbox;
        let half_width = viewbox.width() / 2.0;
        let half_bit = (self.bit_x(0) - self.bit_x(1)) / 2.0;
        let mut min_x_hint = fields.msb().map(|b| self.bit_x(b)).unwrap_or(self.msx);
        let mut max_x_hint = fields.lsb().map(|b| self.bit_x(b)).unwrap_or(self.lsx);
        let centre_hint = (max_x_hint + min_x_hint) / 2.0;
        // Try to keep explanations from sticking out past the overview, at least unless pushed out
        // by other explanations after provisional placement.
        let overlaps_left = (centre_hint - half_width) < (self.msx - half_bit);
        let overlaps_right = (centre_hint + half_width) > (self.lsx + half_bit);
        let msg;
        match (overlaps_left, overlaps_right) {
            (true, true) => {
                // Centre very wide explanations (protruding from both sides).
                msg = "overlaps both edges. Moving hints to";
                min_x_hint = self.msx - half_bit;
                max_x_hint = self.lsx + half_bit;
            }
            (false, true) => {
                // Right-align.
                msg = "overlaps right edge. Moving hints to";
                max_x_hint = self.lsx + half_bit;
                min_x_hint = max_x_hint - viewbox.width();
            }
            (true, false) => {
                // Left-align.
                msg = "overlaps left edge. Moving hints to";
                min_x_hint = self.msx - half_bit;
                max_x_hint = min_x_hint + viewbox.width();
            }
            (false, false) => {
                // Otherwise, widen to cover the whole bit at each end.
                msg = "position hints set to";
                min_x_hint -= half_bit;
                max_x_hint += half_bit;
            }
        }
        let centre_hint = (max_x_hint + min_x_hint) / 2.0;
        log(format!(
            "Explanation '{}' {} [{},{}]",
            title, msg, min_x_hint, max_x_hint
        ));

        // Provisional placement.
        let mut placement = Placement {
            r_expls: vec![r],
            viewbox,
            min_x_hint,
            max_x_hint,
        };
        placement.translate(centre_hint - viewbox.centre_x(), y - viewbox.min_y());
        placement
    }
}

/// A UiExplanation, with a description of where it is rendered.
#[derive(Debug)]
struct RenderedExplanation<'a> {
    subject: &'a ExplanationWithMeta,
    outer: SvgDomBuilder<'a, SvggElement>,
    viewbox: Viewbox,
    dx: f32,
    dy: f32,
}

impl<'a> RenderedExplanation<'a> {
    fn new(
        subject: &'a ExplanationWithMeta,
        outer: SvgDomBuilder<'a, SvggElement>,
    ) -> Option<Self> {
        if let Some(viewbox) = outer.viewbox() {
            Some(Self {
                subject,
                outer,
                viewbox,
                dx: 0.0,
                dy: 0.0,
            })
        } else {
            warn(format!(
                "Could not retrieve viewbox for explanation: {}",
                subject.ui.title()
            ));
            None
        }
    }

    /// Translate, keeping track of changes to the viewbox.
    fn translate(&mut self, x: f32, y: f32) {
        self.dx += x;
        self.dy += y;
    }

    fn viewbox(&self) -> Viewbox {
        self.viewbox.translated(self.dx, self.dy)
    }

    fn subject(&self) -> &ExplanationWithMeta {
        self.subject
    }

    fn title(&self) -> RichText<&str> {
        self.subject.ui.title()
    }

    fn fields(&self) -> &FieldSet {
        &self.subject.fields
    }
}

impl<'a> std::ops::Drop for RenderedExplanation<'a> {
    fn drop(&mut self) {
        if self.outer.translate(self.dx, self.dy).is_err() {
            error("RenderedExplanation::drop(): could not translate.");
        }
    }
}

/// Zero or more horizontally-adjacent explanations.
#[derive(Debug)]
struct Placement<'a> {
    r_expls: Vec<RenderedExplanation<'a>>,
    // The viewbox containing the whole group of explanations.
    viewbox: Viewbox,
    // Positioning hints.
    min_x_hint: f32,
    max_x_hint: f32,
}

impl<'a> Placement<'a> {
    fn translate(&mut self, x: f32, y: f32) {
        self.viewbox.translate(x, y);
        for r in self.r_expls.iter_mut() {
            r.translate(x, y);
        }
    }

    fn is_empty(&self) -> bool {
        self.r_expls.is_empty() || self.viewbox.is_empty()
    }

    fn is_too_close_to(&self, other: &Placement<'a>) -> bool {
        self.viewbox
            .extended_x_by(EXPLANATION_MARGIN)
            .overlaps(&other.viewbox)
    }

    /// Move all of `other` onto the end of `Self`, leaving `other` empty, and translate the
    /// newly-grouped explanations so that they don't overlap.
    fn combine(&mut self, other: &mut Self) {
        log(format!("Combining placements: {} and {}.", self, other));
        // Empty placements should already be filtered out.
        assert!(!self.is_empty());
        assert!(!other.is_empty());

        // TODO: This produces good results with a simple algorithm, but taking into account only
        // the extremes ({min,max}_x_hint) sometimes does odd things things when multiple
        // placements get pulled left and right as they are merged. Consider improving it.

        self.min_x_hint = self.min_x_hint.min(other.min_x_hint);
        self.max_x_hint = self.max_x_hint.max(other.max_x_hint);

        let width = self.viewbox.width() + EXPLANATION_MARGIN + other.viewbox.width();
        let centre_x = (self.min_x_hint + self.max_x_hint) / 2.0;
        let min_x = centre_x - (width / 2.0);
        let max_x = centre_x + (width / 2.0);
        self.translate(min_x - self.viewbox.min_x(), 0.0);
        other.translate(max_x - other.viewbox.max_x(), 0.0);

        self.r_expls.append(&mut other.r_expls);
        self.viewbox.extend(&other.viewbox);
        assert!(other.is_empty());
    }
}

impl<'a> fmt::Display for Placement<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if self.is_empty() {
            write!(f, "{{empty}}")
        } else {
            let titles = self.r_expls.iter().map(|r| r.title());
            write!(f, "{{'{}'}}", titles.format("', '"))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bit_x() {
        let ev = UiExplanationView {
            explanations: vec![],
            cap_field: Field::from_bits(15, 6),
            msx: 15.0,
            lsx: 6.0,
        };
        assert_eq!(20.0, ev.bit_x(20));
        assert_eq!(15.0, ev.bit_x(15));
        assert_eq!(11.0, ev.bit_x(11));
        assert_eq!(7.0, ev.bit_x(7));
        assert_eq!(6.0, ev.bit_x(6));
        assert_eq!(0.0, ev.bit_x(0));
    }
}
