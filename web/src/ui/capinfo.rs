// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    colour,
    dom::SvgDomBuilder,
    ui,
    ui::{
        cap_overview::UiCapOverview,
        component::{Component, RenderResult},
        error::UiError,
        explanation_view::UiExplanationView,
    },
    util::*,
};
use capinfo::{
    capability::{
        error, explanation::Verbosity, morello::Morello, Capability, CapabilityWithFormat,
    },
    field::FieldSet,
};
use std::{convert::TryFrom, fmt};
use wasm_bindgen::prelude::*;
use web_sys::{SvgDefsElement, SvgPatternElement, SvgRectElement, SvgsvgElement};

#[derive(Clone, Debug)]
pub struct UiCapinfo {
    cap: ParsedCap,
    view: View,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ParsedCap(Result<CapabilityWithFormat, (String, error::ParseError)>);

#[derive(Clone, Debug)]
enum View {
    Cap {
        overview: UiCapOverview,
        explanation_view: UiExplanationView,
    },
    Error(UiError),
}

impl ParsedCap {
    fn new(input: impl Into<String>, verbosity: Verbosity) -> Self {
        let input = input.into();
        Self(
            Morello::try_from(input.as_str())
                .map(|mut m| {
                    m.set_verbosity(verbosity);
                    CapabilityWithFormat::Morello(m)
                })
                .map_err(|e| (input, e)),
        )
    }
}

impl fmt::Display for ParsedCap {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match &self.0 {
            Ok(cf) => cf.fmt(f),
            Err((input, err)) => write!(f, "{}: {}", err, input),
        }
    }
}

impl Component for UiCapinfo {
    type Outer = SvgsvgElement;
    type NewArgs = ParsedCap;

    fn new(cap: ParsedCap) -> Self {
        let _cg = ConsoleGroup::new_collapsed(format!("UiCapinfo::new({})", &cap));
        match cap.0.as_ref() {
            Ok(&cf) => {
                log("Replacing view with capability.");
                let expls = cf.explanations();
                let explained = expls
                    .iter()
                    .fold(FieldSet::empty(), |fs, expl| fs.with_set(expl.fields()));
                let overview_props = ui::cap_overview::Properties::new(cf, explained);
                let overview = UiCapOverview::new(overview_props);
                let expl_props =
                    ui::explanation_view::Properties::new(expls, cf.as_field(), |idx| {
                        overview.bit_x(idx)
                    });
                let explanation_view = UiExplanationView::new(expl_props);
                Self {
                    view: View::Cap {
                        overview,
                        explanation_view,
                    },
                    cap,
                }
            }
            Err((input, parse_error)) => {
                log("Replacing view with error.");
                Self {
                    view: View::Error(UiError::new((input.clone(), parse_error.clone()))),
                    cap,
                }
            }
        }
    }

    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, SvgsvgElement>,
    ) -> RenderResult<'a, SvgsvgElement> {
        log(format!("UiCapinfo::render() with cap: {}", &self.cap));
        outer.add_class("capinfo");

        self.render_defs(outer.make_child("defs")?)?;

        let viewbox;
        match &self.view {
            View::Cap {
                overview,
                explanation_view,
            } => {
                let overview_g = overview.render(outer.make_child("g")?)?;
                let mut expl_g = explanation_view.render(outer.make_child("g")?)?;

                if let (Some(overview_vb), Some(mut expl_vb)) =
                    (overview_g.viewbox(), expl_g.viewbox())
                {
                    log(format!("Overview viewbox: {}", &overview_vb));
                    // The `UiExplanationView` positions itself horizontally, but we need to slide it
                    // down, below the `UiCapOverview`.
                    let delta_y = overview_vb.max_y() - expl_vb.min_y();
                    expl_g.translate(0.0, delta_y)?;
                    expl_vb.translate(0.0, delta_y);
                    log(format!("UiExplanationView viewbox: {}", &expl_vb));

                    viewbox = Some(overview_vb.extended(&expl_vb));
                } else {
                    viewbox = None;
                }
            }
            View::Error(ui_error) => {
                viewbox = ui_error.render(outer.make_child("g")?)?.viewbox();
            }
        }

        if let Some(vb) = viewbox {
            // Make the SVG slightly larger than the viewbox so that outline strokes on the edge
            // don't get clipped.
            outer.fit_to_viewbox(vb.extended_by(2.0))?;
        }

        Ok(outer)
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UpdateResult {
    parsed_ok: bool,
    render_needed: bool,
}

impl UpdateResult {
    pub fn parsed_ok(&self) -> bool {
        self.parsed_ok
    }
    pub fn render_needed(&self) -> bool {
        self.render_needed
    }
}

impl UiCapinfo {
    pub fn from_string<S: Into<String>>(input: S, verbosity: Verbosity) -> Self {
        let mut pc = ParsedCap::new(input, verbosity);
        if let Ok(cf) = pc.0.as_mut() {
            cf.set_verbosity(verbosity);
        }
        Self::new(pc)
    }

    /// If the new `input` parses correctly, or we're already showing a parse error, `update()`
    /// with the result. A subseqent `render()` will display it.
    ///
    /// Otherwise, do nothing; this won't replace a capability display with an error.
    pub fn maybe_update_with_value(&mut self, input: String, verbosity: Verbosity) -> UpdateResult {
        let cap = ParsedCap::new(input, verbosity);
        let parsed_ok = cap.0.is_ok();
        let render_needed;
        if self.parsed_ok() && !parsed_ok {
            // Never eagerly replace a valid capability with an invalid one.
            render_needed = false;
            log(format!(
                "Skipping eager update for '{}'; parse failure.",
                &cap
            ));
        } else {
            // Request a re-render if the capability has changed.
            render_needed = self.cap != cap;
            if !render_needed {
                log(format!(
                    "Skipping eager update for '{}'; nothing has changed.",
                    &self.cap
                ));
            }
        }
        if render_needed {
            *self = Self::new(cap);
        }
        UpdateResult {
            parsed_ok,
            render_needed,
        }
    }

    /// Update with the specified capability.
    ///
    /// A subsequent `render()` will display the capability (or an error msesage if the input
    /// failed to parse).
    pub fn update_with_value(&mut self, input: String, verbosity: Verbosity) -> UpdateResult {
        let cap = ParsedCap::new(input, verbosity);
        let parsed_ok = cap.0.is_ok();
        let render_needed = self.cap != cap;
        if render_needed {
            *self = Self::new(cap);
        }
        UpdateResult {
            parsed_ok,
            render_needed,
        }
    }

    /// True if the held value parsed successfully.
    pub fn parsed_ok(&self) -> bool {
        self.cap.0.is_ok()
    }

    /// Build a <defs> block with patterns etc, to be used throughout the UI.
    fn render_defs(&self, mut defs: SvgDomBuilder<SvgDefsElement>) -> Result<(), JsValue> {
        const LINE_SPACE: &str = "8";
        const LINE_WIDTH: &str = "4";

        let mut pattern = defs.make_child::<SvgPatternElement>("pattern")?;
        pattern.set_id_global("unused_fill")?;
        pattern.set_attribute("patternUnits", "userSpaceOnUse")?;
        pattern.set_attribute("patternTransform", "rotate(-45)")?;
        pattern.set_attribute("width", LINE_SPACE)?;
        pattern.set_attribute("height", LINE_SPACE)?;
        let mut bg = pattern.make_child::<SvgRectElement>("rect")?;
        bg.set_attribute("width", LINE_SPACE)?;
        bg.set_attribute("height", LINE_SPACE)?;
        bg.set_attribute("fill", "#ffffff")?;
        let mut fg = pattern.make_child::<SvgRectElement>("rect")?;
        fg.set_attribute("width", LINE_SPACE)?;
        fg.set_attribute("height", LINE_WIDTH)?;
        fg.set_attribute("fill", colour::arm::LIGHT_GREY.html())?;

        Ok(())
    }
}
