// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    colour,
    config::*,
    dom::SvgDomBuilder,
    ui::component::{Component, RenderResult},
    util::*,
    viewbox::Viewbox,
};
use capinfo::{
    bitwise::Bitwise,
    capability::{described_field::DescribedField, explanation::composed_value::ComposedValue},
    field::{Field, FieldWithValue},
    rich_text::RichText,
};
use std::{cmp, fmt, fmt::Write};
use wasm_bindgen::prelude::*;
use web_sys::{
    SvgLineElement, SvgMaskElement, SvgPathElement, SvgRectElement, SvgTextElement, SvggElement,
};

/// The top of the field itself, excluding labels etc.
pub const MAIN_MIN_Y: f32 = 0.0;

/// `UiField` handles every bitfield-like value displayed by the UI, including the main capability
/// bit layout display.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UiField<V: Bitwise> {
    fv: FieldWithValue<V>,
    subfields: Vec<DescribedField>,
    fill: Option<colour::Colour>,
    context: Option<Field>,
    label: Option<RichText<String>>,
    bin: bool,
    hex: bool,
    number_bits: bool,
}

impl<V: Bitwise + cmp::PartialEq> Component for UiField<V> {
    type Outer = SvggElement;
    type NewArgs = Properties<V>;

    /// Construct a new `UiField`.
    fn new(args: Properties<V>) -> Self {
        // We could log the value here, like we do in `render()`, but we don't know the format
        // until the `with_*` methods are called.
        log("UiField::new(...)");
        let Properties { fv, subfields } = args;
        Self {
            fv,
            subfields,
            fill: None,
            context: None,
            label: None,
            bin: false,
            hex: false,
            number_bits: false,
        }
    }

    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, SvggElement>,
    ) -> RenderResult<'a, SvggElement> {
        log(format!("UiField::render({})", self));
        outer.add_class("field");

        let (bg_path_d, bg_path_vb) = self.calculate_bg_path_d();
        log(format!(
            "Rendering background path with viewbox {}",
            bg_path_vb
        ));
        self.render_bg_path(outer.make_child("path")?, &bg_path_d)?;

        // The combination of the background and digits (which might extend beyond the background
        // path), but not the bit or subfield labels.
        let mut main_vb = bg_path_vb;

        if self.has_subfields() {
            self.render_subfield_tints(outer.make_child("g")?, &bg_path_d, &bg_path_vb)?;
        }
        if self.has_subfield_labels() {
            self.render_subfield_labels(outer.make_child("g")?)?;
        }
        if self.number_bits {
            self.render_subfield_bit_numbers(outer.make_child("g")?)?;
        }
        if self.hex {
            main_vb.extend(&self.render_hex_digits(outer.make_child("g")?)?);
        }
        if self.bin {
            main_vb.extend(&self.render_bin_digits(outer.make_child("g")?)?);
        }
        if self.has_label() {
            if let Some(vb) = self.render_label(outer.make_child("text")?, &main_vb)? {
                main_vb.extend(&vb);
            }
        }

        #[cfg(feature = "debug_layout")]
        {
            if let Some(vb) = outer.viewbox() {
                outer.make_debug_rect(vb, "#5555aa")?;
            }
            // If we get the subfields wrong, they might obscure the breaks, so show the background
            // path as well as a simple rectangle.
            let mut outline = outer.make_child::<SvgPathElement>("path")?;
            outline.add_class("debug");
            outline.set_attribute("fill", "none")?;
            outline.set_attribute("stroke", "#5555aa")?;
            outline.set_attribute("stroke-width", "2")?;
            outline.set_attribute("stroke-dasharray", "4 2")?;
            outline.set_attribute("d", &bg_path_d)?;
        }

        Ok(outer)
    }
}

impl<V: Bitwise + Copy> From<ComposedValue<V>> for UiField<V> {
    fn from(mut cv: ComposedValue<V>) -> Self {
        let fill = match cv.has_subfields() {
            // Let `unused_fill` show areas not covered by subfields.
            true => None,
            // We have no subfields, so colour the whole thing (if a category is given).
            false => colour_for_field_category(cv.category()),
        };
        Self {
            fv: cv.field_with_value(),
            subfields: cv.take_subfields(),
            fill,
            context: cv.context(),
            label: cv.take_name(),
            // Default to binary if no format is specified.
            bin: cv.has_explicit_bin_value() || !cv.has_explicit_hex_value(),
            hex: cv.has_explicit_hex_value(),
            number_bits: cv.has_subfield_bit_numbers(),
        }
    }
}

impl<V: Bitwise> UiField<V> {
    /// Display binary digits.
    pub fn with_bin(self) -> Self {
        Self { bin: true, ..self }
    }

    /// Display hexadecimal digits.
    pub fn with_hex(self) -> Self {
        Self { hex: true, ..self }
    }

    /// Number bits on each subfield.
    pub fn with_subfield_bit_numbers(self) -> Self {
        Self {
            number_bits: true,
            ..self
        }
    }

    fn has_label(&self) -> bool {
        self.label.is_some()
    }

    fn has_subfields(&self) -> bool {
        !self.subfields.is_empty()
    }

    fn has_subfield_labels(&self) -> bool {
        self.subfields.iter().any(|sf| sf.has_name())
    }

    // ---- Co-ordinate system ----

    fn px_per_bit(&self) -> f32 {
        const BIN: f32 = PX_PER_BIT;
        const HEX: f32 = PX_PER_NIBBLE / 4.0;
        match (self.bin, self.hex) {
            (false, _) => HEX,
            (true, false) => BIN,
            (true, true) => f32::max(HEX, BIN),
        }
    }

    /// The X co-ordinate of the centre of the specified bit.
    ///
    /// Bits are indexed from whatever `field` considers to be bit 0.
    pub fn bit_x(&self, idx: u32) -> f32 {
        -(idx as f32) * self.px_per_bit()
    }

    /// The X co-ordinate of the centre of the least-significant bit.
    pub fn lsb_x(&self) -> f32 {
        self.bit_x(self.fv.lsb())
    }

    /// The bottom of the field itself, excluding labels etc.
    pub fn main_max_y(&self) -> f32 {
        let hex = self.hex_font_size().map(|s| s + TEXT_MARGIN).unwrap_or(0.0);
        let bin = self.bin_font_size().map(|s| s + TEXT_MARGIN).unwrap_or(0.0);
        hex + bin
    }

    fn bin_centre_y(&self) -> Option<f32> {
        // Measure the centre from the bottom, so that binary digits render underneath hex (if
        // we're showing both).
        self.bin_font_size()
            .map(|h| self.main_max_y() - (TEXT_MARGIN + h) / 2.0)
    }

    fn hex_centre_y(&self) -> Option<f32> {
        // Measure the centre from the top.
        self.hex_font_size()
            .map(|h| MAIN_MIN_Y + (TEXT_MARGIN + h) / 2.0)
    }

    fn bin_font_size(&self) -> Option<f32> {
        match (self.bin, self.hex) {
            (true, false) => Some(BIN_FONT_SIZE),
            (true, true) => Some(BIN_FONT_SIZE_WITH_HEX),
            (false, _) => None,
        }
    }

    fn hex_font_size(&self) -> Option<f32> {
        match (self.bin, self.hex) {
            (false, true) => Some(HEX_FONT_SIZE),
            (true, true) => Some(HEX_FONT_SIZE_WITH_BIN),
            (_, false) => None,
        }
    }

    // ---- Rendering logic ----

    /// Calculate the path that describes the outline for the main field view (exluding labels, bit
    /// numbers, etc).
    ///
    /// The result is a (`String`) value that can be assigned to a `<path>` element's `d`
    /// attribute, and a rectangular `Viewbox` that contains the whole path.
    fn calculate_bg_path_d(&self) -> (String, Viewbox) {
        // We have three field constraints:
        // - The bits we actually care about drawing.
        let field = self.fv.without_value();
        // - The amount of space to use to draw them (in case we have to round to nibble boundaries).
        let space = self.space_for_field();
        // - The context that the field came from.
        let context = self.context.unwrap_or(field);
        assert!(space.contains(field));
        assert!(context.contains(field));

        let height = self.main_max_y() - MAIN_MIN_Y;
        let half_bit = self.px_per_bit() / 2.0;
        let curve_ctrl_offset = FIELD_BREAK_CURVINESS * height;
        let break_space = FIELD_BREAK_SPACE * self.px_per_bit();
        // How much does the break curve extend beyond msx/lsx? This trivial relationship is a
        // convenient property of the simple quadratic Bézier curve we use; if the break design is
        // changed, it might be easier to query the bounding box after rendering.
        let curve_offset = curve_ctrl_offset / 2.0;

        // If we were to draw simple edges, where would they be?
        // Simple edges can be drawn under (hex) digits, for edges that aren't nibble-aligned.
        let msx_edge = self.bit_x(context.msb()) - half_bit;
        let lsx_edge = self.bit_x(context.lsb()) + half_bit;
        // If we were to draw breaks, where would they be?
        // Use `space`, not `field`, because we never want to draw breaks under a digit.
        let msx_break = self.bit_x(space.msb()) - half_bit - break_space;
        let lsx_break = self.bit_x(space.lsb()) + half_bit + break_space;
        // If simple edges would fall inside the extent (not centre) of the breaks, draw simple
        // edges.
        let (ms_broken, msx) = match msx_edge < (msx_break - curve_offset) {
            false => (false, msx_edge),
            true => (true, msx_break),
        };
        let (ls_broken, lsx) = match lsx_edge > (lsx_break + curve_offset) {
            false => (false, lsx_edge),
            true => (true, lsx_break),
        };

        let y = |y_frac| MAIN_MIN_Y + y_frac * height;

        // Wrap the computation in a lambda so we can use `?` on each `write!`.
        let compute_d = || -> Result<_, fmt::Error> {
            let mut vb = Viewbox::with_xxyy(msx, lsx, y(0.0), y(1.0));

            let mut d = String::new();
            // Start at the top left corner, and go anticlockwise.
            write!(d, "M {} {} ", msx, y(1.0))?;
            // Left (most-significant) break.
            if ms_broken {
                vb.extend_x(msx - curve_offset);
                write!(
                    d,
                    "Q {} {}, {} {} ",
                    msx - curve_ctrl_offset,
                    y(0.75),
                    msx,
                    y(0.5)
                )?;
                write!(d, "T {} {} ", msx, y(0.0))?;
            } else {
                vb.extend_x(msx);
                write!(d, "L {} {} ", msx, y(0.0))?;
            }
            // Bottom line.
            write!(d, "L {} {} ", lsx, y(0.0))?;
            // Right (least-significant) break.
            if ls_broken {
                vb.extend_x(lsx + curve_offset);
                write!(
                    d,
                    "Q {} {}, {} {} ",
                    lsx + curve_ctrl_offset,
                    y(0.25),
                    lsx,
                    y(0.5)
                )?;
                write!(d, "T {} {} ", lsx, y(1.0))?;
            } else {
                vb.extend_x(lsx);
                write!(d, "L {} {} ", lsx, y(1.0))?;
            }
            // Top line.
            write!(d, "Z")?;
            Ok((d, vb))
        };
        compute_d().expect("`write!` on `String` cannot fail.")
    }

    /// The `Field` to allow space for, when drawing the diagram.
    ///   - If printing just binary digits, this is equivalent to `self.fv.without_value()`.
    ///   - If printing hex digits, the size is rounded up to a nibble multiple, so that the final
    ///     width includes the whole top (hex) digit.
    fn space_for_field(&self) -> Field {
        if self.fv.is_empty() {
            error("UiField::space_for_field(): Field is empty.");
        }
        match self.hex {
            false => self.fv.without_value(),
            true => {
                let lsb = self.fv.lsb();
                let msb = lsb + (self.fv.size_in_nibbles() * 4) - 1;
                Field::from_bits(msb, lsb)
            }
        }
    }

    fn render_bg_path(
        &self,
        mut path: SvgDomBuilder<SvgPathElement>,
        d: &str,
    ) -> Result<(), JsValue> {
        let fill = match self.fill {
            None => path.global_id_for("unused_fill").to_url(),
            Some(colour) => colour.html(),
        };
        path.add_class("bg");
        path.set_attribute("d", d)?;
        path.set_attribute("fill", &fill)?;
        path.set_attribute("stroke", colour::arm::GREY.html())?;
        Ok(())
    }

    /// Draw coloured tints to mark each subfield.
    fn render_subfield_tints(
        &self,
        mut g: SvgDomBuilder<SvggElement>,
        bg_path_d: &str,
        bg_path_vb: &Viewbox,
    ) -> Result<(), JsValue> {
        g.add_class("subfield_tints");

        // Set up a <mask> that matches `bg_path`, to ensure that we don't draw subfields outside
        // the visible area.
        //
        // We use <mask> rather than <clipPath> because it respects the stroke width of the
        // <path>. However, it means that we have to override fill and stroke, which means we can't
        // just use <use> to copy the original. We could put it in <defs> with no fill or stroke
        // and style it on use, but it's easier to make a new <path> inside the <mask> and copy the
        // `d` attribute from `bg_path_d`.

        let half_bit = self.px_per_bit() / 2.0;

        let mut mask = g.make_child::<SvgMaskElement>("mask")?;
        let mask_id = mask.set_id_auto("bg_path")?;

        // Mask to the background path, but with a transparency to let the unused_fill pattern show
        // through a little (in the context region).
        let mut mask_path = mask.make_child::<SvgPathElement>("path")?;
        mask_path.set_attribute("d", bg_path_d)?;
        mask_path.set_attribute("fill", "#888")?;
        mask_path.set_attribute("stroke", "#888")?;
        // Make the field itself fully opaque.
        let mut mask_rect = mask.make_child::<SvgRectElement>("rect")?;
        let msx = self.bit_x(self.fv.msb()) - half_bit;
        let lsx = self.bit_x(self.fv.lsb()) + half_bit;
        let vb = Viewbox::with_xxyy(msx, lsx, MAIN_MIN_Y, self.main_max_y());
        mask_rect.set_attribute("x", vb.min_x().to_string())?;
        mask_rect.set_attribute("y", vb.min_y().to_string())?;
        mask_rect.set_attribute("width", vb.width().to_string())?;
        mask_rect.set_attribute("height", vb.height().to_string())?;
        mask_rect.set_attribute("fill", "white")?;
        mask_rect.set_attribute("stroke", "white")?;

        // <mask> affects visibility, but not the bounding box, so we also need to clip the
        // subfield <rect>s to the bounding box of `bg_path`.
        // Extend the width by a stroke width, otherwise the subfield borders will always be
        // visible on the extreme edges. Half a stroke width ought to suffice, but broken edges
        // are (gentle) curves and might extend slightly beyond that.
        let clip_to = bg_path_vb.extended_x_by(1.0);

        for sf in self.subfields.iter() {
            let msx = self.bit_x(sf.field().msb()) - half_bit;
            let lsx = self.bit_x(sf.field().lsb()) + half_bit;
            let mut vb = Viewbox::with_xxyy(msx, lsx, MAIN_MIN_Y, self.main_max_y());
            // Reduce the width by almost half the stroke width so that the borders of adjacent
            // subfields don't overlap. We use _almost_ half to ensure that rendering glitches
            // don't reveal the `unused_fill` where two subfields meet.
            vb.extend_x_by(-0.49);
            vb.clip(&clip_to);

            let mut r = g.make_child::<SvgRectElement>("rect")?;
            if let Some(fill) = colour_for_field(sf) {
                r.set_attribute("fill", fill.html())?;
                r.set_attribute("stroke", fill.darken(0.15).html())?;
            } else {
                r.set_attribute("fill", "none")?;
                r.set_attribute("stroke", &colour::arm::LIGHT_GREY.darken(0.15).html())?;
            }
            r.set_attribute("x", vb.min_x().to_string())?;
            r.set_attribute("y", vb.min_y().to_string())?;
            r.set_attribute("width", vb.width().to_string())?;
            r.set_attribute("height", vb.height().to_string())?;
            r.set_attribute("mask", &mask_id.to_url())?;
        }
        Ok(())
    }

    /// Draw subfield labels in the centre of each subfield, underneath the field.
    fn render_subfield_labels(&self, mut g: SvgDomBuilder<SvggElement>) -> Result<(), JsValue> {
        g.add_class("subfield_labels");
        // This has the effect of forcing a monospace font, irrespective of RichText formatting.
        g.set_attribute("font-family", MONO_FONT_FAMILY)?;
        g.set_attribute("text-anchor", "middle")?;
        g.set_attribute("font-size", FIELD_ANNOTATION_FONT_SIZE.to_string())?;
        let labels = self
            .subfields
            .iter()
            .filter(|sf| self.fv.contains(sf.field()) && sf.has_name())
            .map(|sf| (sf.name(), sf.field()));
        // Calculate layout invariants.
        let y1 = self.main_max_y();
        let y2 = y1 + FIELD_ANNOTATION_LINE_LENGTH;
        let text_y = y2 + FIELD_ANNOTATION_FONT_SIZE;
        let y1_str = y1.to_string();
        let text_y_str = text_y.to_string();
        let stroke_str = colour::arm::DARK_GREY.html();

        let mut last_vb: Option<Viewbox> = None;
        for (name, field) in labels {
            let x = (self.bit_x(field.msb()) + self.bit_x(field.lsb())) / 2.0;
            let x_str = x.to_string();

            let mut t = g.make_child::<SvgTextElement>("text")?;
            t.set_attribute("x", &x_str)?;
            t.set_attribute("y", &text_y_str)?;
            t.make_rich_text(name)?;
            let mut delta_y = 0.0;
            if let Some(mut t_vb) = t.viewbox() {
                if let Some(last) = last_vb {
                    if last.overlaps(&t_vb) {
                        // Translate downwards to avoid overlapping labels.
                        // We only look at adjacent labels for this, and never break the lines
                        // linking labels to their fields. If we ever need to handle very dense
                        // labels, this design will need to be revised.
                        delta_y = last.max_y() - t_vb.min_y() + FIELD_ANNOTATION_TEXT_MARGIN;
                        t_vb.translate(0.0, delta_y);
                        t.translate(0.0, delta_y)?;
                    }
                }
                last_vb = Some(t_vb);
            }
            let mut l = g.make_child::<SvgLineElement>("line")?;
            l.set_attribute("x1", &x_str)?;
            l.set_attribute("y1", &y1_str)?;
            l.set_attribute("x2", &x_str)?;
            l.set_attribute("y2", (y2 + delta_y).to_string())?;
            l.set_attribute("stroke", &stroke_str)?;
        }
        Ok(())
    }

    /// Draw bit labels at the start and end of each subfield, over the field.
    fn render_subfield_bit_numbers(
        &self,
        mut g: SvgDomBuilder<SvggElement>,
    ) -> Result<(), JsValue> {
        g.add_class("subfield_bit_numbers");
        g.set_attribute("font-family", MONO_FONT_FAMILY)?;
        g.set_attribute("text-anchor", "middle")?;
        g.set_attribute("font-size", FIELD_ANNOTATION_FONT_SIZE.to_string())?;
        // Subfields are stored in lsb->msb order so we don't need to sort these.
        let bits: Vec<_> = self
            .subfields
            .iter()
            .flat_map(|sf| {
                // TODO: This allocates every time. It probably doesn't matter, but something like
                // `ArrayVec` would be neater.
                if sf.field().is_bit() {
                    // Single-bit subfields only get one label.
                    vec![sf.field().bit()]
                } else {
                    vec![sf.field().lsb(), sf.field().msb()]
                }
            })
            .filter(|&bit| (bit <= self.fv.msb()) && (bit >= self.fv.lsb()))
            .collect();

        // Calculate layout invariants.
        let y1 = MAIN_MIN_Y.to_string();
        let stroke = colour::arm::DARK_GREY.html();

        // Swap annotation line lengths for adjacent bits.
        let mut last_bit = 0u32.wrapping_sub(1); // Ensure that we start with a short line.
        let mut last_line_long = true;
        let mut next_line_length = |bit: u32| {
            let adjacent_to_last = bit.wrapping_sub(last_bit) <= 1;
            let long = last_line_long ^ adjacent_to_last;
            let length = match long {
                false => FIELD_ANNOTATION_LINE_LENGTH,
                true => FIELD_ANNOTATION_LINE_LENGTH + FIELD_ANNOTATION_FONT_SIZE,
            };
            last_bit = bit;
            last_line_long = long;
            length
        };

        for bit in bits {
            let x = self.bit_x(bit).to_string();
            let y2 = (-next_line_length(bit)).to_string();

            let mut l = g.make_child::<SvgLineElement>("line")?;
            l.set_attribute("x1", &x)?;
            l.set_attribute("y1", &y1)?;
            l.set_attribute("x2", &x)?;
            l.set_attribute("y2", &y2)?;
            l.set_attribute("stroke", &stroke)?;

            let mut t = g.make_child::<SvgTextElement>("text")?;
            t.set_attribute("x", &x)?;
            t.set_attribute("y", &y2)?;
            t.set_text_content(&bit.to_string());
        }
        Ok(())
    }

    fn render_hex_digits(&self, mut g: SvgDomBuilder<SvggElement>) -> Result<Viewbox, JsValue> {
        g.add_class("hex");

        let fmt_nibble = |idx: u32| {
            self.fv
                .value_ref()
                .nibble(idx)
                .and_then(|n| std::char::from_digit(n.into(), 16))
                .unwrap_or('?')
        };

        // Nibble bits:  |  3  |  2  |  1  |  0  |
        //                           ^        ^
        //                           |   bit_x(idx*4)
        //                      digit_x(idx)
        let digit_x = |idx: u32| self.bit_x((idx * 4) + self.fv.lsb()) - (self.px_per_bit() * 1.5);

        self.render_digits(
            g,
            fmt_nibble,
            digit_x,
            self.fv.size_in_nibbles(),
            self.hex_font_size().expect("Unknown `hex_font_size()`."),
            self.hex_centre_y().expect("Unknown `hex_centre_y()`."),
        )
    }

    fn render_bin_digits(&self, mut g: SvgDomBuilder<SvggElement>) -> Result<Viewbox, JsValue> {
        g.add_class("bin");

        let fmt_bit = |idx: u32| {
            self.fv
                .value_ref()
                .bit(idx)
                .and_then(|n| std::char::from_digit(n.into(), 2))
                .unwrap_or('?')
        };
        let digit_x = |idx: u32| self.bit_x(idx + self.fv.lsb());

        self.render_digits(
            g,
            fmt_bit,
            digit_x,
            self.fv.size_in_bits(),
            self.bin_font_size().expect("Unknown `bin_font_size()`."),
            self.bin_centre_y().expect("Unknown `bin_centre_y()`."),
        )
    }

    /// Render digits.
    ///
    /// This is agnostic between binary and hex digits, and indexes digits over `0..len` without
    /// regard for the `UiField`'s actual bit range. This permits easy hex formatting of fields
    /// that are not nibble-aligned.
    fn render_digits(
        &self,
        mut g: SvgDomBuilder<SvggElement>,
        fmt_digit: impl Fn(u32) -> char,
        digit_x: impl Fn(u32) -> f32,
        len: u32,
        font_size: f32,
        y_centre: f32,
    ) -> Result<Viewbox, JsValue> {
        g.add_class("digits");
        g.set_attribute("font-family", MONO_FONT_FAMILY)?;
        g.set_attribute("font-size", font_size.to_string())?;

        let y = y_centre.to_string();
        // Render digits in msb..lsb order so that left-to-right text selection works.
        for n in (0..len).rev() {
            let mut t = g.make_child::<SvgTextElement>("text")?;
            t.set_attribute("x", digit_x(n).to_string())?;
            t.set_attribute("y", &y)?;
            t.set_attribute("text-anchor", "middle")?;
            t.set_attribute("dominant-baseline", "central")?;
            t.set_text_content(fmt_digit(n).encode_utf8(&mut [0; 4]));
        }
        let half_digit = (digit_x(0) - digit_x(1)) / 2.0;
        let min_x = digit_x(len - 1) - half_digit;
        let max_x = digit_x(0) + half_digit;
        let min_y = y_centre - font_size / 2.0;
        let max_y = y_centre + font_size / 2.0;
        let vb = Viewbox::with_xxyy(min_x, max_x, min_y, max_y);

        #[cfg(feature = "debug_layout")]
        g.make_debug_rect(vb, "#ccee33")?;

        Ok(vb)
    }

    fn render_label(
        &self,
        mut text: SvgDomBuilder<SvgTextElement>,
        main_vb: &Viewbox,
    ) -> Result<Option<Viewbox>, JsValue> {
        text.add_class("label");
        text.set_attribute("font-family", FONT_FAMILY)?;
        text.set_attribute("font-size", FONT_SIZE.to_string())?;
        text.set_attribute("x", (main_vb.min_x() - TEXT_MARGIN).to_string())?;
        text.set_attribute("y", (main_vb.centre_y()).to_string())?;
        text.set_attribute("dominant-baseline", "central")?;
        text.set_attribute("text-anchor", "end")?;
        if let Some(label) = &self.label {
            let mut cont = text.make_rich_text(label.as_str())?;
            text.append_rich_text(":", &mut cont)?;
        }
        Ok(text.viewbox())
    }
}

impl<V: Bitwise> fmt::Display for UiField<V> {
    /// Format for nice console logging. This is not exhaustive, but should present enough
    /// information to easily identify the field.
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if let Some(label) = &self.label {
            write!(f, "UiField '{}'", label)?;
        } else {
            write!(f, "Anonymous UiField")?;
        }
        // TODO: It'd be nice to log the actual value here, but it's fiddly with only a `Bitwise`,
        // and probably not worth it.
        write!(f, " from bits <{},{}>", self.fv.msb(), self.fv.lsb())?;
        if let Some(context) = self.context {
            write!(f, " (in context <{},{}>)", context.msb(), context.lsb())?;
        }
        write!(f, " with {} subfields", self.subfields.len())?;
        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Properties<V: Bitwise + cmp::PartialEq> {
    fv: FieldWithValue<V>,
    subfields: Vec<DescribedField>,
}

impl<V: Bitwise + cmp::PartialEq> Properties<V> {
    /// Create a new `ui::field::Properties`.
    ///
    /// `fv`:
    ///    A `FieldWithValue` describing the field to display.
    ///
    /// `subfields`:
    ///    A list of subfields. Subfields must not overlap one another, and must be provided
    ///    sorted in (lsb->msb order). They do not need to be contiguous and may extend (wholly or
    ///    partially) outside the field. Only visible portions will be rendered.
    ///
    ///    Notably, when `with_context(...)` is used, subfields may be rendered outside the
    ///    primary field (but within the context).
    ///
    /// Panics if the `subfields` list does not meet these requirements.
    pub fn new(fv: FieldWithValue<V>, subfields: Vec<DescribedField>) -> Self {
        // Check that subfields are valid.
        let mut bit = 0;
        for subfield in subfields.iter() {
            assert!(subfield.field().lsb() >= bit);
            bit = subfield.field().msb() + 1;
        }
        Self { fv, subfields }
    }
}
