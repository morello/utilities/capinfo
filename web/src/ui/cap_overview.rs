// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    dom::SvgDomBuilder,
    ui,
    ui::component::{Component, RenderResult},
    ui::field::UiField,
    util::*,
};
use capinfo::{
    capability::{Capability, CapabilityWithFormat},
    field::FieldSet,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UiCapOverview {
    field: UiField<CapabilityWithFormat>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Properties {
    cap: CapabilityWithFormat,
    explained: FieldSet,
}

impl Properties {
    pub fn new(cap: CapabilityWithFormat, explained: FieldSet) -> Self {
        Self { cap, explained }
    }
}

impl Properties {
    fn for_field(&self) -> ui::field::Properties<CapabilityWithFormat> {
        let mut subfields: Vec<_> = self
            .cap
            .fields()
            .into_iter()
            .map(|df| {
                if self.explained.contains(df.field()) {
                    // Don't label fields that have dedicated explanations.
                    df.without_name()
                } else {
                    df
                }
            })
            .collect();
        subfields.sort();

        ui::field::Properties::new(self.cap.as_field_with_value(), subfields)
    }
}

impl Component for UiCapOverview {
    // A `UiCapOverview` is just a configured `UiField`, so it has the same `Outer`.
    type Outer = <UiField<CapabilityWithFormat> as Component>::Outer;
    type NewArgs = Properties;

    fn new(props: Properties) -> Self {
        log("UiCapOverview::new(..)");
        // This is a simple wrapper around `UiField`.
        let field = UiField::new(props.for_field())
            .with_bin()
            .with_hex()
            .with_subfield_bit_numbers();
        Self { field }
    }

    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, Self::Outer>,
    ) -> RenderResult<'a, Self::Outer> {
        log("UiCapOverview::render()");
        outer.add_class("cap_overview");
        self.field.render(outer)
    }
}

impl UiCapOverview {
    /// The X co-ordinate of the centre of the specified bit.
    ///
    /// Bits are indexed from whatever `field` considers to be bit 0.
    pub fn bit_x(&self, idx: u32) -> f32 {
        self.field.bit_x(idx)
    }
}
