// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! UI components.
//!
//! Many UI components correspond to structs in the core `capinfo` crate, so to avoid naming
//! confusion, all renderable UI components have names with a "Ui" prefix. E.g. `ui::UiExplanation`
//! represents a `capinfo::capability::Explanation`.

pub mod cap_overview;
pub mod capinfo;
pub mod component;
pub mod element;
pub mod error;
pub mod explanation;
pub mod explanation_view;
pub mod field;
