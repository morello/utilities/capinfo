// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    colour,
    colour::Colour,
    config::*,
    dom::SvgDomBuilder,
    layout::{Alignable, HorizontalAligner},
    ui::{
        component::{Component, RenderResult},
        field::UiField,
    },
    util::*,
};
use capinfo::{
    capability,
    capability::explanation::{permissions::Permissions, Element},
    rich_text::RichText,
};
use std::convert::TryInto;
use web_sys::{SvgRectElement, SvgTextElement, SvggElement};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum UiElementLayout {
    Text,
    FieldDiagram,
    OtherDiagram,
}

/// A visualisation of a `capinfo::capability::explanation::Element`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum UiElement {
    RichText(RichText<String>),
    // `ComposedValue`s all render as field diagrams.
    ComposedValueU32(UiField<u32>),
    ComposedValueU64(UiField<u64>),
    ComposedValueU128(UiField<u128>),
    // `ExplainedValue` reduces to `RichText`, so isn't represented explicitly.
    Permissions(Permissions, Colour),
}

impl UiElement {
    pub fn layout(&self) -> UiElementLayout {
        match self {
            Self::RichText(..) => UiElementLayout::Text,
            Self::ComposedValueU32(..) => UiElementLayout::FieldDiagram,
            Self::ComposedValueU64(..) => UiElementLayout::FieldDiagram,
            Self::ComposedValueU128(..) => UiElementLayout::FieldDiagram,
            Self::Permissions(..) => UiElementLayout::OtherDiagram,
        }
    }

    pub fn field_lsb_x(&self) -> Option<f32> {
        match self {
            Self::ComposedValueU32(f) => Some(f.lsb_x()),
            Self::ComposedValueU64(f) => Some(f.lsb_x()),
            Self::ComposedValueU128(f) => Some(f.lsb_x()),
            _ => None,
        }
    }

    pub fn margin(&self) -> f32 {
        match self {
            Self::RichText(..) => TEXT_MARGIN,
            Self::ComposedValueU32(..) => DIAG_MARGIN,
            Self::ComposedValueU64(..) => DIAG_MARGIN,
            Self::ComposedValueU128(..) => DIAG_MARGIN,
            Self::Permissions(..) => 0.0,
        }
    }
}

impl Component for UiElement {
    type Outer = SvggElement;
    type NewArgs = (Colour, Element);

    fn new(args: Self::NewArgs) -> Self {
        let (colour, subject) = (args.0, args.1);
        use capability::explanation::Element::*;
        match subject {
            RichText(rt) => Self::RichText(rt),
            ExplainedValue(ev) => Self::RichText(ev.into_rich_text()),
            ComposedValueU32(cv) => Self::ComposedValueU32(UiField::from(cv)),
            ComposedValueU64(cv) => Self::ComposedValueU64(UiField::from(cv)),
            ComposedValueU128(cv) => Self::ComposedValueU128(UiField::from(cv)),
            Permissions(p) => Self::Permissions(p, colour),
        }
    }

    fn render<'a>(
        &self,
        mut outer: SvgDomBuilder<'a, SvggElement>,
    ) -> RenderResult<'a, SvggElement> {
        outer.add_class("element");
        match self {
            UiElement::RichText(rt) => Self::render_rich_text(rt.as_str(), outer),
            UiElement::ComposedValueU32(field) => field.render(outer),
            UiElement::ComposedValueU64(field) => field.render(outer),
            UiElement::ComposedValueU128(field) => field.render(outer),
            // TODO: Add another permission display option, rendering the list top-to-bottom
            // for readability, and clearly mapping each permission onto the corresponding bit.
            // This would require more space (especially vertically), so the UiExplanationView
            // layout algorithm might need an improvement at the same time.
            UiElement::Permissions(perms, colour) => {
                Self::render_permissions_compact(perms, outer, *colour)
            }
        }
    }
}

impl UiElement {
    fn render_rich_text<'a>(
        rt: RichText<&str>,
        mut outer: SvgDomBuilder<'a, SvggElement>,
    ) -> RenderResult<'a, SvggElement> {
        let mut ha = HorizontalAligner::new();
        for line in rt.lines() {
            let mut text = outer.make_child::<SvgTextElement>("text")?;
            text.make_rich_text(line)?;
            text.force_line_height(LINE_HEIGHT)?;
            if ha.try_add_left(text, 0.0).is_err() {
                error(format!("Could not align text '{}'.", line));
                // Continue anyway.
            }
        }
        outer.override_viewbox(ha.resolve()?.viewbox());
        Ok(outer)
    }

    fn render_permissions_compact<'a>(
        permissions: &Permissions,
        mut outer: SvgDomBuilder<'a, SvggElement>,
        colour: Colour,
    ) -> RenderResult<'a, SvggElement> {
        // Render everything top-to-bottom, centered, then rotate at the end so that each label
        // corresponds with a permission bit.

        let mut block = outer.make_child::<SvggElement>("g")?;

        let font_size = PERMISSIONS_FONT_SIZE.to_string();
        let inactive_text_colour = colour.darken(0.2).html();
        let active_text_colour = colour::BLACK.html();
        let inactive_bg_colour = colour.lighten(0.5).html();
        let active_bg_colour = colour.html();

        let mut bgs = Vec::with_capacity(permissions.field().size_in_bits().try_into().unwrap());
        let mut ha = HorizontalAligner::new();
        for permission in permissions.iter().rev() {
            // Make the background highlight first. We'll position it later.
            let mut bg = block.make_child::<SvgRectElement>("rect")?;
            let mut text = block.make_child::<SvgTextElement>("text")?;
            text.set_attribute("font-family", MONO_FONT_FAMILY)?;
            text.set_attribute("font-size", &font_size)?;
            text.set_text_content(permission.name());
            if permission.value() {
                bg.set_attribute("fill", &active_bg_colour)?;
                text.set_attribute("fill", &active_text_colour)?;
            } else {
                bg.set_attribute("fill", &inactive_bg_colour)?;
                text.set_attribute("fill", &inactive_text_colour)?;
            }
            text.force_line_height(PERMISSIONS_LINE_HEIGHT)?;

            bgs.push(bg);
            if ha.try_add_centre(text, 0.0).is_err() {
                error(format!(
                    "Could not align permission label '{}'.",
                    permission.name()
                ));
                // Continue anyway.
            }
        }
        let vertical_vb = ha.resolve()?.viewbox().extended_x_by(DIAG_MARGIN);
        log(format!("vertical_vb: {}", vertical_vb));

        // Position the backgrounds.
        let bg_x = vertical_vb.min_x().to_string();
        let bg_width = vertical_vb.width().to_string();
        let bg_rect_height = PERMISSIONS_LINE_HEIGHT.to_string();
        for (n, mut bg) in bgs.into_iter().enumerate() {
            let bg_y = (vertical_vb.min_y() + (n as f32) * PERMISSIONS_LINE_HEIGHT).to_string();
            bg.set_attribute("x", &bg_x)?;
            bg.set_attribute("y", &bg_y)?;
            bg.set_attribute("width", &bg_width)?;
            bg.set_attribute("height", &bg_rect_height)?;
        }

        // Rotate the nested block, so that the transformations apply in the required order, but
        // override the outer viewbox, since viewbox overrides don't automatically propagate.
        let centre_x = vertical_vb.centre_x();
        let centre_y = vertical_vb.centre_y();
        block.rotate(270.0, centre_x, centre_y)?;
        outer.override_viewbox(vertical_vb.transposed_about(centre_x, centre_y));

        Ok(outer)
    }
}
