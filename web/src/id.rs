// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use std::{cell::Cell, fmt, rc::Rc};

/// Generate values for `id` attributes.
///
/// SVG sometimes requires ID-based references (e.g. for masking). These IDs must be unique not
/// only within the `<svg>` itself, but across the whole document. `Generator` provides a convenient
/// way to generate such IDs, given a unique prefix.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Generator<'a> {
    scope: &'a str,
    children: Rc<Cell<u32>>,
}

impl<'a> Generator<'a> {
    /// Create a `Generator` using `scope` as a disambiguator.
    ///
    /// The `Generator` claims the ability to generate any `id` beginning with `scope`, so the
    /// caller must ensure that `scope`...
    ///
    /// 1. ... is valid for use in an SVG `id` attribute. Notably, spaces are forbidden.
    /// 2. ... is a unique prefix amongst `id` attributes in the whole document.
    pub fn new(scope: &'a str) -> Self {
        Generator {
            scope,
            children: Rc::new(Cell::new(0)),
        }
    }

    /// Derive a new, unique `id` attribute.
    ///
    /// The caller must ensure that `name` is valid for use in an SVG `id` attribute.
    ///
    /// Successive calls to `derive_auto()` on the same `Generator` are guaranteed to generate
    /// unique IDs (even if the same `name` is reused).
    ///
    /// This is useful if you need to generate an unknown number of elements (maybe from different
    /// contexts), and need each to have unique IDs.
    pub fn derive_auto<S: fmt::Display>(&'a self, name: S) -> Id {
        let index = self.children.get();
        self.children.set(index + 1);
        Id(format!("{}_{}.{}", self.scope, index, name))
    }

    /// Derive a global (within `scope`) `id` attribute.
    ///
    /// The caller must ensure that `name` is valid for use in an SVG `id` attribute.
    ///
    /// This is useful if you need to find IDs for known elements (e.g. in a `<defs>` block) but
    /// still need `scope` disambiguation.
    pub fn derive_global<S: fmt::Display>(&self, name: S) -> Id {
        Id(format!("{}.{}", self.scope, name))
    }
}

/// An `id` attribute.
///
/// This is basically just a string, but with ID-specific conveniences.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Id(String);

impl Id {
    /// The ID, but in the `url("#<id>")` syntax used by many SVG attributes.
    pub fn to_url(&self) -> String {
        format!("url(\"#{}\")", self.0)
    }
}

impl AsRef<str> for Id {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl fmt::Display for Id {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        self.0.fmt(f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn derive_auto() {
        // The index auto-increments.
        let gen = Generator::new("scope");
        assert_eq!("scope_0.a", gen.derive_auto("a").as_ref());
        assert_eq!("scope_1.a", gen.derive_auto("a").as_ref());
        assert_eq!("scope_2.b", gen.derive_auto("b").as_ref());
        assert_eq!("scope_3.b", gen.derive_auto("b").as_ref());
        assert_eq!("scope_4.a", gen.derive_auto("a").as_ref());
    }

    #[test]
    fn derive_global() {
        // Global IDs aren't indexed.
        let gen = Generator::new("scope");
        assert_eq!("scope.a", gen.derive_global("a").as_ref());
        assert_eq!("scope.a", gen.derive_global("a").as_ref());
        assert_eq!("scope.b", gen.derive_global("b").as_ref());
        assert_eq!("scope.b", gen.derive_global("b").as_ref());
        assert_eq!("scope.a", gen.derive_global("a").as_ref());
    }

    #[test]
    fn clone() {
        // Clad generators share an index counter.
        let gen0 = Generator::new("scope");
        let gen1 = gen0.clone();
        assert_eq!("scope_0.a", gen0.derive_auto("a").as_ref());
        assert_eq!("scope_1.a", gen0.derive_auto("a").as_ref());
        assert_eq!("scope_2.b", gen1.derive_auto("b").as_ref());
        assert_eq!("scope_3.b", gen0.derive_auto("b").as_ref());
        assert_eq!("scope_4.a", gen1.derive_auto("a").as_ref());
        assert_eq!(gen0, gen1);
    }
}
