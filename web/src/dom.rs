// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{config::*, id, util::*, viewbox::Viewbox};
use capinfo::rich_text;
use std::{collections::HashSet, fmt};
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{
    Document, Node, SvgElement, SvgGraphicsElement, SvgTextElement, SvgsvgElement, SvgtSpanElement,
};

#[cfg(feature = "debug_layout")]
use web_sys::{SvgLineElement, SvgRectElement};

pub const SVG_NS: &str = "http://www.w3.org/2000/svg";

/// A marker indicating the type of children a builder has (and how many).
///
/// `SvgDomBuilder` permits elements to have any number of element children, or a single text-node
/// child, but not both.
#[derive(Clone, Copy, Debug)]
enum Children {
    Text,                   // `set_text_content` has been used.
    Elements { next: u32 }, // `make_child` has been used `next` times.
}

impl Children {
    fn new() -> Self {
        Children::Elements { next: 0 }
    }

    fn make_child(&mut self) -> u32 {
        if let Children::Elements { next } = *self {
            *self = Children::Elements { next: next + 1 };
            next
        } else {
            unimplemented!("mixed text/element children")
        }
    }

    fn make_text(&mut self) {
        match self {
            Children::Elements { next: 0 } => {
                *self = Children::Text;
            }
            Children::Elements { .. } => {
                unimplemented!("mixed text/element children")
            }
            Children::Text => {
                unimplemented!("multiple text nodes")
            }
        }
    }
}

/// Per-element DOM manipulation helpers.
///
/// This struct can be used to build complete SVG DOM trees.
///
/// Note that `SvgDomBuilder` cannot be cloned; to do so would almost certainly be an error,
/// because the clone would operate on the same DOM node, which would then be corrupted during the
/// deferred update.
#[derive(Debug)]
pub struct SvgDomBuilder<'a, E>
where
    E: JsCast + fmt::Debug,
{
    // `E` is only a marker type.
    phantom: std::marker::PhantomData<E>,
    internal: SvgDomBuilderInternal<'a>,
}

#[derive(Debug)]
struct SvgDomBuilderInternal<'a> {
    element: SvgElement,
    // We sometimes need references to the containing `document` and `svg` elements, for example to
    // create new elements.
    document: &'a Document,
    svg: &'a SvgsvgElement,
    // Track which properties have been set, so we can remove the others on `drop()`.
    children: Children,
    attrs: HashSet<&'a str>,
    classes: String,
    viewbox_override: Option<Viewbox>,
    id_generator: id::Generator<'a>,
}

impl<'a, E> SvgDomBuilder<'a, E>
where
    E: JsCast + fmt::Debug,
{
    pub fn new(
        document: &'a Document,
        svg: &'a SvgsvgElement,
        element: E,
        id_generator: id::Generator<'a>,
    ) -> Result<Self, JsValue> {
        let mut attrs = HashSet::new();
        // We manage some attributes ourselves.
        attrs.insert("class");
        attrs.insert("transform");
        attrs.insert("id");
        if let Some(g) = element.dyn_ref::<SvgGraphicsElement>() {
            g.transform().base_val().clear()?;
        }

        Ok(Self {
            phantom: std::marker::PhantomData,
            internal: SvgDomBuilderInternal {
                element: element.dyn_into().expect("Not an SvgElement"),
                document,
                svg,
                children: Children::new(),
                attrs,
                classes: String::new(),
                viewbox_override: None,
                id_generator,
            },
        })
    }

    /// An SvgDomBuilder can be converted to another SvgDomBuilder with a compatible element type.
    ///
    /// This mirrors the behaviour of `JsCast::dyn_into()`: if `self.internal.element` cannot be converted to
    /// `O`, this returns `Err(self)`.
    pub fn dyn_into<O>(self) -> Result<SvgDomBuilder<'a, O>, Self>
    where
        O: JsCast + fmt::Debug,
    {
        if self.internal.element.has_type::<O>() {
            Ok(SvgDomBuilder {
                phantom: std::marker::PhantomData::<O>,
                internal: self.internal,
            })
        } else {
            Err(self)
        }
    }

    /// Ensure that `element` has the specified attribute.
    pub fn set_attribute<S: AsRef<str>>(&mut self, name: &'a str, value: S) -> Result<(), JsValue> {
        // Some attributes are managed by other means.
        // TODO: We could consider catching and handling these transparently, but is it worth it?
        assert_ne!("class", name);
        assert_ne!("transform", name);
        assert_ne!("id", name);

        self.internal.attrs.insert(name);
        self.internal.element.set_attribute(name, value.as_ref())
    }

    /// Ensure that `self.internal.element` has the specified class name.
    pub fn add_class<S: AsRef<str>>(&mut self, value: S) {
        self.internal.attrs.insert("class");
        if !self.internal.classes.is_empty() {
            self.internal.classes.push(' ');
        }
        self.internal.classes.push_str(value.as_ref());
    }

    /// Set the `id` attribute using `id::Generator::derive_auto()`.
    ///
    /// Returns the new ID.
    pub fn set_id_auto(&mut self, name: impl fmt::Display) -> Result<id::Id, JsValue> {
        let id = self.internal.id_generator.derive_auto(name);
        self.internal
            .element
            .set_attribute("id", id.as_ref())
            .and(Ok(id))
    }

    /// Set the `id` attribute using `id::Generator::derive_global()`.
    pub fn set_id_global(&mut self, name: impl fmt::Display) -> Result<(), JsValue> {
        self.internal
            .element
            .set_attribute("id", self.global_id_for(name).as_ref())
    }

    /// Retrieve the global `id` with the specified `name` (using
    /// `id::Generator::derive_global()`).
    pub fn global_id_for(&self, name: impl fmt::Display) -> id::Id {
        self.internal.id_generator.derive_global(name)
    }

    /// Apply an SVG `translate` transformation to the element.
    ///
    /// Updates the viewbox override, if one is present.
    ///
    /// Panics if `self.internal.element` is not an `SvgGraphicsElement`.
    pub fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue> {
        let p = self
            .internal
            .element
            .dyn_ref::<SvgGraphicsElement>()
            .expect("Not an SvgGraphicsElement");
        let transform = self.internal.svg.create_svg_transform();
        transform.set_translate(x, y)?;
        p.transform().base_val().append_item(&transform)?;
        if let Some(vb) = self.internal.viewbox_override.as_mut() {
            vb.translate(x, y);
        }
        Ok(())
    }

    /// Apply an SVG `rotate` transformation to the element.
    ///
    /// Panics if `self.internal.element` is not an `SvgGraphicsElement`, or if a viewbox override is set.
    pub fn rotate(&mut self, angle: f32, x: f32, y: f32) -> Result<(), JsValue> {
        let p = self
            .internal
            .element
            .dyn_ref::<SvgGraphicsElement>()
            .expect("Not an SvgGraphicsElement");
        assert!(self.internal.viewbox_override.is_none());
        let transform = self.internal.svg.create_svg_transform();
        transform.set_rotate(angle, x, y)?;
        p.transform().base_val().append_item(&transform)?;
        Ok(())
    }

    pub fn has_transformations(&self) -> bool {
        match self.internal.element.dyn_ref::<SvgGraphicsElement>() {
            None => false,
            Some(g) => g.transform().base_val().number_of_items() > 0,
        }
    }

    /// Ensure that the next child is a node with the specified name, and return it as a new
    /// builder of the required type.
    ///
    /// This tries to reuse existing nodes where possible, which tends to result in a smoother UI
    /// (compared to rebuilding everything each time), for example preserving text selections
    /// across updates.
    ///
    /// Panics if text has been added to this builder (e.g. with `set_text_content()`).
    //
    // TODO: We want to be able to have multiple simulaneous builds active under a given parent,
    // but calling that parent's `viewbox()` doesn't `flush()` its children. Can we enforce that
    // with Rust lifetimes?
    pub fn make_child<T>(&mut self, name: &str) -> Result<SvgDomBuilder<'a, T>, JsValue>
    where
        T: JsCast + fmt::Debug,
    {
        let children = self.internal.element.children();
        let idx = self.internal.children.make_child();
        let child = if let Some(child) = children.item(idx) {
            if child.local_name() == name {
                // The next node is exactly what we want.
                child.dyn_into::<T>()?
            } else {
                // `child` is not what we want. Ignore it for now, and insert a new node before it.
                let new = self.create_element_svg::<Node>(name)?;
                self.internal
                    .element
                    .insert_before(&new, Some(&child))?
                    .dyn_into::<T>()?
            }
        } else {
            // We have run out of children; append a new one.
            let new = self.create_element_svg::<Node>(name)?;
            self.internal.element.append_child(&new)?.dyn_into::<T>()?
        };
        // Wrap `child` in a builder before returning it, so that its own children and attributes
        // will be cleaned up automatically.
        SvgDomBuilder::new(
            self.internal.document,
            self.internal.svg,
            child,
            self.internal.id_generator.clone(),
        )
    }

    /// Replace all children with a single text node, with the specified content.
    ///
    /// This (roughly) mirrors the behaviour of `Node::set_text_content()`.
    ///
    /// Panics if `set_text_children()` or `make_child()` have already called on this builder.
    pub fn set_text_content<S: AsRef<str>>(&mut self, text: S) {
        self.internal.children.make_text();
        self.internal.element.set_text_content(Some(text.as_ref()));
    }

    /// Insert a debug <rect> (as a child node) describing the provided viewbox.
    #[cfg(feature = "debug_layout")]
    pub fn make_debug_rect(&mut self, viewbox: Viewbox, colour: &str) -> Result<(), JsValue> {
        let x = format!("{}", viewbox.min_x());
        let y = format!("{}", viewbox.min_y());
        let width = format!("{}", viewbox.width());
        let height = format!("{}", viewbox.height());
        let mut rect = self.make_child::<SvgRectElement>("rect")?;
        rect.add_class("debug");
        rect.set_attribute("fill", "none")?;
        rect.set_attribute("stroke", colour)?;
        rect.set_attribute("stroke-width", "2")?;
        rect.set_attribute("stroke-dasharray", "4 2")?;
        rect.set_attribute("x", &x)?;
        rect.set_attribute("y", &y)?;
        rect.set_attribute("width", &width)?;
        rect.set_attribute("height", &height)?;
        Ok(())
    }

    /// Insert a debug <line> (as a child node).
    #[cfg(feature = "debug_layout")]
    pub fn make_debug_line(
        &mut self,
        start: (f32, f32),
        end: (f32, f32),
        colour: &str,
    ) -> Result<(), JsValue> {
        let mut line = self.make_child::<SvgLineElement>("line")?;
        line.add_class("debug");
        line.set_attribute("fill", "none")?;
        line.set_attribute("stroke", colour)?;
        line.set_attribute("stroke-width", "2")?;
        line.set_attribute("stroke-dasharray", "4 2")?;
        line.set_attribute("x1", start.0.to_string())?;
        line.set_attribute("y1", start.1.to_string())?;
        line.set_attribute("x2", end.0.to_string())?;
        line.set_attribute("y2", end.1.to_string())?;
        Ok(())
    }

    fn create_element_svg<T>(&self, name: &str) -> Result<T, JsValue>
    where
        T: JsCast,
    {
        Ok(self
            .internal
            .document
            .create_element_ns(Some(SVG_NS), name)?
            .dyn_into::<T>()?)
    }

    /// Retrieve the viewbox given to `override_viewbox()`, or the measured "b_box" of `element`.
    ///
    /// Measurement can fail (returning `None`) when...
    ///  - ... the element cannot be rendered (e.g. because it is not part of the DOM).
    ///  - ... any transformations (e.g. `translate()`) apply to the element, because the
    ///    underlying `get_b_box()` does not take these into account.
    ///
    /// If the viewbox must be measured, this automatically calls `flush()`.
    pub fn viewbox(&self) -> Option<Viewbox> {
        match self.internal.viewbox_override {
            None => {
                if self.flush().is_ok() {
                    let maybe_b_box = self
                        .internal
                        .element
                        .dyn_ref::<SvgGraphicsElement>()?
                        .get_b_box();
                    match (maybe_b_box, self.has_transformations()) {
                        (Ok(b_box), false) => Some(Viewbox::with_xywh(
                            b_box.x(),
                            b_box.y(),
                            b_box.width(),
                            b_box.height(),
                        )),
                        (_, true) => {
                            warn(
                                "Refusing to return `viewbox()` for builder with transformations.",
                            );
                            None
                        }
                        _ => {
                            error("Could not measure `viewbox()` of SvgDomBuilder.");
                            None
                        }
                    }
                } else {
                    error("Could not flush SvgDomBuilder on `viewbox()`.");
                    None
                }
            }
            some_vb => some_vb,
        }
    }

    /// Specify a `Viewbox` for this element.
    ///
    /// This has no effect on the DOM, but causes future `viewbox()` queries to return the
    /// specified viewbox. This is useful for handling text, for example, where a line-height-based
    /// viewbox might be prefable to a measured one.
    ///
    /// Note that DOM updates (e.g. adding children) do not modify the stored viewbox. This is not
    /// checked.
    ///
    /// Also note that this is local to this builder, and is not taken into account by its parent.
    pub fn override_viewbox(&mut self, viewbox: Viewbox) {
        self.internal.viewbox_override = Some(viewbox);
    }

    /// Ensure that the DOM matches the current state of the builder.
    fn flush(&self) -> Result<(), JsValue> {
        self.internal.flush()
    }
}

impl<'a> SvgDomBuilder<'a, SvgsvgElement> {
    /// Resize an `<svg>` to suit the provided `viewbox`.
    pub fn fit_to_viewbox(&mut self, viewbox: Viewbox) -> Result<(), JsValue> {
        debug(format!("SVG viewbox: {}", &viewbox));
        self.set_attribute(
            "viewBox",
            &format!(
                "{} {} {} {}",
                viewbox.min_x(),
                viewbox.min_y(),
                viewbox.width(),
                viewbox.height()
            ),
        )?;
        self.set_attribute("width", &format!("{}", viewbox.width()))?;
        self.set_attribute("height", &format!("{}", viewbox.height()))?;
        Ok(())
    }
}

impl<'a> SvgDomBuilder<'a, SvgTextElement> {
    /// Force text into the specified `line_height`. This keeps the line spacing reasonably
    /// consistent when some lines have varying fonts, subscripts, etc.
    ///
    /// This overrides the viewbox on `self`.
    pub fn force_line_height(&mut self, line_height: f32) -> Result<(), JsValue> {
        // TODO: See if there's a way to preserve the text baseline, rather than the centre.
        if let Some(vb) = self.viewbox() {
            let delta = (line_height - vb.height()) / 2.0;
            self.override_viewbox(vb.extended_y_by(delta));
        }
        Ok(())
    }

    /// Add formatted text to the <text> element, using <tspan> children as required.
    ///
    /// Returns the next value of `dy`.
    pub fn append_rich_text<R, S>(
        &mut self,
        rt: R,
        cont: &mut RichTextContinuation<'a>,
    ) -> Result<(), JsValue>
    where
        R: Into<rich_text::RichText<S>>,
        S: AsRef<str>,
    {
        let mut dx = 0.0;
        for block in rt.into().tabs() {
            for p in block.parts() {
                self.make_rich_text_part(&p, dx, cont)?;
                dx = 0.0;
            }
            dx = cont.dx_for_next_tab();
        }
        Ok(())
    }

    /// Add formatted text to the <text> element, using <tspan> children as required.
    ///
    /// Returns a continuation value to pass into a subsequent `append_rich_text()`.
    pub fn make_rich_text<R, S>(&mut self, rt: R) -> Result<RichTextContinuation<'a>, JsValue>
    where
        R: Into<rich_text::RichText<S>>,
        S: AsRef<str>,
    {
        let mut cont = RichTextContinuation::new();
        self.append_rich_text(rt, &mut cont)?;
        Ok(cont)
    }

    /// Add a single `<tspan>` to represent the `rich_text::Part`.
    ///
    /// `p`: The formatted text to add.
    /// `dx`: The value of the `dx` attribute to use.
    /// `cont`: Continuation information (originally provided by `make_rich_text()`).
    ///
    /// Returns the `dy` to be used on subsequent text.
    fn make_rich_text_part(
        &mut self,
        p: &rich_text::Part,
        dx: f32,
        cont: &mut RichTextContinuation<'a>,
    ) -> Result<(), JsValue> {
        let mut tspan = self.make_child::<SvgtSpanElement>("tspan")?;
        let fmt = p.format();
        if dx != 0.0 {
            tspan.set_attribute("dx", dx.to_string())?;
        }
        // The various options for rendering sub- and super-script seem to be poorly supported,
        // so do it manually.
        let dy_target;
        if fmt.is_subscript() {
            tspan.set_attribute("style", "font-size: 80%;")?;
            dy_target = FONT_SIZE / 5.0;
        } else if fmt.is_superscript() {
            tspan.set_attribute("style", "font-size: 80%;")?;
            dy_target = -FONT_SIZE / 3.2;
        } else {
            dy_target = 0.0;
        }
        if cont.dy_for(dy_target) != 0.0 {
            tspan.set_attribute("dy", &cont.dy_for(dy_target).to_string())?;
        };
        if fmt.is_mono() {
            tspan.set_attribute("font-family", "DejaVu Sans Mono, monospace")?;
        }
        if fmt.is_bold() {
            tspan.set_attribute("font-weight", "bold")?;
        }
        // "dominant-baseline" defaults to "auto", which should behave like "no-change" for <tspan>
        // elements. The exact behaviour varies across browsers, though, so make it explicit.
        tspan.set_attribute("dominant-baseline", "no-change")?;
        tspan.set_text_content(p.as_str());
        cont.append(tspan, dy_target);
        Ok(())
    }
}

pub struct RichTextContinuation<'a> {
    // We need to track:
    //  - ... the next value of "dy" to use to get the text on the default baseline.
    //  - ... the width so far.
    // As an optimisation, we don't measure any <tspan> until we need to, since `viewbox()` can be
    // expensive and most text elements don't need to be measured in parts.
    reset_dy: f32,
    first: Option<SvgDomBuilder<'a, SvgtSpanElement>>,
    last: Option<SvgDomBuilder<'a, SvgtSpanElement>>,
}

impl<'a> RichTextContinuation<'a> {
    fn new() -> Self {
        Self {
            reset_dy: 0.0,
            first: None,
            last: None,
        }
    }

    fn dy_for(&self, target: f32) -> f32 {
        self.reset_dy + target
    }

    fn dx_for_next_tab(&self) -> f32 {
        let width = self.width();
        let stop = (width / TAB_WIDTH).ceil() * TAB_WIDTH;
        stop - width
    }

    fn width(&self) -> f32 {
        if let Some(f) = self.first.as_ref().and_then(|tspan| tspan.viewbox()) {
            if let Some(l) = self.last.as_ref().and_then(|tspan| tspan.viewbox()) {
                TEXT_MARGIN + l.max_x() - f.min_x()
            } else {
                TEXT_MARGIN + f.width()
            }
        } else {
            0.0
        }
    }

    fn append(&mut self, tspan: SvgDomBuilder<'a, SvgtSpanElement>, dy: f32) {
        self.reset_dy = -dy;
        if self.first.is_some() {
            self.last = Some(tspan);
        } else {
            self.first = Some(tspan);
        }
    }
}

impl<'a> SvgDomBuilderInternal<'a> {
    /// Ensure that the DOM matches the current state of the builder.
    fn flush(&self) -> Result<(), JsValue> {
        // TODO: If we call `viewbox()`, we end up calling this once there, and again on `drop()`,
        // even though it's unlikely that anything has changed. If it matters, we could add a dirty
        // flag to reduce the number of DOM queries.

        match self.children {
            Children::Text => {
                // To get to this state, we should have called `set_text_content`, which removes
                // all element children and replaces them with zero or one text nodes.
                assert_eq!(0, self.element.child_element_count());
                assert!(self.element.child_nodes().length() <= 1);
                if self.element.child_nodes().length() > 0 {
                    assert_eq!(
                        Some(Node::TEXT_NODE),
                        self.element.child_nodes().item(0).map(|n| n.node_type())
                    );
                }
            }
            Children::Elements { next: count } => {
                // If the builder was constructed on an element that already had other children
                // (e.g. text nodes), we need to remove them here.
                let mut i = 0;
                let child_nodes = self.element.child_nodes();
                while let Some(child) = child_nodes.item(i) {
                    if (child.node_type() == Node::ELEMENT_NODE) && (i < count) {
                        // Keep this element.
                        i += 1;
                    } else {
                        // Remove everything else.
                        self.element.remove_child(&child)?;
                    }
                }
                // We should be left with exactly the right number of child elements.
                assert_eq!(count, i);
            }
        }

        // Remove attributes that we didn't set.
        let attributes = self.element.attributes();
        // Remove attributes that shouldn't be there. Iterate backwards so we can remove attributes
        // as we go without upsetting the index.
        for i in (0..attributes.length()).rev() {
            let name = attributes.item(i).unwrap().name();
            if !self.attrs.contains(name.as_str())
                && attributes.remove_named_item(name.as_str()).is_err()
            {
                error(format!(
                    "Could not remove superfluous attribute: '{}'",
                    name.as_str()
                ));
            }
        }

        // Register class names.
        if self.classes.is_empty() {
            if self.element.remove_attribute("class").is_err() {
                // We mainly use classes as debug annotations, so this is probably not critical.
                warn("Could not remove class.");
            }
        } else {
            // MDN says that it's better to use `setAttribute` for SVG elements (rather than
            // `setClassName`), so that's what we do.
            if self.element.set_attribute("class", &self.classes).is_err() {
                // We mainly use classes as debug annotations, so this is probably not critical.
                warn(format!("Could not set class: '{}'", &self.classes));
            }
        }
        Ok(())
    }
}

/// `Drop` is implemented for `SvgDomBuilderInternal`, not `SvgDomBuilder`, so we can move it in
/// `dyn_into()`.
impl<'a> Drop for SvgDomBuilderInternal<'a> {
    fn drop(&mut self) {
        if self.flush().is_err() {
            error("Could not flush SvgDomBuilder on `drop()`.");
        }
    }
}
