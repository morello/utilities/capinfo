// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#![allow(dead_code)]

use wasm_bindgen::prelude::*;

pub struct ConsoleGroup {}

/// Console groups with automatic (scope-based) duration.
impl ConsoleGroup {
    pub fn new<S: Into<JsValue>>(name: S) -> Self {
        web_sys::console::group_1(&name.into());
        Self {}
    }

    pub fn new_collapsed<S: Into<JsValue>>(name: S) -> Self {
        web_sys::console::group_collapsed_1(&name.into());
        Self {}
    }
}

impl std::ops::Drop for ConsoleGroup {
    fn drop(&mut self) {
        web_sys::console::group_end();
    }
}

// TODO: We very often call `log(format!(...))` and so on. Write macros like `log!(...)`, or
// investigate the `console_log` crate, which seems to offer such macros.

// A convenient wrapper around web_sys::console::debug_1.
pub fn debug<S: Into<JsValue>>(msg: S) {
    web_sys::console::debug_1(&msg.into());
}

// A convenient wrapper around web_sys::console::info_1.
pub fn info<S: Into<JsValue>>(msg: S) {
    web_sys::console::info_1(&msg.into());
}

// A convenient wrapper around web_sys::console::log_1.
pub fn log<S: Into<JsValue>>(msg: S) {
    web_sys::console::log_1(&msg.into());
}

// A convenient wrapper around web_sys::console::warn_1.
pub fn warn<S: Into<JsValue>>(msg: S) {
    web_sys::console::warn_1(&msg.into());
}

// A convenient wrapper around web_sys::console::error_1.
pub fn error<S: Into<JsValue>>(msg: S) {
    web_sys::console::error_1(&msg.into());
}
