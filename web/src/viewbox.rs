// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use std::fmt;

/// A simple representation of a rectangular area in an SVG, describing the extent of a UI element,
/// or the whole UI.
///
/// A `Viewbox` always has a position and non-negative dimensions, but may be empty.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Viewbox {
    min_x: f32,
    max_x: f32,
    min_y: f32,
    max_y: f32,
}

impl Viewbox {
    /// Create an empty `Viewbox` at the specified co-ordinates.
    ///
    /// Panics if either input is NaN.
    pub fn empty(x: f32, y: f32) -> Self {
        Self::with_xxyy(x, x, y, y)
    }

    /// Create a new `Viewbox` from X and Y ranges.
    ///
    /// Panics if `min_x > max_x`, `min_y > max_y` or if any input is NaN.
    pub fn with_xxyy(min_x: f32, max_x: f32, min_y: f32, max_y: f32) -> Self {
        assert!(min_x <= max_x);
        assert!(min_y <= max_y);
        Self {
            min_x,
            max_x,
            min_y,
            max_y,
        }
    }

    /// Create a new `Viewbox` from co-ordinates and size.
    ///
    /// Panics if `width < 0.0`, `height < 0.0` or if any input is NaN.
    pub fn with_xywh(x: f32, y: f32, width: f32, height: f32) -> Self {
        Self::with_xxyy(x, x + width, y, y + height)
    }

    pub fn max_x(&self) -> f32 {
        self.max_x
    }
    pub fn max_y(&self) -> f32 {
        self.max_y
    }
    pub fn min_x(&self) -> f32 {
        self.min_x
    }
    pub fn min_y(&self) -> f32 {
        self.min_y
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        let mut vb = *self;
        vb.clip(other);
        !vb.is_empty()
    }

    /// Extend the viewbox to include the specified X co-ordinate.
    pub fn extend_x(&mut self, to: f32) {
        self.min_x = self.min_x.min(to);
        self.max_x = self.max_x.max(to);
    }

    /// Extend the viewbox to include the specified X co-ordinate.
    pub fn extended_x(mut self, to: f32) -> Self {
        self.extend_x(to);
        self
    }

    /// Extend the viewbox to include the specified Y co-ordinate.
    pub fn extend_y(&mut self, to: f32) {
        self.min_y = self.min_y.min(to);
        self.max_y = self.max_y.max(to);
    }

    /// Extend the viewbox to include the specified Y co-ordinate.
    pub fn extended_y(mut self, to: f32) -> Self {
        self.extend_y(to);
        self
    }

    /// Extend the viewbox to include the whole of the other viewbox.
    pub fn extend(&mut self, other: &Viewbox) {
        self.min_x = self.min_x.min(other.min_x);
        self.max_x = self.max_x.max(other.max_x);
        self.min_y = self.min_y.min(other.min_y);
        self.max_y = self.max_y.max(other.max_y);
    }

    /// Consume this viewbox, producing a new one based on it and extended to include the whole of
    /// the other viewbox.
    pub fn extended(mut self, other: &Viewbox) -> Self {
        self.extend(other);
        self
    }

    /// Extend by `margin` in both directions on the X axis.
    pub fn extend_x_by(&mut self, margin: f32) {
        self.min_x -= margin;
        self.max_x += margin;
    }

    /// Extend by `margin` in both directions on the X axis.
    pub fn extended_x_by(mut self, margin: f32) -> Self {
        self.extend_x_by(margin);
        self
    }

    /// Extend by `margin` in both directions on the Y axis.
    pub fn extend_y_by(&mut self, margin: f32) {
        self.min_y -= margin;
        self.max_y += margin;
    }

    /// Extend by `margin` in both directions on the Y axis.
    pub fn extended_y_by(mut self, margin: f32) -> Self {
        self.extend_y_by(margin);
        self
    }

    /// Extend by `margin` in all directions.
    pub fn extended_by(mut self, margin: f32) -> Self {
        self.extend_x_by(margin);
        self.extend_y_by(margin);
        self
    }

    /// Clip to the bounds of `other`.
    ///
    /// This may leave `self` empty, but its co-ordinates (`min_x()` etc) will not leave the
    /// original `Viewbox`. This means that `a.clip(b)` and `b.clip(a)` are equivalent only if the
    /// result is non-empty.
    pub fn clip(&mut self, other: &Viewbox) {
        self.min_x = self.min_x.max(other.min_x).min(self.max_x);
        self.max_x = self.max_x.min(other.max_x).max(self.min_x);
        self.min_y = self.min_y.max(other.min_y).min(self.max_y);
        self.max_y = self.max_y.min(other.max_y).max(self.min_y);
    }

    /// Swap `width()` and `height()`, without moving the point `(x,y)`.
    pub fn transposed_about(self, x: f32, y: f32) -> Self {
        Self {
            min_x: x - self.height() / 2.0,
            max_x: x + self.height() / 2.0,
            min_y: y - self.width() / 2.0,
            max_y: y + self.width() / 2.0,
        }
    }

    pub fn translate(&mut self, x: f32, y: f32) {
        self.min_x += x;
        self.max_x += x;
        self.min_y += y;
        self.max_y += y;
    }

    pub fn translated(mut self, x: f32, y: f32) -> Self {
        self.translate(x, y);
        self
    }

    pub fn is_empty(&self) -> bool {
        !((self.min_x < self.max_x) && (self.min_y < self.max_y))
    }

    pub fn width(&self) -> f32 {
        self.max_x - self.min_x
    }

    pub fn height(&self) -> f32 {
        self.max_y - self.min_y
    }

    pub fn centre_x(&self) -> f32 {
        (self.min_x + self.max_x) / 2.0
    }

    pub fn centre_y(&self) -> f32 {
        (self.min_y + self.max_y) / 2.0
    }
}

impl fmt::Display for Viewbox {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "({},{}) to ({},{})",
            self.min_x(),
            self.min_y(),
            self.max_x(),
            self.max_y()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty() {
        let e = Viewbox::empty(0.0, 0.0);
        assert!(e.is_empty());
        assert_eq!(0.0, e.width());
        assert_eq!(0.0, e.height());
    }

    #[test]
    fn extend_empty() {
        let mut e = Viewbox::empty(42.0, 42.0);
        e.extend_x(42.0);
        assert!(e.is_empty());
        assert!(e.extended(&e).is_empty());
        assert!(e.extended_x(3.141).is_empty());
        assert!(e.extended_y(3.141).is_empty());
        // Viewboxes have position, even if empty, so they can be extended to become non-empty.
        assert!(!e.extended_x(3.141).extended_y(3.141).is_empty());
        assert!(!e.extended_by(3.141).is_empty());
        assert!(!e.extended(&Viewbox::empty(0.0, 0.0)).is_empty());
    }

    #[test]
    fn clip_empty() {
        // Clipping with a non-overlapping viewbox results in an empty viewbox inside the original.
        let base = Viewbox::with_xxyy(0.3, 0.9, -2.0, 3.0);

        let mut a = base;
        a.clip(&Viewbox::with_xxyy(0.3, 0.9, -4.0, -3.0));
        assert!(a.is_empty());
        assert_eq!(a, Viewbox::with_xxyy(0.3, 0.9, -2.0, -2.0));

        let mut b = base;
        b.clip(&Viewbox::with_xxyy(0.9, 0.9, -4.0, 1.0));
        assert!(b.is_empty());
        assert_eq!(b, Viewbox::with_xxyy(0.9, 0.9, -2.0, 1.0));
    }
}
