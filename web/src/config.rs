// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

//! Various easily-tweakable settings to tune the capability display.

use crate::{colour, colour::Colour};
use capinfo::capability::{
    described_field::{Category, DescribedField},
    explanation::Explanation,
};

/// For binary layouts, the number of pixels (usually in width) for each bit.
pub const PX_PER_BIT: f32 = 10.0;

/// For hexadecimal layouts, the number of pixels (usually in width) for each nibble.
pub const PX_PER_NIBBLE: f32 = PX_PER_BIT * 4.0 * 0.6;

/// The curviness of bitfield edge breaks, as a proportion of field height.
pub const FIELD_BREAK_CURVINESS: f32 = 0.15;

/// Minimum spacing between break curves and a rendered bit, as a proportion of the width per bit.
pub const FIELD_BREAK_SPACE: f32 = 1.0;

/// Base spacing around diagrams.
pub const DIAG_MARGIN: f32 = 6.0;

/// Spacing between horizontally-adjacent diagrams.
pub const DIAG_HORIZONTAL_MARGIN: f32 = DIAG_MARGIN * 2.0;

/// Minimum spacing between adjacent explanation boxes.
pub const EXPLANATION_MARGIN: f32 = 24.0;

/// Base spacing around blocks of text.
pub const TEXT_MARGIN: f32 = 6.0;

/// The spacing of tab stops.
pub const TAB_WIDTH: f32 = FONT_SIZE * 2.0;

/// Spacing around characters in the error diagram. If 0.0, the diagram should match the input
/// text, but this makes the border highlights look cramped.
pub const ERROR_CHAR_MARGIN: f32 = 2.0;

/// Base font size.
pub const FONT_SIZE: f32 = 16.0;

pub const EXPLANATION_TITLE_FONT_SIZE: f32 = FONT_SIZE * 1.125;

pub const PERMISSIONS_FONT_SIZE: f32 = FONT_SIZE * 0.625;

/// Base line height.
pub const LINE_HEIGHT: f32 = FONT_SIZE + TEXT_MARGIN;

pub const EXPLANATION_TITLE_LINE_HEIGHT: f32 = EXPLANATION_TITLE_FONT_SIZE + TEXT_MARGIN;

pub const PERMISSIONS_LINE_HEIGHT: f32 = PERMISSIONS_FONT_SIZE + TEXT_MARGIN * 0.5;

/// Font size for binary field displays.
pub const BIN_FONT_SIZE: f32 = FONT_SIZE;

/// Font size for hex field displays.
pub const HEX_FONT_SIZE: f32 = FONT_SIZE * 1.5;

/// Font size for binary digits in field displays showing both hex and binary.
pub const BIN_FONT_SIZE_WITH_HEX: f32 = BIN_FONT_SIZE;

/// Font size for hex digits in field displays showing both hex and binary.
pub const HEX_FONT_SIZE_WITH_BIN: f32 = BIN_FONT_SIZE_WITH_HEX * 2.0;

/// Font size for subfield labels and bit numbers on field displays.
pub const FIELD_ANNOTATION_FONT_SIZE: f32 = FONT_SIZE * 0.625;

/// Default spacing between adjacent field annotation text elements.
pub const FIELD_ANNOTATION_TEXT_MARGIN: f32 = TEXT_MARGIN * FIELD_ANNOTATION_FONT_SIZE / FONT_SIZE;

pub const FONT_FAMILY: &str = "DejaVu Serif, serif";

pub const MONO_FONT_FAMILY: &str = "DejaVu Sans Mono, monospace";

/// The base length of lines used for bit number and subfield name labels.
pub const FIELD_ANNOTATION_LINE_LENGTH: f32 = 10.0;

/// The preference for inline (rather than one-per-line) diagrams, as a ratio between the visual area
/// required by each. Lower values favour one diagram per line.
pub const INLINE_DIAGRAM_PREFERENCE: f32 = 0.7;

/// Map colours to field categories.
pub fn colour_for_field_category(category: Category) -> Option<Colour> {
    use colour::arm;
    use Category::*;
    match category {
        Value => Some(arm::GREEN),
        BoundsValue => Some(arm::GREEN),
        ValueMeta => Some(arm::GREEN.blended_with(&arm::LIGHT_GREY, 0.5)),
        Bounds => Some(arm::BLUE.blended_with(&arm::LIGHT_BLUE, 0.5)),
        BoundsBase => Some(arm::BLUE),
        BoundsLimit => Some(arm::LIGHT_BLUE),
        BoundsExponent => Some(arm::ORANGE),
        BoundsFormat => Some(arm::GREY),
        ObjectType => Some(arm::LIGHT_GREY),
        Permissions => Some(arm::YELLOW),
        Tag => Some(arm::GREY),
        _ => None,
    }
}

pub fn colour_for_field(df: &DescribedField) -> Option<Colour> {
    colour_for_field_category(df.category())
}

pub fn colour_for_explanation(e: &Explanation) -> Option<Colour> {
    colour_for_field_category(e.category())
}
