// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use crate::{dom::SvgDomBuilder, viewbox::Viewbox};
use itertools::Itertools;
use std::{
    fmt, mem,
    ops::{Deref, DerefMut},
};
use wasm_bindgen::{prelude::*, JsCast};

/// An element, or group of elements, that can be translated, and have a known viewbox.
///
/// `translate()` keeps the viewbox up to date, so `viewbox()` always returns a current value.
pub trait Alignable {
    fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue>;
    fn viewbox(&self) -> Viewbox;
}

impl<'a> Alignable for Box<dyn Alignable + 'a> {
    fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue> {
        Box::deref_mut(self).translate(x, y)
    }
    fn viewbox(&self) -> Viewbox {
        Box::deref(self).viewbox()
    }
}

/// Like `convert::TryInto`, but where the concrete result type is parameterised.
pub trait TryIntoAlignable {
    type IntoOk: Alignable;
    type IntoErr;
    fn try_into_alignable(self) -> Result<Self::IntoOk, Self::IntoErr>;
}

#[derive(Copy, Clone, Debug)]
enum HorizontalAlignment {
    Left,
    Centre,
    Offset(f32),
}

struct HorizontallyAlignedElement<'a> {
    element: Box<dyn Alignable + 'a>,
    align: HorizontalAlignment,
    margin: f32,
}

impl<'a> Alignable for HorizontallyAlignedElement<'a> {
    fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue> {
        self.element.translate(x, y)
    }
    fn viewbox(&self) -> Viewbox {
        self.element.viewbox()
    }
}

/// Top-to-bottom positioning, with per-element horizontal alignment.
pub struct HorizontalAligner<'a> {
    elements: Vec<HorizontallyAlignedElement<'a>>,
    min_x: Option<f32>,
    max_x: Option<f32>,
    min_width: f32,
}

impl<'a> HorizontalAligner<'a> {
    pub fn new() -> Self {
        Self {
            elements: Vec::new(),
            min_x: None,
            max_x: None,
            min_width: 0.0,
        }
    }

    /// The height required for the elements added so far.
    pub fn min_height(&self) -> f32 {
        let mut last_bottom_margin = 0.0;
        self.elements.iter().fold(0.0, |height, hae| {
            // Accumulate everything except the bottom margin (since the following element might
            // override that). We'll add the final bottom margin at the end.
            let top_margin = hae.margin.max(last_bottom_margin);
            last_bottom_margin = hae.margin;
            height + top_margin + hae.element.viewbox().height()
        }) + last_bottom_margin
    }

    /// The width required for the elements added so far.
    pub fn min_width(&self) -> f32 {
        self.min_width
    }

    /// Ensure that elements are laid out with at least the specified width.
    pub fn increase_min_width(&mut self, width: f32) {
        self.min_width = self.min_width.max(width);
    }

    fn add(&mut self, element: impl Alignable + 'a, align: HorizontalAlignment, margin: f32) {
        let viewbox = element.viewbox().extended_by(margin);
        self.increase_min_width(viewbox.width());
        self.elements.push(HorizontallyAlignedElement {
            element: Box::new(element),
            align,
            margin,
        });
        if let HorizontalAlignment::Offset(offset) = align {
            let element_min_x = viewbox.min_x() + offset;
            let element_max_x = viewbox.max_x() + offset;
            self.min_x = Some(self.min_x.unwrap_or(element_min_x).min(element_min_x));
            self.max_x = Some(self.max_x.unwrap_or(element_max_x).max(element_max_x));
        }
    }

    /// Add an empty element with the specified height.
    ///
    /// Note that even if `height` is `0.0`, this has the effect of preventing adjacent margins
    /// from being merged.
    pub fn add_margin_break(&mut self, height: f32) {
        self.add(
            EmptyAlignable::new_with_height(height),
            HorizontalAlignment::Centre,
            0.0,
        );
    }

    pub fn add_left(&mut self, element: impl Alignable + 'a, margin: f32) {
        self.add(element, HorizontalAlignment::Left, margin);
    }

    pub fn add_centre(&mut self, element: impl Alignable + 'a, margin: f32) {
        self.add(element, HorizontalAlignment::Centre, margin);
    }

    pub fn add_offset(&mut self, element: impl Alignable + 'a, offset: f32, margin: f32) {
        self.add(element, HorizontalAlignment::Offset(offset), margin);
    }

    pub fn try_add_left<E>(&mut self, element: E, margin: f32) -> Result<(), E::IntoErr>
    where
        E: TryIntoAlignable,
        E::IntoOk: 'a,
    {
        self.add_left(element.try_into_alignable()?, margin);
        Ok(())
    }

    pub fn try_add_centre<E>(&mut self, element: E, margin: f32) -> Result<(), E::IntoErr>
    where
        E: TryIntoAlignable,
        E::IntoOk: 'a,
    {
        self.add_centre(element.try_into_alignable()?, margin);
        Ok(())
    }

    /// Reset the aligner to its initial state (as `new()`), and return all the elements that have
    /// been added so far.
    pub fn take_raw_elements(&mut self) -> impl Iterator<Item = Box<dyn Alignable + 'a>> {
        let old = mem::replace(self, Self::new());
        old.elements.into_iter().map(|hae| hae.element)
    }

    pub fn element_count(&self) -> usize {
        self.elements.len()
    }

    pub fn resolve(self) -> Result<ElementGroup<'a>, JsValue> {
        // Centre boundary co-ordinates on the constraints set by `Offset`-mode alignments, or pick
        // arbitrary ones otherwise.
        let (min_x, centre_x, max_x);
        match (self.min_x, self.max_x) {
            (Some(offset_min_x), Some(offset_max_x)) => {
                centre_x = (offset_min_x + offset_max_x) / 2.0;
                min_x = offset_min_x.min(centre_x - self.min_width() / 2.0);
                max_x = offset_max_x.min(centre_x + self.min_width() / 2.0);
            }
            (None, None) => {
                centre_x = 42.0;
                min_x = centre_x - self.min_width() / 2.0;
                max_x = centre_x + self.min_width() / 2.0;
            }
            _ => unreachable!("Offset modes should set both min and max."),
        }

        let mut last_bottom_margin = 0.0;
        let mut viewbox = Viewbox::with_xxyy(min_x, max_x, 0.0, 0.0);
        let mut elements = Vec::with_capacity(self.elements.len());
        for mut hae in self.elements.into_iter() {
            use HorizontalAlignment::*;
            let delta_x = match hae.align {
                Left => min_x - (hae.viewbox().min_x() - hae.margin),
                Centre => centre_x - hae.viewbox().centre_x(),
                Offset(offset) => offset,
            };
            // Collapse adjacent margins (like CSS).
            let y_margin = hae.margin.max(last_bottom_margin);
            let delta_y = y_margin + viewbox.max_y() - hae.viewbox().min_y();
            hae.translate(delta_x, delta_y)?;
            viewbox.extend(&hae.viewbox());
            elements.push(hae.element);
            last_bottom_margin = hae.margin;
        }
        viewbox.extend_y(viewbox.max_y() + last_bottom_margin);
        Ok(ElementGroup { elements, viewbox })
    }
}

/// An empty element that renders to nothing.
///
/// Useful as a default, and prevents adjacent margins from overlapping.
struct EmptyAlignable {
    viewbox: Viewbox,
}

impl<'a> Alignable for EmptyAlignable {
    fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue> {
        self.viewbox.translate(x, y);
        Ok(())
    }
    fn viewbox(&self) -> Viewbox {
        self.viewbox
    }
}

impl EmptyAlignable {
    fn new() -> Self {
        Self::new_with_height(0.0)
    }

    fn new_with_height(height: f32) -> Self {
        Self {
            viewbox: Viewbox::with_xywh(0.0, 0.0, 0.0, height),
        }
    }
}

/// Zero or more elements, aligned as a group.
pub struct ElementGroup<'a> {
    elements: Vec<Box<dyn Alignable + 'a>>,
    viewbox: Viewbox,
}

impl<'a> Alignable for ElementGroup<'a> {
    fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue> {
        self.viewbox.translate(x, y);
        for e in self.elements.iter_mut() {
            e.translate(x, y)?;
        }
        Ok(())
    }
    fn viewbox(&self) -> Viewbox {
        self.viewbox
    }
}

impl<'a> ElementGroup<'a> {
    pub fn with_elements<E, I>(elements: I) -> Self
    where
        E: Alignable + 'a,
        I: Iterator<Item = E>,
    {
        Self::try_with_elements(elements.map(Ok)).unwrap()
    }

    pub fn try_with_elements<E, I>(elements: I) -> Result<Self, JsValue>
    where
        E: Alignable + 'a,
        I: Iterator<Item = Result<E, JsValue>>,
    {
        let elements: Vec<_> = elements
            .map::<Result<_, JsValue>, _>(|e| Ok(Box::new(e?) as Box<dyn Alignable + 'a>))
            .try_collect()?;
        // This will be `None` if there are no elements.
        let maybe_viewbox = elements
            .iter()
            .map(|e| e.viewbox())
            .reduce(|a, b| a.extended(&b));
        Ok(Self {
            elements,
            viewbox: maybe_viewbox.unwrap_or_else(|| EmptyAlignable::new().viewbox()),
        })
    }

    pub fn empty() -> Self {
        Self {
            elements: vec![],
            viewbox: EmptyAlignable::new().viewbox(),
        }
    }
}

/// An AlignableSvgDomBuilder is an SvgDomBuilder, restricted so that it can be `Alignable`. In
/// particular, no changes are permitted that might change or invalidate the `viewbox()`.
#[derive(Debug)]
pub struct AlignableSvgDomBuilder<'a, E: JsCast + fmt::Debug> {
    builder: SvgDomBuilder<'a, E>,
}

impl<'a, E: JsCast + fmt::Debug> Alignable for AlignableSvgDomBuilder<'a, E> {
    fn translate(&mut self, x: f32, y: f32) -> Result<(), JsValue> {
        // SvgDomBuilder automatically translates the viewbox override.
        self.builder.translate(x, y)?;
        Ok(())
    }

    fn viewbox(&self) -> Viewbox {
        self.builder
            .viewbox()
            .expect("Viewbox should be overridden.")
    }
}

impl<'a, E: JsCast + fmt::Debug> TryIntoAlignable for SvgDomBuilder<'a, E> {
    type IntoOk = AlignableSvgDomBuilder<'a, E>;
    /// On error, return the original builder (unmodified).
    type IntoErr = Self;

    fn try_into_alignable(mut self) -> Result<AlignableSvgDomBuilder<'a, E>, Self> {
        match self.viewbox() {
            Some(vb) => {
                // Make sure that the viewbox is overridden, so we can `expect()` success in
                // `viewbox()` later.
                self.override_viewbox(vb);
                Ok(Self::IntoOk { builder: self })
            }
            // We can't align something without a viewbox.
            None => Err(self),
        }
    }
}
