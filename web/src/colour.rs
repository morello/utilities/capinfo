// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Colour {
    r: u8,
    g: u8,
    b: u8,
}

impl Colour {
    pub const fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }

    pub fn html(&self) -> String {
        format!("#{:02x}{:02x}{:02x}", self.r, self.g, self.b)
    }

    pub fn blended_with(&self, other: &Self, alpha: f32) -> Self {
        let blend = |a: u8, b: u8| -> u8 {
            let a = f32::from(a);
            let b = f32::from(b);
            let r = (a * alpha) + (b * (1.0 - alpha));
            r.round().clamp(0.0, 255.0) as u8
        };
        Self {
            r: blend(self.r, other.r),
            g: blend(self.g, other.g),
            b: blend(self.b, other.b),
        }
    }

    pub fn lighten(&self, alpha: f32) -> Self {
        WHITE.blended_with(self, alpha)
    }

    pub fn darken(&self, alpha: f32) -> Self {
        BLACK.blended_with(self, alpha)
    }
}

pub const WHITE: Colour = Colour::new(0xff, 0xff, 0xff);
pub const BLACK: Colour = Colour::new(0x00, 0x00, 0x00);

#[allow(dead_code)]
pub mod arm {
    use super::Colour;
    pub const LIGHT_GREY: Colour = Colour::new(0xe5, 0xec, 0xeb);
    pub const GREY: Colour = Colour::new(0x7d, 0x86, 0x8c);
    pub const DARK_GREY: Colour = Colour::new(0x33, 0x3e, 0x48);
    pub const DARK_BLUE: Colour = Colour::new(0x33, 0x3e, 0x48);
    pub const BLUE: Colour = Colour::new(0x00, 0x91, 0xbd);
    pub const LIGHT_BLUE: Colour = Colour::new(0x00, 0xc1, 0xde);
    pub const GREEN: Colour = Colour::new(0x95, 0xd6, 0x00);
    pub const YELLOW: Colour = Colour::new(0xff, 0xc7, 0x00);
    pub const ORANGE: Colour = Colour::new(0xff, 0x6b, 0x00);
}
