// Copyright (c) 2021 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

mod colour;
mod config;
mod dom;
mod id;
mod layout;
mod ui;
mod util;
mod viewbox;

use capinfo::capability::explanation::Verbosity;
use std::cell::RefCell;
use std::rc::Rc;
use ui::{capinfo::UiCapinfo, component::Component};
use url::form_urlencoded;
use util::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{Document, HtmlElement, HtmlInputElement, SvgsvgElement, UrlSearchParams, Window};

pub use dom::SvgDomBuilder;

#[wasm_bindgen(start)]
pub fn main() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
}

#[wasm_bindgen]
pub fn version() -> String {
    String::from(env!("CARGO_PKG_VERSION"))
}

#[wasm_bindgen]
pub fn capinfo_version() -> String {
    capinfo::version()
}

fn initial_cap_str(window: &Window) -> String {
    window
        .location()
        .search()
        .and_then(|s| UrlSearchParams::new_with_str(&s))
        // Throw away the Err values. We don't examine them.
        .ok()
        .and_then(|p| p.get("c"))
        .unwrap_or_else(|| {
            log("No 'c' GET parameter found. Using an arbitrary default capability.");
            "0x1:fc00000033371117:12342222".to_string()
        })
}

fn update_input_class(parsed_ok: bool, input: &HtmlInputElement) {
    if parsed_ok {
        if input.remove_attribute("class").is_err() {
            warn("Could not remove <input> class.");
        }
    } else if input.set_attribute("class", "error").is_err() {
        warn("Could not set <input> class: 'error'");
    }
}

fn render(
    capinfo: &UiCapinfo,
    document: &Document,
    svg: &SvgsvgElement,
    id_base: &str,
) -> Result<(), JsValue> {
    match SvgDomBuilder::new(document, svg, svg.clone(), id::Generator::new(id_base)) {
        Ok(builder) => capinfo.render(builder).map(|_| ()),
        Err(err) => {
            warn("Failed to construct SvgDomBuilder on output <svg>.");
            Err(err)
        }
    }
}

#[wasm_bindgen]
pub fn append_capinfo_to(container: HtmlElement, id_base: &str) -> Result<(), JsValue> {
    let window = web_sys::window().expect("No 'window' exists.");
    let document = window.document().expect("No 'document' exists.");
    let svg: SvgsvgElement = document
        .create_element_ns(Some(dom::SVG_NS), "svg")
        .expect("Could not create <svg>.")
        .dyn_into()
        .unwrap();
    let cap_str = initial_cap_str(&window);

    let input = document
        .create_element("input")?
        .dyn_into::<HtmlInputElement>()?;
    input.set_value(&cap_str);
    container.append_child(&input)?;
    container.append_child(&svg)?;

    // TODO: Add some UI to toggle these settings.
    let mut verbosity = Verbosity::default();
    verbosity.set_trivial(false);
    verbosity.set_descriptions(true);
    verbosity.set_fancy_bounds(true);
    verbosity.set_multi_value_maps(true);

    let capinfo = {
        let _cg = ConsoleGroup::new_collapsed(format!("Initialising Capinfo: {}", cap_str));
        let c = UiCapinfo::from_string(cap_str, verbosity);
        update_input_class(c.parsed_ok(), &input);
        render(&c, &document, &svg, id_base)?;
        c
    };

    // Wrap the Capinfo in an Rc<RefCell<...>> so we can mutate it in event handlers.
    let capinfo = Rc::new(RefCell::new(capinfo));

    // Update when the input is updated.
    {
        // Clone (and shadow) variables so we can take them out of scope.
        let capinfo = Rc::clone(&capinfo);
        let i = input.clone();
        let document = document.clone();
        let svg = svg.clone();
        let id_base = id_base.to_string();
        let closure = Closure::wrap(Box::new(move || {
            let _cg =
                ConsoleGroup::new_collapsed(format!("Input updated (onchange): {}", &i.value()));
            let update = capinfo.borrow_mut().update_with_value(i.value(), verbosity);
            update_input_class(update.parsed_ok(), &i);
            if update.render_needed()
                && render(&capinfo.borrow(), &document, &svg, &id_base).is_err()
            {
                warn("Failed to `render()` on input update.");
            }

            // Also update the URL bar.
            let get: String = form_urlencoded::Serializer::for_suffix("?".to_string(), 1)
                .append_pair("c", &i.value())
                .finish();
            window
                .history()
                .expect("Couldn't access 'window.history'.")
                .push_state_with_url(&JsValue::NULL, "Capinfo", Some(&get))
                .expect("Couldn't write browser history.");
        }) as Box<dyn Fn()>);
        input.add_event_listener_with_callback("change", closure.as_ref().unchecked_ref())?;
        closure.forget(); // TODO: this leaks. Is there a better way?
    }

    // Eagerly update when the input is edited, but only if the edit is valid.
    {
        // Clone (and shadow) variables so we can take them out of scope.
        let i = input.clone();
        let id_base = id_base.to_string();
        let closure = Closure::wrap(Box::new(move || {
            let _cg =
                ConsoleGroup::new_collapsed(format!("Input updated (oninput): {}", &i.value()));
            let update = capinfo
                .borrow_mut()
                .maybe_update_with_value(i.value(), verbosity);
            update_input_class(update.parsed_ok(), &i);
            if update.render_needed()
                && render(&capinfo.borrow(), &document, &svg, &id_base).is_err()
            {
                warn("Failed to `render()` on eager input update.");
            }
        }) as Box<dyn Fn()>);
        input.add_event_listener_with_callback("input", closure.as_ref().unchecked_ref())?;
        closure.forget(); // TODO: this leaks. Is there a better way?
    }

    Ok(())
}
