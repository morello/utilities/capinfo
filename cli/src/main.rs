// Copyright (c) 2021-2022 Arm Limited. All rights reserved.
//
// SPDX-License-Identifier: BSD-3-Clause

use capinfo::{
    capability::explanation,
    capability::{error, CapabilityWithFormat},
    Capability,
};
use clap::{App, Arg};
use std::convert::TryFrom;

fn report_error(e: error::ParseError, cap_str: &str) {
    eprintln!("{}:", e);
    eprintln!("  {}", cap_str);
    if let Some(flags) = e.flagged() {
        eprintln!("  {}", flags);
    }
}

fn main() {
    let matches = App::new("Capability Inspection Tool")
        .version("0.1.0")
        .author("Arm Ltd")
        .arg(
            Arg::with_name("CAPABILITY")
                .required(true)
                .help("The capability to inspect.")
                .long_help(
                    format!(
                        "The capability to inspect. {}",
                        CapabilityWithFormat::legal_formats()
                    )
                    .as_str(),
                ),
        )
        .arg(
            Arg::with_name("alt_format")
                .takes_value(false)
                .short('a')
                .long("alt-format")
                .help(
                    "Print the capability with bar-separated parts:
    {tag}|{high64}|{low64}",
                ),
        )
        .arg(
            Arg::with_name("show_trivial")
                .short('T')
                .long("show-trivial")
                .help("Show trivial explanations. This is the default.")
                .display_order(0),
        )
        .arg(
            Arg::with_name("hide_trivial")
                .short('t')
                .long("hide-trivial")
                .help("Hide trivial explanations.")
                .overrides_with("show_trivial")
                .display_order(0),
        )
        .arg(
            Arg::with_name("show_descriptions")
                .short('D')
                .long("show-descriptions")
                .help("Show descriptive (verbose) explanations of calculation stages.")
                .display_order(1),
        )
        .arg(
            Arg::with_name("hide_descriptions")
                .short('d')
                .long("hide-descriptions")
                .help("Hide descriptive explanations of calculation stages. This is the default.")
                .overrides_with("show_descriptions")
                .display_order(1),
        )
        .arg(
            Arg::with_name("show_fancy_bounds")
                .short('F')
                .long("show-fancy-bounds")
                .help("Show all bounds calculation stages.")
                .display_order(2),
        )
        .arg(
            Arg::with_name("hide_fancy_bounds")
                .short('f')
                .long("hide-fancy-bounds")
                .help("Hide verbose stages of the bounds calculation. This is the default.")
                .display_order(2),
        )
        .get_matches();

    let cap = matches.value_of("CAPABILITY").unwrap();
    let mut cap = match CapabilityWithFormat::try_from(cap) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Failed to parse capability.");
            report_error(e, cap);
            return;
        }
    };

    macro_rules! should_show {
        ($name:expr, $default:expr) => {
            match (
                matches.is_present(concat!("show_", $name)),
                matches.is_present(concat!("hide_", $name)),
            ) {
                (true, _) => true,
                (_, true) => false,
                (false, false) => $default,
            }
        };
    }

    // Note: these defaults must agree with the help text above.
    let mut verbosity = explanation::Verbosity::default();
    verbosity.set_trivial(should_show!("trivial", true));
    verbosity.set_descriptions(should_show!("descriptions", false));
    verbosity.set_fancy_bounds(should_show!("fancy_bounds", false));
    verbosity.set_multi_value_maps(false);
    cap.set_verbosity(verbosity);

    let heading = if matches.is_present("alt_format") {
        format!(" Capability: {:#} ", cap)
    } else {
        format!(" Capability: {} ", cap)
    };
    println!("----{:-<76}", heading);

    // Format explanations.
    let expls: Vec<_> = cap
        .explanations()
        .into_iter()
        .map(|expl| (format!("  {}: ", expl.title()), expl))
        .collect();
    let prefix_width = expls
        .iter()
        .map(|(title, _)| title.len())
        .max()
        .unwrap_or(0);
    for (mut prefix, expl) in expls {
        for element in expl.elements().map(|e| e.to_string()) {
            for line in element.split('\n') {
                println!("{prefix:>prefix_width$}{line}");
                prefix.clear();
            }
        }
    }
}
