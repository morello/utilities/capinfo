# Capinfo (command-line interface)

The command-line `capinfo` binary, for capability inspection.

This is the default binary for the project workspace, and can be run using
`cargo run`, either locally or from the workspace root. The command-line syntax
is described by `cargo run -- --help`.

Refer to the [project-level documentation][top] for further information.

[top]: ../README.md
